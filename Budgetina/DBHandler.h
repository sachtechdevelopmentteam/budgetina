//
//  DBHandler.h
//  ImgScore
//
//  Created by Vipul Patel on 13/10/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface DBHandler : NSObject{
    BOOL isChecked;
}

+(NSString *)NullsafeValue:(char *)value;
+(BOOL) genericQuery:(NSString *)temp_sql;
+(NSMutableArray *) getDataFromTable:(NSString *)tableNameWithAnyCondition;
+(NSMutableArray *) getDataUsingQuery:(NSString *)query;
+(BOOL) checkMonthAlreadyAddedForCategory:(NSString *)query;
+(void) addSignature:(UIImage *) imgSignature onPDFData:(NSData *)pdfData atPosition:(CGRect )rect;
+(NSInteger) getTotalSubCategoriesForCategory:(NSString *)query;

@end
