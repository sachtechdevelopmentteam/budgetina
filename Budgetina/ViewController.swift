//
//  ViewController.swift
//  Budgetina
//
//  Created by Ovedgroup LLC on 28/10/15.
//  Copyright © 2015 Ovedgroup LLC. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase

class ViewController: UIViewController,UITextFieldDelegate,UIScrollViewDelegate,UIAlertViewDelegate,UIViewControllerTransitioningDelegate{
       
    @IBOutlet var imageLogo: UIImageView!
    @IBOutlet var txtEmailName: UITextField!
    @IBOutlet var txtPassword: UITextField!
    @IBOutlet var btnSignUp: UIButton!
    @IBOutlet var scrollViewLogin: UIScrollView!
    @IBOutlet var ActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var loginButton: TKTransitionSubmitButton!
    @IBOutlet weak var forgetPasswordBtn: UIButton!
    var tField: UITextField!
    weak var actionToEnable : UIAlertAction?
    var ref: DatabaseReference!

    @IBAction func btnSignUpAction(_ sender: UIButton)
    {
        self.navigationItem.title = ""
        let SignVC = self.storyboard?.instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
        self.navigationController?.pushViewController(SignVC, animated: true)
    }
    
    @IBAction func btnclickArrowAction(_ sender: UIButton)
    {
        self.parsingurl()
    }
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return TKFadeInAnimator(transitionDuration: 0.5, startingAlpha: 0.8)
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return nil
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        ref = Database.database().reference()

        txtEmailName.layer.cornerRadius = txtEmailName.frame.size.height/2
        txtPassword.layer.cornerRadius = txtPassword.frame.size.height/2
        btnSignUp.layer.cornerRadius = btnSignUp.frame.size.height/2
        loginButton.layer.cornerRadius = loginButton.frame.size.height/2

        
        
        self.navigationController?.navigationBar.isTranslucent = false
        let initialSetupDone =  UserDefaults.standard.value(forKey: "initialSetupDone") as? Bool
        
      
        if Auth.auth().currentUser != nil && initialSetupDone != nil {
            /* string is not blank */
            
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            appdelegate.showMainTabbar()
        } else {
            let initialQuestionsViewController = self.storyboard?.instantiateViewController(withIdentifier: "InitialQuestionsViewController") as! InitialQuestionsViewController
            self.present(initialQuestionsViewController, animated: true, completion: nil)
        }
        

        
        self.ActivityIndicator.hidesWhenStopped = true
        
       if (UserDefaults.standard.value(forKey: "email")) != nil
        {
            txtEmailName.text = UserDefaults.standard.value(forKey: "email") as? String
            txtPassword.text = UserDefaults.standard.value(forKey: "password") as? String
        }
        
        let attrs = [
            convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor) : UIColor.white,
            convertFromNSAttributedStringKey(NSAttributedString.Key.underlineStyle) : 1] as [String : Any]
        
        let attributedString1 = NSMutableAttributedString(string:"")
        let attributedString2 = NSMutableAttributedString(string:"")

        
        let signUpTitleStr = NSMutableAttributedString(string:"Sign Up?", attributes:convertToOptionalNSAttributedStringKeyDictionary(attrs))
        attributedString1.append(signUpTitleStr)
        btnSignUp.setAttributedTitle(attributedString1, for: UIControl.State())
        
        let buttonTitleStr = NSMutableAttributedString(string:"Forgot password?", attributes:convertToOptionalNSAttributedStringKeyDictionary(attrs))
        attributedString2.append(buttonTitleStr)
        forgetPasswordBtn.setAttributedTitle(attributedString2, for: UIControl.State())

    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    @IBAction func forgetPasswordBtnClicked(_ sender: UIButton) {
        
        func configurationTextField(_ textField: UITextField!)
        {
            textField.placeholder = "Email Address"
            textField.addTarget(self, action: #selector(ViewController.textChanged(_:)), for: .editingChanged)

            tField = textField
        }
        
        let alert = UIAlertController(title: "Recover your username and password", message: "Enter your Email Address", preferredStyle: .alert)
        
        alert.addTextField(configurationHandler: configurationTextField)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:nil))
        let action = UIAlertAction(title: "Send", style: .default, handler:{ (UIAlertAction) in
            
             self.forgetPassword()
        })
        
        alert.addAction(action)
        self.actionToEnable = action
        action.isEnabled = false
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func textChanged(_ sender:UITextField) {
        self.actionToEnable?.isEnabled = (self.isValidEmail(sender.text!))
    }
    
    func forgetPassword()
    {
        
        Auth.auth().sendPasswordReset(withEmail: self.tField.text ?? "nil") { error in

            if let errorL = error {
                self.ActivityIndicator.stopAnimating()
                self.ShowMessage(errorL.localizedDescription as NSString)
            } else {
                self.ActivityIndicator.stopAnimating()
                self.ShowMessage("Your username and password has just been sent to the registered email address.")
            }
        }
    }
    
    func emailVerification() -> Void {
        Auth.auth().currentUser?.sendEmailVerification { (error) in
            if let errorL = error {
                self.ShowMessage(errorL.localizedDescription as NSString)
            } else {
                self.ShowMessage("We have also sent you an email for verification. Please take a moment to verify your email.")
            }
        }
    }
    
    func isValidEmail(_ testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func ShowMessage(_ message:NSString)
    {
        let alert = UIAlertController (title:"Alert", message:message as String, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title:"OK", style: UIAlertAction.Style.default, handler: nil))
        
        self.present(alert, animated:true, completion: nil)
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override var prefersStatusBarHidden : Bool
    {
        return false
    }
    
    //MARK: Textfield Delegate Method
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if(textField == txtEmailName)
        {
            txtPassword.becomeFirstResponder()
        }
        else
        {
            txtPassword.resignFirstResponder()
            
            self .parsingurl()
            
        }
        
        return true
    }
    
    func parsingurl()
    {
        if(txtEmailName.text == "")
        {
            txtEmailName.shake(10, withDelta: 10.0)
            
            /*
             // Shake with the default speed
             self.textField.shake(10,              // 10 times
             withDelta: 5.0   // 5 points wide
             )
             
             // Shake with a custom speed
             self.textField.shake(10,              // 10 times
             withDelta: 5.0,  // 5 points wide
             speed: 0.03      // 30ms per shake
             )
             
             // Shake with a custom speed and direction
             self.textField.shake(10,              // 10 times
             withDelta: 5.0,  // 5 points wide
             speed: 0.03,     // 30ms per shake
             shakeDirection: ShakeDirection.Vertical
             )
             */
        }
        else if(txtPassword.text == "")
        {
            txtPassword.shake(10, withDelta: 10.0)
        }
        else
        {
            txtPassword.resignFirstResponder()
            txtEmailName.resignFirstResponder()

            loginButton.startLoadingAnimation()
            
            Auth.auth().signIn(withEmail: txtEmailName.text ?? "nil", password: txtPassword.text ?? "nil") { [weak self] user, error in
                guard let strongSelf = self else { return }

                if let error = error {
                    strongSelf.loginButton.startFinishAnimation(2, completion: ( {
                        //self.ShowMessage(data["message"]! as! NSString)
                        strongSelf.ShowMessage(error.localizedDescription as NSString)
                        strongSelf.txtPassword.text = ""
                    }));
                } else {
                    let currentUser = Auth.auth().currentUser
                    
                    if(!(currentUser?.isEmailVerified)!){
                        strongSelf.emailVerification()
                    }
                    
                    
                    UserDefaults.standard.set(currentUser?.email, forKey: "email")
                    
                    UserDefaults.standard.synchronize()
                    
                    strongSelf.loginButton.startFinishAnimation(2, completion: ( {
                        //self.ShowMessage(data["message"]! as! NSString)
                        let initialSetupDone =  UserDefaults.standard.value(forKey: "initialSetupDone") as? Bool
                        
                        if initialSetupDone != nil {
                            let appdelegate = UIApplication.shared.delegate as! AppDelegate
                            appdelegate.showMainTabbar()
              
                        } else {
                            let initialQuestionsViewController = strongSelf.storyboard?.instantiateViewController(withIdentifier: "InitialQuestionsViewController") as! InitialQuestionsViewController
                            strongSelf.present(initialQuestionsViewController, animated: true, completion: nil)
                        }
                    }));
                }
            }
        }
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromNSAttributedStringKey(_ input: NSAttributedString.Key) -> String {
	return input.rawValue
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}
