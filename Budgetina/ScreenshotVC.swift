//
//  ScreenshotVC.swift
//  Budgetina
//
//  Created by Ovedgroup LLC on 09/01/16.
//  Copyright © 2016 Ovedgroup LLC. All rights reserved.
//


import UIKit


class ScreenshotVC: UIViewController,PiechartDelegate
{
    @IBOutlet var viewChart: UIView!
    
    @IBOutlet var viewPieChart: UIView!
    
    @IBOutlet var lblIncome: UILabel!
    @IBOutlet var lblEssential: UILabel!
    @IBOutlet var lblLifestyle: UILabel!
    @IBOutlet var lblFinancial: UILabel!
    @IBOutlet var lblMessage: UILabel!
    
    
    var dataArray = NSMutableArray()
    var dataIncomeAmount = NSMutableArray()
    var dataEssentialAmount = NSMutableArray()
    var dataLifestyleAmount = NSMutableArray()
    var dataFinancialAmount = NSMutableArray()
    
    override func viewDidLoad()
    {
        
        super.viewDidLoad()
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.isNavigationBarHidden = true
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        let selectedPickerMonth  =  UserDefaults.standard.value(forKey: "SelectedMonthYear") as! String
        
        var views: [String: UIView] = [:]
        
        dataArray = DBHandler.getDataUsingQuery("select * from Income where incomemonthyear like '%\(selectedPickerMonth)'")
        
        dataIncomeAmount = DBHandler.getDataUsingQuery(NSString(format:"select sum(amount) total from Income where incomemonthyear like '%%%@'",selectedPickerMonth) as String)
        NSLog("%@", dataIncomeAmount)
        
        dataEssentialAmount = DBHandler.getDataUsingQuery(NSString(format: "select sum(amount) total from Spending where spendingmonthyear like '%%%@' AND parentcategoryid = '1'",selectedPickerMonth) as String)
        NSLog("%@", dataEssentialAmount)
        
        
        dataLifestyleAmount = DBHandler.getDataUsingQuery(NSString(format: "select sum(amount) total from Spending where spendingmonthyear like '%%%@' AND parentcategoryid = '2'", selectedPickerMonth) as String)
        NSLog("%@", dataLifestyleAmount)
        
        
        dataFinancialAmount = DBHandler.getDataUsingQuery(NSString(format: "select sum(amount) total from Spending where spendingmonthyear like '%%%@' AND parentcategoryid = '3'", selectedPickerMonth) as String)
        NSLog("%@", dataFinancialAmount)
        
        if(((dataIncomeAmount[0] as! NSDictionary)["total"] as! String) == ""  && ((dataEssentialAmount[0] as! NSDictionary)["total"] as! String) == "" && ((dataLifestyleAmount[0] as! NSDictionary)["total"] as! String) == "" && ((dataFinancialAmount[0] as! NSDictionary)["total"] as! String) == "")
        {
            lblMessage.isHidden = false
            viewChart.isHidden = true
            self.view.viewWithTag(101)?.removeFromSuperview()
            
        }
        else
        {
            lblMessage.isHidden = true
            viewChart.isHidden = false
            
            
            var IncomeAmount = 0.0 as CGFloat
            if dataIncomeAmount.count > 0{
                let st = (dataIncomeAmount[0] as! NSDictionary)["total"] as! String
                if NSString(string: st).length > 0{
                    IncomeAmount = CGFloat(Float(st)!)
                }
            }
            
            var EssentialAmount = 0.0 as CGFloat
            if self.dataEssentialAmount.count > 0{
                
                let st = (self.dataEssentialAmount[0] as! NSDictionary)["total"] as! String
                if NSString(string: st).length > 0{
                    EssentialAmount = CGFloat(Float(st)!)
                }
                
                
            }
            
            var LifestyleAmount = 0.0 as CGFloat
            if self.dataLifestyleAmount.count > 0{
                
                let st = (self.dataLifestyleAmount[0] as! NSDictionary)["total"] as! String
                if NSString(string: st).length > 0{
                    LifestyleAmount = CGFloat(Float(st)!)
                }
                
            }
            
            var FinanceAmount = 0.0 as CGFloat
            if self.dataFinancialAmount.count > 0{
                
                let st = (dataFinancialAmount[0] as! NSDictionary)["total"] as! String
                if NSString(string: st).length > 0{
                    FinanceAmount = CGFloat(Float(st)!)
                }
            }
            
            var MonthIncome = Piechart.Slice()
            MonthIncome.value = 100 - (FinanceAmount/IncomeAmount + LifestyleAmount/IncomeAmount + EssentialAmount/IncomeAmount)
            MonthIncome.color = UIColor(red: 23/255, green: 159/255, blue: 152/255, alpha: 1.0)
            
            var MonthFinancial = Piechart.Slice()
            MonthFinancial.value = FinanceAmount/IncomeAmount
            MonthFinancial.color = UIColor(red: 250/255, green: 199/255, blue: 26/255, alpha: 1.0)
            
            var MonthLifestyle = Piechart.Slice()
            MonthLifestyle.value = LifestyleAmount/IncomeAmount
            MonthLifestyle.color = UIColor(red: 54/255, green: 130/255, blue: 148/255, alpha: 1.0)
            
            
            var MonthEssentials = Piechart.Slice()
            MonthEssentials.value = EssentialAmount/IncomeAmount
            MonthEssentials.color = UIColor(red: 255/255, green: 107/255, blue: 65/255, alpha: 1.0)
            
            let IncomePercentage = 100 - (FinanceAmount/IncomeAmount + LifestyleAmount/IncomeAmount + EssentialAmount/IncomeAmount) * 100
            lblIncome.text = NSString(format: "INCOME %.2f%%", IncomePercentage) as String
            
            let FinancialPercentage = FinanceAmount/IncomeAmount * 100.00
            lblFinancial.text = NSString(format: "FINANCIAL %.2f%%", FinancialPercentage) as String
            
            let LifestylePercentage = LifestyleAmount/IncomeAmount * 100.00
            lblLifestyle.text = NSString(format: "LIFESTYLE %.2f%%", LifestylePercentage) as String
            
            let EssentialPercentage = EssentialAmount/IncomeAmount * 100.00
            lblEssential.text = NSString(format: "ESSENTIALS %.2f%%", EssentialPercentage) as String
            
            
            
            let piechart = Piechart()
            piechart.delegate = self
            piechart.activeSlice = 0
            piechart.slices = [MonthIncome, MonthFinancial, MonthLifestyle , MonthEssentials]
            
            piechart.translatesAutoresizingMaskIntoConstraints = false
            /*if UIScreen.mainScreen().bounds.size.height == 480
            {
              var frame = CGRect()
                frame.size.width = view.frame.size.width - 80
                frame.size.height = viewPieChart.frame.size.height - 100
                viewPieChart.frame = frame
            }*/
            viewPieChart.addSubview(piechart)
            views["piechart"] = piechart
            viewPieChart.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-[piechart]-|", options: [], metrics: nil, views: views))
            viewPieChart.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[piechart(==202)]", options: [], metrics: nil, views: views))

        }
        
    
        
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
       func setSubtitle(_ slice: Piechart.Slice) -> String
    {
        return "\(Int(slice.value * 100))% \(String(describing: slice.text))"
    }


}
