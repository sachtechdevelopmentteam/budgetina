//
//  HomeVC.swift
//  Budgetina
//
//  Created by Ovedgroup LLC on 28/10/15.
//  Copyright © 2015 Ovedgroup LLC. All rights reserved.
//

import UIKit
import MessageUI
import StoreKit
import Foundation

class HomeVC: UIViewController, UITableViewDataSource, UITableViewDelegate, MonthYearDelegate, UITextFieldDelegate,UIGestureRecognizerDelegate,MFMailComposeViewControllerDelegate,SKProductsRequestDelegate,SKPaymentTransactionObserver{
    
    
    @IBOutlet var viewCategoryChart: UIView!
    @IBOutlet var tableCategory: UITableView!
    
    @IBOutlet var lblIncome : UILabel!
    
    @IBOutlet var imageEssential : UIImageView!
    @IBOutlet var imageLifestyle : UIImageView!
    @IBOutlet var imageFinancial : UIImageView!
    
    @IBOutlet var lblEssentialVal : UILabel!
    @IBOutlet var lblLifestyleVal : UILabel!
    @IBOutlet var lblFinancialVal : UILabel!
    @IBOutlet var imageIncomeChart: UIImageView!
    
    @IBOutlet var lblEssentialAmount : UILabel!
    @IBOutlet var lblLifestyleAmount : UILabel!
    @IBOutlet var lblFinancialAmount : UILabel!
    
    var _currentIndexPath : IndexPath!
    var _currentTextfield = UITextField()
    var _currentTextfieldValue = 0.0 as CGFloat
    
    var dataIncome: NSMutableArray = NSMutableArray()
    var dataEssentials : NSMutableArray = NSMutableArray()
    var dataLifestyle : NSMutableArray = NSMutableArray()
    var dataFinancial : NSMutableArray = NSMutableArray()
    var dataSpending : NSArray = NSArray()
    var dataTotalValues : NSArray = NSArray()
    
    var arrRepeatSpending = NSMutableArray()
    var arrRepeatIncome = NSMutableArray()
    var arrcontentobject = NSMutableArray()
    
    let Expandablearray : NSMutableArray = NSMutableArray()
    
    let Categoryarray = NSArray(array:["INCOME","ESSENTIALS","LIFESTYLE","FINANCIAL FUTURE"])
    
    var totalIncomeAmount = NSString()
    
    var TotalExpencevalue = 0.0 as CGFloat
    var totalRemainingValue = 0.0 as CGFloat
    
    let date = Date()
    
    //For App Purchase
    
    let defaults = UserDefaults.standard
    
    var product_id = NSString()
    
    var ScreenshotImage = UIImage()
    var ScreenshotChart = UIImage()
    
    @IBAction func btnGetTutorialAction(_ sender: UIButton)
    {
        if(defaults.bool(forKey: "purchased"))
        {
            
        }
        else
        {
            
        }
        
        if (SKPaymentQueue.canMakePayments())
        {
            let productID:NSSet = NSSet(object: self.product_id)
            let productsRequest:SKProductsRequest = SKProductsRequest(productIdentifiers: productID as! Set<String>)
            productsRequest.delegate = self
            productsRequest.start()
            print("Fething Products")
        }
        else
        {
            print("can't make purchases");
        }
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        print("today date is-------%@", date)
        
        //App Purchase
        
        product_id = ""
        
        SKPaymentQueue.default().add(self)
        
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 200/225, green: 185/225, blue: 207/225, alpha: 1.0)
        self.automaticallyAdjustsScrollViewInsets = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        let rightBarButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.add, target: self, action: #selector(HomeVC.addNewMonth))
        self.navigationItem.rightBarButtonItem = rightBarButton
        
        let button = UIButton(type:.system) as UIButton
        button.setImage(UIImage(named: "email_icon.PNG"), for: UIControl.State())
        button.addTarget(self, action:#selector(HomeVC.sendEmail), for: UIControl.Event.touchUpInside)
        button.frame=CGRect(x: 0, y: 0, width: 22, height: 22)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        
        let delayTime = DispatchTime.now() + Double(Int64(0.2 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            self.dataRefresh()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        let tapGesture1 = UITapGestureRecognizer(target: self, action: #selector(HomeVC.ClickOnChart(_:)))
        imageEssential.tag = 1001
        imageEssential.addGestureRecognizer(tapGesture1)
        
        tapGesture1.delegate = self
        
        
        let tapGesture2 = UITapGestureRecognizer(target: self, action: #selector(HomeVC.ClickOnChart(_:)))
        imageLifestyle.tag = 1002
        imageLifestyle.addGestureRecognizer(tapGesture2)
        
        tapGesture2.delegate = self
        
        
        let tapGesture3 = UITapGestureRecognizer(target: self, action: #selector(HomeVC.ClickOnChart(_:)))
        imageFinancial.tag = 1003
        imageFinancial.addGestureRecognizer(tapGesture3)
        tapGesture3.delegate = self
    }
    
    @objc func ClickOnChart(_ tapGesture:UITapGestureRecognizer)
    {
        var array = NSArray()
        
        if(tapGesture.view?.tag == 1001)
        {
            let predicate = NSPredicate(format: "amount != '0'")
            array = dataEssentials.filtered(using: predicate) as NSArray
        }
        else if(tapGesture.view?.tag == 1002)
        {
            let predicate = NSPredicate(format: "amount != '0'")
            array = dataLifestyle.filtered(using: predicate) as NSArray
        }
        else if(tapGesture.view?.tag == 1003)
        {
            let predicate = NSPredicate(format: "amount != '0'")
            array = dataFinancial.filtered(using: predicate) as NSArray
        }
        
        let SubCategoryVC = self.storyboard?.instantiateViewController(withIdentifier: "ChartSubCategoryVC") as! ChartSubCategoryVC
        SubCategoryVC.SubCategoryArray = NSMutableArray()
        SubCategoryVC.SubCategoryArray.addObjects(from: array as [AnyObject])
        self.navigationController?.pushViewController(SubCategoryVC, animated: true)
        
    }
    
    func dataRefresh()
    {
        
        if(UserDefaults.standard.value(forKey: "SelectedMonthYear") == nil)
        {
            //            let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(1 * Double(NSEC_PER_SEC)))
            //            dispatch_after(delayTime, dispatch_get_main_queue()) {
            
            self.imageEssential.frame.size.height = 0
            self.imageLifestyle.frame.size.height = 0
            self.imageFinancial.frame.size.height = 0
            self.addNewMonth()
            
            
            //            }
        }
        else
        {
            let navTitle = UserDefaults.standard.value(forKey: "SelectedMonthYear") as! String
            
            dataIncome = DBHandler.getDataUsingQuery("select cc.*, max(cc.amount) from (  SELECT c.*, (case when i.incomemonthyear = '\(navTitle)' then  i.amount else 0 end) amount from incomecategories c left join income i on i.i_categoryid =  c.i_categoryid ) cc where amount != -1 group by i_categoryid")
            
            dataEssentials = DBHandler.getDataUsingQuery("select cc.*, max(cc.amount) from (  SELECT c.*, (case when s.spendingmonthyear = '\(navTitle)' then  s.amount else 0 end) amount from Categories c left join spending s on s.categoryid =  c.categoryid  where c.parentcategoryid=1 ) cc where amount != -1 group by cc.categoryid ")
            
            dataLifestyle = DBHandler.getDataUsingQuery("select cc.*, max(cc.amount) from (  SELECT c.*, (case when s.spendingmonthyear = '\(navTitle)' then  s.amount else 0 end) amount from Categories c left join spending s on s.categoryid =  c.categoryid  where c.parentcategoryid=2) cc where amount != -1 group by cc.categoryid ")
            
            dataFinancial = DBHandler.getDataUsingQuery("select cc.*, max(cc.amount) from (  SELECT c.*, (case when s.spendingmonthyear = '\(navTitle)' then  s.amount else 0 end) amount from Categories c left join spending s on s.categoryid =  c.categoryid  where c.parentcategoryid=3) cc where amount != -1 group by cc.categoryid ")
            
            self.navigationItem.title = navTitle
            
            //let predicate = NSPredicate(format: "spendingmonthyear = '\(navTitle)' || spendingmonthyear = ''")
            //dataEssentials = dataEssentials.filtered(using: predicate) as NSArray
            //dataLifestyle = dataLifestyle.filtered(using: predicate) as NSArray
            //dataFinancial = dataFinancial.filtered(using: predicate) as NSArray
            
            //let predicateIncome = NSPredicate(format: "incomemonthyear = '\(navTitle)' || incomemonthyear = ''")
            
            //dataIncome = dataIncome.filtered(using: predicate) as NSArray
            
            
            let query = "select sum(amount) amount, parentcategoryid from spending where amount!=-1 AND spendingmonthyear = '\(navTitle)'  group by parentcategoryid   UNION select sum(amount) amount, '0' parentcategoryid from income where incomemonthyear = '\(navTitle)' AND amount!=-1"
            
            dataTotalValues = DBHandler.getDataUsingQuery(query)
            
            var totalIncome = 0.0 as Float
            var totalExpense = 0.0 as Float
            
            var EssentialAmountTotal = 0.0 as CGFloat
            var LifestyleAmountTotal = 0.0 as CGFloat
            var FinancialAmountTotal = 0.0 as CGFloat
            var amount = NSString()
            
            
            var predicate = NSPredicate(format: "parentcategoryid= '0'")
            var array = dataTotalValues.filtered(using: predicate) as NSArray
            if array.count > 0{
                amount = (array[0] as! NSDictionary).value(forKey: "amount") as! String as NSString
                amount = amount == "" ? "0" : amount
                totalIncome = (amount as NSString).floatValue
                
            }else
            {
                lblIncome.text = "Income: $0.00"
            }
            
            //70
            //ESSENTIAL
            predicate = NSPredicate(format: "parentcategoryid= '1'")
            array = dataTotalValues.filtered(using: predicate) as NSArray
            
            if array.count > 0
            {
                var amount = (array[0] as! NSDictionary).value(forKey: "amount") as! String
                amount = amount == "" ? "0" : amount
                let amt = (amount as NSString).floatValue
                totalExpense += amt
                
                EssentialAmountTotal = CGFloat(amt)
                
                if totalIncome > 0{
                    let percentage = amt/totalIncome * 100.00
                    
                    lblEssentialVal.text = NSString(format: "%.2f%%\nESSENTIALS", percentage) as String
                    
                    
                    lblEssentialAmount.text = NSString(format :"$%.2f",EssentialAmountTotal) as String
                    
                    let totalHeight = viewCategoryChart.frame.size.height - 65
                    //let totalHeight = viewCategoryChart.frame.size.height - lblIncome.frame.size.height
                    
                    print("TotalHeight----%@",totalHeight)
                    var vHeight = Float(totalHeight) * percentage / 100
                    print("TotalvHeight----%@",vHeight)
                    if vHeight > Float(totalHeight) {
                        vHeight = Float(totalHeight) - 26
                    }
                    
                    var r = imageEssential.frame
                    r.size.height = CGFloat(vHeight)
                    r.origin.y = totalHeight - CGFloat(vHeight) + 26
                    imageEssential.frame = r
                    print(r)
                    print(imageEssential.frame)
                }
            }
            else
            {
                lblEssentialVal.text = "0%\nESSENTIALS"
                lblEssentialAmount.text = "$0.00"
                
                var r = imageEssential.frame
                r.size.height = 0
                let totalHeight = viewCategoryChart.frame.size.height - 65
                r.origin.y = totalHeight
                imageEssential.frame = r
            }
            
            
            //lifestyle
            predicate = NSPredicate(format: "parentcategoryid= '2'")
            array = dataTotalValues.filtered(using: predicate) as NSArray
            if array.count > 0
            {
                var amount = (array[0] as! NSDictionary).value(forKey: "amount") as! String
                amount = amount == "" ? "0" : amount
                let amt = (amount as NSString).floatValue
                totalExpense += amt
                
                LifestyleAmountTotal  = CGFloat(amt)
                
                
                if totalIncome > 0{
                    
                    let percentage = amt/totalIncome * 100.00
                    lblLifestyleVal.text = NSString(format: "%.2f%%\nLIFESTYLE", percentage) as String
                    
                    
                    lblLifestyleAmount.text = NSString(format:"$%.2f",LifestyleAmountTotal) as String
                    
                    let totalHeight = viewCategoryChart.frame.size.height - 65.00
                    var vHeight = Float(totalHeight) * percentage / 100
                    if vHeight > Float(totalHeight) {
                        vHeight = Float(totalHeight) - 26
                    }
                    
                    
                    var r = imageLifestyle.frame
                    r.size.height = CGFloat(vHeight)
                    r.origin.y = totalHeight - CGFloat(vHeight) + 26
                    imageLifestyle.frame = r
                }
            }
            else
            {
                lblLifestyleVal.text = "0%\nLIFESTYLE"
                lblLifestyleAmount.text = "$0.00"
                
                
                
                var r = imageLifestyle.frame
                r.size.height = 0
                let totalHeight = viewCategoryChart.frame.size.height - 65.00
                r.origin.y = totalHeight
                imageLifestyle.frame = r
            }
            
            
            //Financial
            predicate = NSPredicate(format: "parentcategoryid= '3'")
            array = dataTotalValues.filtered(using: predicate) as NSArray
            if array.count > 0
            {
                var amount = (array[0] as! NSDictionary).value(forKey: "amount") as! String
                amount = amount == "" ? "0" : amount
                let amt = (amount as NSString).floatValue
                totalExpense += amt
                
                FinancialAmountTotal  = CGFloat(amt)
                
                if totalIncome > 0
                {
                    let percentage = amt/totalIncome * 100.00
                    lblFinancialVal.text = NSString(format: "%.2f%%\nFINANCIAL", percentage) as String
                    
                    lblFinancialAmount.text = NSString(format:"$%.2f",FinancialAmountTotal) as String
                    
                    let totalHeight = viewCategoryChart.frame.size.height - 65.00
                    var vHeight = Float(totalHeight) * percentage / 100
                    if vHeight > Float(totalHeight) {
                        vHeight = Float(totalHeight) - 26
                    }
                    
                    var r = imageFinancial.frame
                    r.size.height = CGFloat(vHeight)
                    r.origin.y = totalHeight - CGFloat(vHeight) + 26
                    imageFinancial.frame = r
                    
                }
                
            }
            else
            {
                lblFinancialVal.text = "0%\nFINANCIAL"
                lblFinancialAmount.text = "$0.00"
                
                var r = imageFinancial.frame
                r.size.height = 0
                let totalHeight = viewCategoryChart.frame.size.height - 65.00
                r.origin.y = totalHeight
                imageFinancial.frame = r
            }
            
            if totalIncome > 0
            {
                let incomePercentage = 100 - (totalExpense / totalIncome) * 100
                print(incomePercentage)
                
                let totalHeight = viewCategoryChart.frame.size.height - 39.00
                var vHeight = Float(totalHeight) * incomePercentage / 100
                if vHeight > Float(totalHeight)
                {
                    vHeight = Float(totalHeight) - 39
                }
                var r = imageIncomeChart.frame
                r.size.height = CGFloat(vHeight)
                r.origin.y = totalHeight - CGFloat(vHeight)
                imageIncomeChart.frame = r
                
            }
            else
            {
                var r = imageIncomeChart.frame
                r.size.height = 0
                let totalHeight = viewCategoryChart.frame.size.height - 39.00
                r.origin.y = totalHeight
                imageIncomeChart.frame = r
            }
            
            TotalExpencevalue = EssentialAmountTotal + LifestyleAmountTotal + FinancialAmountTotal
            
            if(totalIncome > 0)
            {
                totalRemainingValue = CGFloat(totalIncome) - EssentialAmountTotal - LifestyleAmountTotal - FinancialAmountTotal
                lblIncome.text = NSString(format: "REMAINING INCOME: $%.2f", totalRemainingValue) as String
            }
            else
            {
                lblIncome.text = "REMAINING INCOME: $0.00"
            }
            
            
            arrRepeatSpending = DBHandler.getDataFromTable("RepeatSpending")
            
            arrRepeatIncome = DBHandler.getDataFromTable("RepeatIncome")
            
            print(arrRepeatIncome)
            
            
        }
        
    }
    
    @objc func addNewMonth(){
        print("addNewMonth")
        let monthYearPicker = self.storyboard?.instantiateViewController(withIdentifier: "MonthYearPickerVC") as! MonthYearPickerVC
        monthYearPicker.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        monthYearPicker.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        monthYearPicker.delegate = self
        self.present(monthYearPicker, animated: true, completion: nil)
    }
    
    @objc func sendEmail()
    {
        
        let yearlyOverView = self.storyboard?.instantiateViewController(withIdentifier: "ScreenshotVC") as! ScreenshotVC
        self.addChild(yearlyOverView)
        self.view.addSubview(yearlyOverView.view)
        
        let delayTime = DispatchTime.now() + Double(Int64(0 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: delayTime)
        {
            let width = yearlyOverView.viewChart.frame.size.width
            let height = yearlyOverView.viewChart.frame.size.height
            
            let grabRect: CGRect = CGRect(x: yearlyOverView.viewChart.frame.origin.x, y: yearlyOverView.viewChart.frame.origin.y, width: width, height: height)
            //for retina displays
            if UIScreen.main.responds(to: #selector(NSDecimalNumberBehaviors.scale)) {
                UIGraphicsBeginImageContextWithOptions(grabRect.size, false, UIScreen.main.scale)
            }
            else {
                UIGraphicsBeginImageContext(grabRect.size)
            }
            let ctx: CGContext = UIGraphicsGetCurrentContext()!
            ctx.translateBy(x: -grabRect.origin.x, y: -grabRect.origin.y)
            self.view.layer.render(in: ctx)
            self.ScreenshotChart = UIGraphicsGetImageFromCurrentImageContext()!
            UIGraphicsEndImageContext()
            //UIImageWriteToSavedPhotosAlbum(self.ScreenshotChart, nil, nil, nil)
            
            yearlyOverView.view.removeFromSuperview()
            yearlyOverView.removeFromParent()
            
            self.navigationController?.isNavigationBarHidden = false
            
            self.captureHomescreenchart()
            
            self.PdfGenerator()
            
            let mailComposeViewController = self.configuredMailComposeViewController()
            if MFMailComposeViewController.canSendMail()
            {
                self.present(mailComposeViewController, animated: true, completion: nil)
            }
            else
            {
                self.showSendMailErrorAlert()
            }
        }
        
    }
    
    
    func monthYearSelected(_ month:NSString, year:NSString)
    {
        UserDefaults.standard.set(NSString(format: "%@  %@", month, year), forKey: "SelectedMonthYear")
        UserDefaults.standard.synchronize()
        
        
        arrRepeatSpending = DBHandler.getDataFromTable("RepeatSpending")
        if arrRepeatSpending.count > 0
        {
            
            for i in 0..<arrRepeatSpending.count
            {
                
                
                let dic = arrRepeatSpending.object(at: i) as! NSMutableDictionary
                let monthyear = UserDefaults.standard.object(forKey: "SelectedMonthYear") as!NSString
                print(monthyear)
                print("%@",self.arrRepeatSpending)
                
                
                let amount = dic.value(forKey: "amount") as! NSString
                
                
                
                
                var query = ""
                if (DBHandler.checkMonthAlreadyAdded(forCategory: "select * from spending where categoryid=\(dic.value(forKey: "categoryid")!) AND spendingmonthyear='\(monthyear)'")){
                    print("available so update")
                    //query = "update spending set amount = \(amount) where categoryid=\(dic.valueForKey("categoryid")!) AND spendingmonthyear='\(monthyear)'"
                }else{
                    print("not available so insert")
                    query = "insert into spending values(null,\(amount), \(dic.value(forKey: "categoryid")!), '\(monthyear)', \(dic.value(forKey: "parentcategoryid")!), datetime())"
                }
                
                
                
                if(DBHandler.genericQuery(query))
                {
                    print("updated successfull")
                }else{
                    print("query failed")
                }
                
            }
            
            
        }
        
        arrRepeatIncome = DBHandler.getDataFromTable("RepeatIncome")
        
        if arrRepeatIncome.count > 0
        {
            
            for i in 0..<arrRepeatIncome.count
            {
                let dic = arrRepeatIncome.object(at: i) as! NSMutableDictionary
                let monthyear = UserDefaults.standard.object(forKey: "SelectedMonthYear") as! NSString
                print(monthyear)
                print("%@",self.arrRepeatIncome)
                
                let amount = dic.value(forKey: "amount") as! NSString
                
                
                var query = ""
                if (DBHandler.checkMonthAlreadyAdded(forCategory: "select * from income where i_categoryid=\(dic.value(forKey: "i_categoryid")!) AND incomemonthyear='\(monthyear)'")){
                    //print("available so update")
                    //query = "update income set amount = \(amount) where i_categoryid=\(dic.valueForKey("i_categoryid")!) AND incomemonthyear='\(monthyear)'"
                }else{
                    print("not available so insert")
                    query = "insert into income values(null, '\(monthyear)',\(amount), datetime(), \(dic.value(forKey: "i_categoryid")!))"
                }
                
                if(DBHandler.genericQuery(query))
                {
                    print("updated successfull")
                }else{
                    print("query failed")
                }
                
            }
            
        }
        
        
        
        self.dataRefresh()
    }
    
    
    func captureHomescreenchart()
    {
        let width = viewCategoryChart.frame.size.width
        let Height = viewCategoryChart.frame.size.height
        
        let grabRect: CGRect = CGRect(x: viewCategoryChart.frame.origin.x, y: viewCategoryChart.frame.origin.y, width: width, height: Height)
        //for retina displays
        if UIScreen.main.responds(to: #selector(NSDecimalNumberBehaviors.scale)) {
            UIGraphicsBeginImageContextWithOptions(grabRect.size, false, UIScreen.main.scale)
        }
        else {
            UIGraphicsBeginImageContext(grabRect.size)
        }
        let ctx: CGContext = UIGraphicsGetCurrentContext()!
        ctx.translateBy(x: -grabRect.origin.x, y: -grabRect.origin.y)
        self.view.layer.render(in: ctx)
        ScreenshotImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        //UIImageWriteToSavedPhotosAlbum(ScreenshotImage, nil, nil, nil)
        
    }
    
    // MARK: Mail Composer Delegate Method
    
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        
        let documentPath : AnyObject = NSSearchPathForDirectoriesInDomains(.documentDirectory,.userDomainMask,true)[0] as NSString
        
        // let FilePath = documentPath.appendingPathComponent("/SpendingReport.pdf")
        let FilePath = documentPath.appending("/SpendingReport.pdf")
        
        let pdfFile = try? Data(contentsOf: URL(fileURLWithPath: FilePath))
        
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self
        // mailComposerVC.setToRecipients(["contact@budgetina.com"])
        mailComposerVC.setSubject("Budgetina Spending Report")
        let Messagebody = ""
        mailComposerVC.setMessageBody(Messagebody, isHTML: false)
        mailComposerVC.addAttachmentData(pdfFile!, mimeType:"pdf", fileName: "SpendingReport.pdf")
        
        return mailComposerVC
        
    }
    
    func showSendMailErrorAlert()
    {
        let sendMailErrorAlert = UIAlertView(title: "Could Not Send Email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again", delegate: self, cancelButtonTitle: "OK")
        sendMailErrorAlert.show()
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        
        switch result
        {
        case .cancelled:
            NSLog("Mail Cancelled")
            
        case .saved:
            NSLog("Mail Saved")
            
        case .failed:
            NSLog("Mail Sent Failure: %@",[error!.localizedDescription])
            
        case .sent:
            NSLog("Mail Sent Sucessfully")
        }
        
        controller.dismiss(animated: true, completion: nil)
    }
    
    
    // MARK: TableView Delegate Method
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return Categoryarray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if(Expandablearray.contains(section))
        {
            if(section == 0)
            {
                return dataIncome.count
            }
            else if(section == 1)
            {
                return dataEssentials.count
            }
            else if(section == 2)
            {
                return dataLifestyle.count
            }
            else if(section == 3)
            {
                return dataFinancial.count
            }
        }
        else
        {
            return 0
        }
        
        return 0
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        
        let  Headerview = UIView(frame:CGRect(x: 0, y: 0, width: view.frame.size.width, height: tableView.frame.size.height/4))
        //Headerview.layer.borderWidth = 1
        Headerview.backgroundColor = UIColor(red: 200/225, green: 185/225, blue: 207/225, alpha: 1.0)
        //Headerview.autoresizingMask = UIViewAutoresizing.flexibleHeight; UIViewAutoresizing.flexibleWidth
        Headerview.autoresizingMask = [.flexibleHeight,.flexibleWidth]
        
        
        let headerlabel = UILabel(frame:CGRect(x: 0, y: 0,width: view.frame.size.width-30, height: tableView.frame.size.height/4))
        // headerlabel.layer.borderWidth = 1
        headerlabel.textAlignment = .center
        headerlabel.font = UIFont.boldSystemFont(ofSize: 16)
        let image = UIImage(named: "plus_icon.PNG") as UIImage?
        let btnframe = CGRect(x: 10, y: (tableView.frame.size.height/4)/2-9, width: 18, height: 18)
        let btnAdd = UIButton(frame:btnframe)
        btnAdd.setImage(image, for: UIControl.State())
        headerlabel.addSubview(btnAdd)
        
        
        headerlabel.text = Categoryarray.object(at: section) as? String
        Headerview .addSubview(headerlabel)
        Headerview.tag = section
        
        let headerAmount = UILabel(frame:CGRect(x: UIScreen.main.bounds.size.width - 15 - 100, y: 0,width: 100, height: tableView.frame.size.height/4))
        headerAmount.tag = section + 101
        // headerlabel.layer.borderWidth = 1
        headerAmount.textAlignment = .right
        headerAmount.font = UIFont.boldSystemFont(ofSize: 14)
        Headerview.addSubview(headerAmount)
        
        
        let predicate = NSPredicate(format: "parentcategoryid= '\(section)'")
        print(predicate)
        let array = dataTotalValues.filtered(using: predicate)
        
        
        if array.count > 0
        {
            let dic = array[0] as? NSDictionary
            var amount = dic?["amount"] as! String
            
            amount = amount == "" ? "0" : amount
            
            
            
            //headerAmount.text = "$\(amount)"
            headerAmount.text = NSString(format: "$%.2f", Float(amount)!) as String
            
            
        }else
        {
            headerAmount.text = "$0.00"
        }
        
        
        
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(HomeVC.ClickOnCell(_:)))
        Headerview.addGestureRecognizer(tapGesture)
        
        return Headerview
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return tableView.frame.size.height/4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        let lblTitle = cell.contentView.viewWithTag(100) as! UILabel
        let textField = cell.contentView.viewWithTag(101) as! UITextField
        let imageview = cell.contentView.viewWithTag(103) as! UIImageView!
        let viewimg = cell.contentView.viewWithTag(104) as UIView?
        textField.delegate = self
        
        var dic = NSDictionary()
        
        
        if(indexPath.section == 0)
        {
            dic = self.dataIncome[indexPath.row] as! NSDictionary
            lblTitle.text = dic["i_categoryname"] as? String
            let amount = dic["amount"] as! String!
            if (amount?.characters.count == 0){
                textField.text = "$0"
            }else{
                //textField.text = "$\(amount)"
                textField.text = NSString(format: "$%.2f", Float(amount!)!) as String
            }
            
            let predicate = NSPredicate(format: "i_categoryid == %@",(dic["i_categoryid"] as? String)!)
            
            let filterarray = self.arrRepeatIncome.filtered(using: predicate)
            if filterarray.count > 0
            {
                imageview?.image = UIImage(named: "checked1.png")
            }
            else
            {
                if arrcontentobject.contains(indexPath)
                {
                    imageview?.image = UIImage(named: "checked1.png")
                }
                else
                {
                    imageview?.image = UIImage(named: "box_new.png")
                }
            }
        }
        else if(indexPath.section == 1)
        {
            dic = self.dataEssentials[indexPath.row] as! NSDictionary
            //dic = self.dataIncome[indexPath.row] as! NSDictionary
            lblTitle.text = dic["categoryname"] as? String
            let amount = dic["amount"] as! String!
            if (amount?.characters.count == 0){
                textField.text = "$0"
            }else{
                // textField.text = "$\(amount)"
                textField.text = NSString(format: "$%.2f", Float(amount!)!) as String
            }
            
            let predicate = NSPredicate(format: "categoryid == %@",(dic["categoryid"] as? String)!)
            
            let filterarray = self.arrRepeatSpending.filtered(using: predicate)
            if filterarray.count > 0
            {
                imageview?.image = UIImage(named: "checked1.png")
            }
            else
            {
                if arrcontentobject.contains(indexPath)
                {
                    imageview?.image = UIImage(named: "checked1.png")
                }
                else
                {
                    imageview?.image = UIImage(named: "box_new.png")
                }
            }
        }
        else if(indexPath.section == 2)
        {
            dic = self.dataLifestyle[indexPath.row] as! NSDictionary
            
            lblTitle.text = dic["categoryname"] as? String
            let amount = dic["amount"] as! String!
            if (amount?.count == 0){
                textField.text = "$0"
            }else{
                //textField.text = "$\(amount)"
                textField.text = NSString(format: "$%.2f", Float(amount!)!) as String
            }
            
            let predicate = NSPredicate(format: "categoryid == %@",(dic["categoryid"] as? String)!)
            
            let filterarray = self.arrRepeatSpending.filtered(using: predicate)
            if filterarray.count > 0
            {
                imageview?.image = UIImage(named: "checked1.png")
            }
            else
            {
                if arrcontentobject.contains(indexPath)
                {
                    imageview?.image = UIImage(named: "checked1.png")
                }
                else
                {
                    imageview?.image = UIImage(named: "box_new.png")
                }
            }
        }
        else if(indexPath.section == 3)
        {
            
            dic = self.dataFinancial[indexPath.row] as! NSDictionary
            lblTitle.text = dic["categoryname"] as? String
            let amount = dic["amount"] as! String!
            if (amount?.characters.count == 0){
                textField.text = "$0"
            }else{
                //textField.text = "$\(amount)"
                textField.text = NSString(format: "$%.2f", Float(amount!)!) as String
            }
            
            let predicate = NSPredicate(format: "categoryid == %@",(dic["categoryid"] as? String)!)
            
            let filterarray = self.arrRepeatSpending.filtered(using: predicate)
            if filterarray.count > 0
            {
                imageview?.image = UIImage(named: "checked1.png")
            }
            else
            {
                if arrcontentobject.contains(indexPath)
                {
                    imageview?.image = UIImage(named: "checked1.png")
                }
                else
                {
                    imageview?.image = UIImage(named: "box_new.png")
                }
            }
        }
        
        if _currentIndexPath != nil {
            if _currentIndexPath == indexPath {
                textField.becomeFirstResponder()
            }
        }
        
        let tapgesture = UITapGestureRecognizer(target: self, action: #selector(HomeVC.taponImage(_:)))
        tapgesture.delegate = self
        viewimg!.isUserInteractionEnabled = true
        viewimg!.addGestureRecognizer(tapgesture)
        
        return cell
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath)
    {
        if editingStyle == UITableViewCell.EditingStyle.delete
        {
            if indexPath.section == 0{
                let categoryid = (dataIncome[indexPath.row] as! NSDictionary)["i_categoryid"] as! String
                //DBHandler.genericQuery("delete from IncomeCategories where i_categoryid = \(categoryid)")
                //DBHandler.genericQuery("delete from Income where i_categoryid = \(categoryid)")
                let monthyear = self.navigationItem.title! as NSString
                var query = ""
                if (DBHandler.checkMonthAlreadyAdded(forCategory: "select * from income where i_categoryid=\(categoryid) AND incomemonthyear='\(monthyear)'")){
                    print("available so update")
                    query = "update income set amount = -1 where i_categoryid=\(categoryid) AND incomemonthyear='\(monthyear)'"
                }else{
                    print("not available so insert")
                    query = "insert into income values(null, '\(monthyear)',-1, datetime(), \(categoryid))"
                }
                
                if(DBHandler.genericQuery(query)){
                    print("updated successfull")
                }else{
                    print("query failed")
                }
                
                dataRefresh()
            }
            else
            {
                var object = NSDictionary()
                if indexPath.section == 1 {
                    object = dataEssentials[indexPath.row] as! NSDictionary
                }else if(indexPath.section == 2){
                    object = dataLifestyle[indexPath.row] as! NSDictionary
                }else if(indexPath.section == 3){
                    object = dataFinancial[indexPath.row] as! NSDictionary
                }
                
                let categoryid = object["categoryid"] as! String
                //DBHandler.genericQuery("delete from Categories where categoryid = \(categoryid)")
                let monthyear = self.navigationItem.title! as NSString
                var query = ""
                if (DBHandler.checkMonthAlreadyAdded(forCategory: "select * from spending where categoryid=\(categoryid) AND spendingmonthyear='\(monthyear)'")){
                    print("available to update")
                    query = "update Spending set amount = -1 where categoryid =\(categoryid) AND spendingmonthyear='\(monthyear)'"
                }else{
                    print("not available so insert")
                    query = "insert into spending values(null,-1, \(categoryid), '\(monthyear)', \(indexPath.section), datetime())"
                }
                
                if(DBHandler.genericQuery(query)){
                    print("updated successfull")
                }else{
                    print("query failed")
                }
                
                dataRefresh()
            }
            
        }
    }
    
    @objc func ClickOnCell(_ tapGesture:UITapGestureRecognizer)
    {
        if _currentTextfield .isFirstResponder
        {
            //_currentIndexPath = nil
            _currentTextfield.resignFirstResponder()
            //self.textFieldShouldReturn(_currentTextfield)
            return
            
            let delayTime = DispatchTime.now() + Double(Int64(5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: delayTime) {
                
                var Section : NSNumber = NSNumber()
                
                Section = NSNumber(value: tapGesture.view!.tag)
                NSLog("section----> %@", Section)
                
                self.tableCategory.beginUpdates()
                if(self.Expandablearray.contains(Section))
                {
                    self.Expandablearray.remove(Section)
                    self.DeleteCell(Section.intValue)
                }
                else
                {
                    self.Expandablearray.removeAllObjects()
                    self.DeleteAllRow()
                    self.Expandablearray.add(Section)
                    self.InsertCell(Section.intValue)
                }
                self.tableCategory.endUpdates()
                
                if(self.Expandablearray.contains(Section))
                {
                    //if Section == 3{
                    self.tableCategory.scrollToRow(at: IndexPath(row: 0, section: Section.intValue), at: UITableView.ScrollPosition.top, animated: true)
                    //}
                }
            }
        }
        else
        {
            var Section : NSNumber = NSNumber()
            
            Section = NSNumber(value: tapGesture.view!.tag)
            NSLog("section----> %@", Section)
            
            tableCategory.beginUpdates()
            if(Expandablearray.contains(Section))
            {
                Expandablearray.remove(Section)
                DeleteCell(Section.intValue)
            }
            else
            {
                Expandablearray.removeAllObjects()
                self.DeleteAllRow()
                Expandablearray.add(Section)
                InsertCell(Section.intValue)
            }
            tableCategory.endUpdates()
            
            if(Expandablearray.contains(Section))
            {
                tableCategory.scrollToRow(at: IndexPath(row: 0, section: Section.intValue), at: UITableView.ScrollPosition.top, animated: true)
            }
        }
    }
    func DeleteAllRow()
    {
        var count:NSInteger
        count = tableCategory.numberOfSections
        
        for i in 0 ..< count
        {
            self.DeleteCell(i)
        }
    }
    
    func DeleteCell(_ section:NSInteger)
    {
        var count :NSInteger = NSInteger()
        count = tableCategory.numberOfRows(inSection: section)
        for i in 0 ..< count
        {
            let path = IndexPath(row: i, section: section)
            self.tableCategory.deleteRows(at: [path], with: .automatic)
            
        }
        
    }
    
    func InsertCell(_ section:NSInteger)
    {
        var count :NSInteger = NSInteger()
        if(section == 0)
        {
            count = dataIncome.count
        }
        else if(section == 1)
        {
            count = dataEssentials.count
        }
        else if(section == 2)
        {
            count = dataLifestyle.count
        }
        else if(section == 3)
        {
            count = dataFinancial.count
        }
        
        for i in 0 ..< count
        {
            let path = IndexPath(row: i, section: section)
            self.tableCategory.insertRows(at: [path], with: .automatic)
        }
        
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        _currentIndexPath = indexPath
        
        if(indexPath.section == 0)
        {
            //let AddVC = self.storyboard?.instantiateViewControllerWithIdentifier("AddIncomeVC") as! AddIncomeVC
            //self.navigationController?.pushViewController(AddVC, animated: true)
            
            let cell = tableView.cellForRow(at: indexPath)
            let textfield = cell?.contentView.viewWithTag(101) as! UITextField
            textfield.isEnabled = true
            if textfield.text == "$0.00"{
                textfield.text = "$"
            }
            textfield.becomeFirstResponder()
            
        }
        else
        {
            //let ExpenceVc = self.storyboard?.instantiateViewControllerWithIdentifier("AddExpenceVC") as! AddExpenceVC
            //self.navigationController?.pushViewController(ExpenceVc, animated: true)
            let cell = tableView.cellForRow(at: indexPath)
            let textfield = cell?.contentView.viewWithTag(101) as! UITextField
            textfield.isEnabled = true
            if textfield.text == "$0.00"{
                textfield.text = "$"
            }
            textfield.becomeFirstResponder()
            
        }
    }
    
    
    // MARK: TextField Delegate Method
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        let toolBar = UIToolbar(frame: CGRect(x: 0,y: 0,width: self.view.frame.size.width,height: 40))
        toolBar.barStyle = UIBarStyle.default
        
        toolBar.items = [UIBarButtonItem(title: "Cancel", style:UIBarButtonItem.Style.plain, target: self, action: #selector(HomeVC.btnKeyboardCancelAction)),
                         UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil),
                         UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(HomeVC.btnKeyboardDoneAction))]
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        textField.inputAccessoryView = toolBar
        
        _currentTextfield = textField
        
        //tableCategory.scrollEnabled = false
        
        let val = (textField.text?.replacingOccurrences(of: "$", with: ""))! as NSString
        _currentTextfieldValue = CGFloat(val.floatValue)
        
        var cell = UITableViewCell()
        
        var tempView = textField.superview
        while (true) {
            if (tempView is UITableViewCell) {
                cell = tempView as! UITableViewCell
                break;
            }else{
                tempView = tempView?.superview
            }
        }
        
        //_currentIndexPath = tableCategory.indexPathForCell(cell)
        
        
        if textField.text == "$0.00"
        {
            textField.text = "$"
        }
        textField.becomeFirstResponder()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        //print(textField.text)
        
        textField.isEnabled = false
        
        tableCategory.isScrollEnabled = true
        
        //_currentTextfield = nil
        
        
        
        var cell = UITableViewCell()// = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "Cell")
        var tempview: UIView = textField
        while true {
            if tempview.superview!.isKind(of: UITableViewCell.self) {
                cell = tempview.superview as! UITableViewCell
                break
            }
            else
            {
                tempview = tempview.superview!
            }
        }
        
        let tempIndexPath = tableCategory.indexPath(for: cell)
        var indexPath : IndexPath
        if (tempIndexPath == nil) {
            indexPath = _currentIndexPath as IndexPath
        }else{
            indexPath = tempIndexPath! as IndexPath
        }
        
        
        if _currentIndexPath != nil {
            if _currentIndexPath == indexPath {
                _currentIndexPath = nil
            }
        }
        
        if(indexPath.section == 0){
            
            var object = NSDictionary()
            object = self.dataIncome[indexPath.row] as! NSDictionary
            
            let monthyear = self.navigationItem.title! as NSString
            let amount = textField.text?.replacingOccurrences(of: "$", with: "")
            
            var query = ""
            if (DBHandler.checkMonthAlreadyAdded(forCategory: "select * from income where i_categoryid=\(object.value(forKey: "i_categoryid")!) AND incomemonthyear='\(monthyear)'")){
                print("available so update")
                query = "update income set amount = \(amount!) where i_categoryid=\(object.value(forKey: "i_categoryid")!) AND incomemonthyear='\(monthyear)'"
            }else{
                print("not available so insert")
                query = "insert into income values(null, '\(monthyear)',\(amount!), datetime(), \(object.value(forKey: "i_categoryid")!))"
            }
            
            if(DBHandler.genericQuery(query)){
                print("updated successfull")
            }else{
                print("query failed")
            }
            self.dataRefresh()
        }
        else
        {
            if(dataTotalValues.count > 0)
            {
                var EssentialAmount = 0.0 as CGFloat
                var LifestyleAmount = 0.0 as CGFloat
                var FinancialAmount = 0.0 as CGFloat
                var IncomeAmount = 0.0 as CGFloat
                
                var predicate = NSPredicate(format: "parentcategoryid='0'")
                var array: NSArray = dataTotalValues.filtered(using: predicate) as NSArray
                
                if array.count > 0
                {
                    let dic = array[0] as! NSDictionary

                    if ((dic["amount"] as! NSString).length > 0){
                        IncomeAmount  = CGFloat( Float(dic["amount"] as! String)!)
                        //IncomeAmount = IncomeAmount == 0.0 ? 0 : CGFloat( Float(array[0]["amount"] as! String)!)
                    }
                    
                }
                
                predicate = NSPredicate(format: "parentcategoryid='1'")
                array = dataTotalValues.filtered(using: predicate) as NSArray

                if array.count > 0 {
                    let dic = array[0] as! NSDictionary

                    EssentialAmount  = CGFloat( Float(dic["amount"] as! String)!)
                    
                }
                
                predicate = NSPredicate(format: "parentcategoryid ='2'")
                array = dataTotalValues.filtered(using: predicate) as NSArray

                if array.count > 0 {
                    let dic = array[0] as! NSDictionary

                    LifestyleAmount  = CGFloat( Float(dic["amount"] as! String)!)
                }
                
                predicate = NSPredicate(format: "parentcategoryid ='3'")
                array = dataTotalValues.filtered(using: predicate) as NSArray

                if array.count > 0 {
                    let dic = array[0] as! NSDictionary

                    FinancialAmount  = CGFloat( Float(dic["amount"] as! String)!)
                }
                
                
                let val = (textField.text?.replacingOccurrences(of: "$", with: ""))! as NSString
                let currentAmount = CGFloat(val.floatValue)
                
                let totalAmount = EssentialAmount + LifestyleAmount + FinancialAmount - _currentTextfieldValue + currentAmount
                
                
                if(totalAmount > IncomeAmount)
                {
                    
                    let alert = UIAlertController(title: "Alert", message: "Over Limit", preferredStyle: UIAlertController.Style.alert)
                    
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    
                    self.present(alert, animated: true, completion: nil)
                    
                    _currentTextfield.text = String(format: "$%.2f", _currentTextfieldValue)
                    
                    return;
                }
                if(indexPath.section == 1)
                {
                    if(EssentialAmount + currentAmount >= IncomeAmount/2)
                    {
                        let alert = UIAlertController(title: "Alert", message: "Essential spending exceeded 50% of available income", preferredStyle: UIAlertController.Style.alert)
                        
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                }
                if(indexPath.section == 2)
                {
                    if(LifestyleAmount + currentAmount >= IncomeAmount * 30 / 100)
                    {
                        let alert = UIAlertController(title: "Alert", message: "Life Style spending exceeded 30% of available income", preferredStyle: UIAlertController.Style.alert)
                        
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                    
                }
                
            }
            
            var object = NSDictionary()
            if(indexPath.section == 1){
                object = self.dataEssentials[indexPath.row] as! NSDictionary
            }
            else if(indexPath.section == 2)
            {
                object = self.dataLifestyle[indexPath.row] as! NSDictionary
            }
            else if(indexPath.section == 3)
            {
                object = self.dataFinancial[indexPath.row] as! NSDictionary
            }
            
            let monthyear = self.navigationItem.title! as NSString
            let amount = textField.text?.replacingOccurrences(of: "$", with: "")
            
            var query = ""
            if (DBHandler.checkMonthAlreadyAdded(forCategory: "select * from spending where categoryid=\(object.value(forKey: "categoryid")!) AND spendingmonthyear='\(monthyear)'")){
                print("available so update")
                query = "update spending set amount = \(amount!) where categoryid=\(object.value(forKey: "categoryid")!) AND spendingmonthyear='\(monthyear)'"
            }else{
                print("not available so insert")
                query = "insert into spending values(null,\(amount!), \(object.value(forKey: "categoryid")!), '\(monthyear)', \(indexPath.section), datetime())"
            }
            
            if(DBHandler.genericQuery(query)){
                print("updated successfull")
            }else{
                print("query failed")
            }
            self.dataRefresh()
            
            
        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if let range = string.rangeOfCharacter(from: CharacterSet.decimalDigits) {
            print("start index: \(range.lowerBound), end index: \(range.upperBound)")
            
            return true
        }
        else {
            print("no data")
            
            if ((string == "") && (textField.text?.count != 1))
            {
                return true
            }
            return false
        }
    }
    
    @objc func btnKeyboardCancelAction()
    {
        _currentIndexPath = nil
        view.endEditing(true)
    }
    
    @objc func btnKeyboardDoneAction()
    {
        view.endEditing(true)
    }
    // MARK: Pdf Generator Method
    
    func PdfGenerator()
    {
        
        var array1 = NSArray()
        var array2 = NSArray()
        var array3 = NSArray()
        
        let predicate = NSPredicate(format: "amount != '0'")
        array1 = dataEssentials.filtered(using: predicate) as NSArray
        NSLog("%@", array1)
        
        array2 = dataLifestyle.filtered(using: predicate) as NSArray
        NSLog("%@", array2)
        
        array3 = dataFinancial.filtered(using: predicate) as NSArray
        NSLog("%@", array3)
        
        
        let maxCount = (array1.count) > (array2.count) ? ((array1.count) > (array3.count) ? array1.count : array3.count) : array2.count
        
        let predicate1 = NSPredicate(format: "parentcategoryid= '\(0)'")
        print(predicate1)
        let array = dataTotalValues.filtered(using: predicate1)
        let dic = array[0] as! NSDictionary
        
        if array.count > 0
        {
            var amount = dic["amount"] as! String
            
            amount = amount == "" ? "0" : amount
            let Amount = NSString(format: "%.2f", Float(amount)!) as String
            totalIncomeAmount = Amount as NSString
            
        }
        
        
        var imageFileName = NSString()
        var imageFileExtension = NSString()
        
        imageFileName = "logo_2"
        imageFileExtension = "png"
        
        let imagepath = Bundle.main.path(forResource: imageFileName as String, ofType:imageFileExtension as String)
        
        
        let  perEssential = (lblEssentialVal.text! as NSString).floatValue
        let  perLifeStyle = (lblLifestyleVal.text! as NSString).floatValue
        let  perFinancial = (lblFinancialVal.text! as NSString).floatValue
        
        let totalpercentage = perEssential + perLifeStyle + perFinancial
        NSLog("%.2f%%", totalpercentage)
        
        
        /*let format = NSDateFormatter()
         format.dateStyle = NSDateFormatterStyle.MediumStyle
         format.dateFormat = "MMM"
         let month = format.stringFromDate(NSDate())
         
         format.dateFormat = "YYYY"
         
         let year = format.stringFromDate(NSDate())
         let currentdate = NSString(format: "%@ %@", month,year)*/
        
        let currentmonth = UserDefaults.standard.value(forKey: "SelectedMonthYear") as! String
        
        var html = String(format:"<html><style>td{border-left: 1px solid gray;            border-right: 1px solid gray;}tr:first-child{border: 1px solid gray;}tr:first-child td{border-right: 1px solid gray;}table{border: 1px solid gray; border-collapse: collapse;}</style><body><br/><img src=\"logo_2.png\" alt=\"Mountain View\" style=width:304px;height:50px;></br><CENTER><FONT SIZE=3>Your Budgetina Monthly Spending Report</FONT></CENTER> <FONT SIZE=1>Budget Report of Month: %@</FONT><br/><FONT SIZE=1> Income: $%@</FONT></br> <img src=\"ScreenshotImage\" alt=\"chart image\" style=width:280px;height:150px;><img src=\"ScreenshotChart\" alt=\"chart image\" style=width:280px;height:150px;><br/>",currentmonth,totalIncomeAmount) as String
        
        
        html.append("<table style = width:100% ><tr><td colspan =2><FONT SIZE=0.5><b><CENTER>Essentials Spending</CENTER></b></FONT></td><td colspan =2><FONT SIZE=0.5><b><CENTER>Lifestyle Spending</CENTER></b></FONT></td><td colspan =2><FONT SIZE=0.5><b><CENTER>Financial Future Fund</CENTER></b></FONT></td>")
        
        
        for i in 0 ..< maxCount
        {
            var AmountEssential = 0.0 as Float
            var strNameEssential  = String()
            
            var AmountFinancial = 0.0 as Float
            var strNameFinancial = String()
            
            var AmountLifestyle = 0.0 as Float
            var strNameLifestyle = String()
            
            var strAmtEssential = String()
            var strAmtFinancial = String()
            var strAmtLifestyle = String()
            
            if(i < array1.count)
            {
                AmountEssential = (((array1.object(at: i) as AnyObject).value(forKey: "amount") as AnyObject).floatValue)!
                strNameEssential = (array1.object(at: i) as AnyObject).value(forKey: "categoryname") as! String
                
                strAmtEssential = NSString(format: "$%.2f", AmountEssential) as String
            }
            
            if(i < array3.count)
            {
                AmountFinancial = (((array3.object(at: i) as AnyObject).value(forKey: "amount") as AnyObject).floatValue)!
                strNameFinancial = (array3.object(at: i) as AnyObject).value(forKey: "categoryname") as! String
                strAmtFinancial = NSString(format: "$%.2f", AmountFinancial) as String
                
            }
            
            if(i < array2.count)
            {
                
                AmountLifestyle = (((array2.object(at: i) as AnyObject).value(forKey: "amount") as AnyObject).floatValue)!
                strNameLifestyle = (array2.object(at: i) as AnyObject).value(forKey: "categoryname") as! String
                
                strAmtLifestyle = NSString(format: "$%.2f", AmountLifestyle) as String
            }
            
            
            let rowHTML = String(format:"<tr><td align=center><FONT SIZE=0.5>%@</FONT></td><td align=center><FONT SIZE=0.5>%@</FONT></td><td align=center><FONT SIZE=0.5>%@</FONT></td><td align=center><FONT SIZE=0.5>%@</FONT></td><td align=center><FONT SIZE=0.5>%@</FONT></td><td align=center><FONT SIZE=0.5>%@</FONT></td>",strNameEssential,strAmtEssential,strNameLifestyle,strAmtLifestyle,strNameFinancial,strAmtFinancial)
            
            html.append(rowHTML)
            
        }
        
        let rowHtml = String(format: "</br></table><br/><FONT SIZE=1> Total Spending Percentage: %.2f%%</FONT><br/><FONT SIZE=1>Total Spending Value: $%.2f</FONT><br/><FONT SIZE=1> Remaining income balance: $%.2f</FONT><br/><CENTER><FONT SIZE=3>Thank you for using Budgetina!</FONT></CENTER></body></html>", totalpercentage,TotalExpencevalue,totalRemainingValue) as  String
        
        html.append(rowHtml)
        
        
        let fmt = UIMarkupTextPrintFormatter(markupText: html)
        
        // 2. Assign print formatter to UIPrintPageRenderer
        
        let render = UIPrintPageRenderer()
        render.addPrintFormatter(fmt, startingAtPageAt: 0)
        
        // 3. Assign paperRect and printableRect
        
        let page = CGRect(x: 0, y: 0, width: 595.2, height: 841.8) // A4, 72 dpi
        let printable = page.insetBy(dx: 0, dy: 0)
        
        render.setValue(NSValue(cgRect: page), forKey: "paperRect")
        render.setValue(NSValue(cgRect: printable), forKey: "printableRect")
        
        // 4. Create PDF context and draw
        
        var pdfData = NSMutableData()
        UIGraphicsBeginPDFContextToData(pdfData, CGRect.zero, nil)
        
        
        for i in 1...render.numberOfPages {
            
            UIGraphicsBeginPDFPage();
            let bounds = UIGraphicsGetPDFContextBounds()
            render.drawPage(at: i - 1, in: bounds)
        }
        
        UIGraphicsEndPDFContext();
        
        // 5. Save PDF file
        
        let path = "\(NSTemporaryDirectory())file.pdf"
        pdfData.write(toFile: path, atomically: true)
        print("open \(path)") // command to open the generated file
        
        DBHandler.addSignature(UIImage(contentsOfFile: imagepath!), onPDFData: pdfData as Data!, atPosition: CGRect(x: 280, y: 740, width: 50, height: 50))
        
        let documentsPath : AnyObject = NSSearchPathForDirectoriesInDomains(.documentDirectory,.userDomainMask,true)[0] as AnyObject
        //let destinationPath:NSString = documentsPath.appending("/SpendingReport.pdf")
        let destinationPath = documentsPath.appending("/SpendingReport.pdf")
        pdfData = NSMutableData(contentsOfFile: destinationPath as String)!
        
        let oWidth: CGFloat = ScreenshotImage.size.width
        let oHeight: CGFloat = ScreenshotImage.size.height
        let scaleFactor1: CGFloat = (oWidth > oHeight) ? 170 / oWidth : 500 / oHeight
        let nHeight: CGFloat = oHeight * scaleFactor1
        let nWidth: CGFloat = oWidth * scaleFactor1
        
        DBHandler.addSignature(ScreenshotImage, onPDFData: pdfData as Data!, atPosition: CGRect(x: 140, y: 560,width: nWidth,height: nHeight))
        
        pdfData = NSMutableData(contentsOfFile: destinationPath as String)!
        
        let oldWidth: CGFloat = ScreenshotChart.size.width
        let oldHeight: CGFloat = ScreenshotChart.size.height
        var scaleFactor = 0.0 as CGFloat
        if UIScreen.main.bounds.size.height == 480
        {
            scaleFactor = (oldWidth > oldHeight) ? 119 / oldWidth : 250 / oldHeight
        }
        else if UIScreen.main.bounds.size.height == 568
        {
            scaleFactor = (oldWidth > oldHeight) ? 150.25 / oldWidth : 250 / oldHeight
            
        }
        else if UIScreen.main.bounds.size.height == 667
        {
            scaleFactor = (oldWidth > oldHeight) ? 159 / oldWidth : 250 / oldHeight
        }
        else
        {
            scaleFactor = (oldWidth > oldHeight) ? 163 / oldWidth : 250 / oldHeight
        }
        let newHeight: CGFloat = oldHeight * scaleFactor
        let newWidth: CGFloat = oldWidth * scaleFactor
        
        DBHandler.addSignature(ScreenshotChart, onPDFData: pdfData as Data!, atPosition: CGRect(x: 330, y: 560, width: newWidth, height: newHeight))
        
    }
    
    // MARK: App Purchase Delegate Method
    
    func buyProduct(_ product: SKProduct)
    {
        print("Sending the Payment Request to Apple")
        let payment = SKPayment(product: product)
        SKPaymentQueue.default().add(payment)
    }
    
    func productsRequest (_ request: SKProductsRequest, didReceive response: SKProductsResponse)
    {
        
        let count : Int = response.products.count
        if (count>0)
        {
            //var validProducts = response.products
            let validProduct: SKProduct = response.products[0] as SKProduct
            if (validProduct.productIdentifier == self.product_id as String)
            {
                print(validProduct.localizedTitle)
                print(validProduct.localizedDescription)
                print(validProduct.price)
                buyProduct(validProduct);
            } else
            {
                print(validProduct.productIdentifier)
            }
        }
            
        else
        {
            // print("nothing")
            let alert = UIAlertController (title:"Alert", message:"Prouct Not Found" as String, preferredStyle: UIAlertController.Style.alert)
            
            alert.addAction(UIAlertAction(title:"OK", style: UIAlertAction.Style.default, handler: nil))
            
            self.present(alert, animated:true, completion: nil)
            
        }
    }
    
    
    func request(_ request: SKRequest, didFailWithError error: Error)
    {
        print("Error Fetching product information");
    }
    
    func paymentQueue(_ _queue: SKPaymentQueue,
                      updatedTransactions transactions: [SKPaymentTransaction])
    {
        print("Received Payment Transaction Response from Apple")
        
        for transaction:AnyObject in transactions {
            if let trans:SKPaymentTransaction = transaction as? SKPaymentTransaction{
                switch trans.transactionState {
                case .purchased:
                    print("Product Purchased");
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    defaults.set(true , forKey: "purchased")
                    //overlayView.hidden = true
                    break;
                    
                case .failed:
                    print("Purchased Failed");
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    break;
                    
                case .restored:
                    print("Already Purchased");
                    SKPaymentQueue.default().restoreCompletedTransactions()
                    
                    
                default:
                    break;
                }
            }
        }
    }
    
    @objc func taponImage(_ gesture:UITapGestureRecognizer)
    {
        var cell = UITableViewCell()
        var tempview: UIView = gesture.view!
        
        while true {
            if tempview.superview!.isKind(of: UITableViewCell.self) {
                cell = tempview.superview as! UITableViewCell
                break
            }
            else
            {
                tempview = tempview.superview!
            }
        }
        
        let indexPath = tableCategory.indexPath(for: cell)! as IndexPath
        
        let imageview = cell.contentView.viewWithTag(103) as! UIImageView!
        let textField = cell.contentView.viewWithTag(101) as! UITextField
        
        if imageview?.image == UIImage(named: "box_new.png")
        {
            if textField.text == "$0.00"
            {
                let alert = UIAlertController(title: "Alert", message: "Please enter the amount first.", preferredStyle: UIAlertController.Style.alert)
                
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                
                self.present(alert, animated: true, completion: nil)
            }
            else
            {
                imageview?.image = UIImage(named: "checked1.png")
                arrcontentobject.add(indexPath)
                
                let indexPath = tableCategory.indexPath(for: cell)! as IndexPath
                
                var object = NSDictionary()
                
                if (indexPath.section == 0)
                {
                    object = self.dataIncome[indexPath.row] as! NSDictionary
                    
                    let cateId = object.value(forKey: "i_categoryid") as! String
                    
                    
                    var  amount = textField.text! as NSString
                    amount = amount.substring(from: 1) as String as NSString
                    
                    let monthyear =  UserDefaults.standard.value(forKey: "SelectedMonthYear") as! NSString
                    
                    UserDefaults.standard.set(monthyear, forKey: "RepeatIncomeMonthYear")
                    UserDefaults.standard.synchronize()
                    
                    print("%@",object)
                    print(monthyear)
                    
                    
                    let  query = "insert into RepeatIncome values(null, '\(amount)', '\(cateId)','\(monthyear)')"
                    
                    
                    if(DBHandler.genericQuery(query))
                    {
                        print("updated successfull")
                        
                        
                    }else
                    {
                        print("query failed")
                    }
                    self.dataRefresh()
                }
                
                if(indexPath.section == 1){
                    object = self.dataEssentials[indexPath.row] as! NSDictionary
                    
                    updatespendingdata(object, str: textField.text! as NSString)
                }
                else if(indexPath.section == 2)
                {
                    object = self.dataLifestyle[indexPath.row] as! NSDictionary
                    updatespendingdata(object, str: textField.text! as NSString)
                    
                }
                else if(indexPath.section == 3)
                {
                    object = self.dataFinancial[indexPath.row] as! NSDictionary
                    updatespendingdata(object, str: textField.text! as NSString)
                }
            }
        }
        else
        {
            imageview?.image = UIImage(named: "box_new.png")
            arrcontentobject.remove(indexPath)
            
            let indexPath = tableCategory.indexPath(for: cell)! as IndexPath
            
            var object = NSDictionary()
            
            if (indexPath.section == 0)
            {
                object = self.dataIncome[indexPath.row] as! NSDictionary
                
                let monthyear =  UserDefaults.standard.value(forKey: "RepeatIncomeMonthYear") as! NSString
                
                let cateId = object.value(forKey: "i_categoryid") as! String
                
                
                let  query = "delete from RepeatIncome where i_categoryid=\(cateId) AND incomemonthyear='\(monthyear)'"
                
                
                if(DBHandler.genericQuery(query))
                {
                    print("updated successfull")
                    
                    
                }else
                {
                    print("query failed")
                }
                self.dataRefresh()
            }
            
            if(indexPath.section == 1){
                object = self.dataEssentials[indexPath.row] as! NSDictionary
                
                removeSpendingData(object)
            }
            else if(indexPath.section == 2)
            {
                object = self.dataLifestyle[indexPath.row] as! NSDictionary
                removeSpendingData(object)
                
            }
            else if(indexPath.section == 3)
            {
                object = self.dataFinancial[indexPath.row] as! NSDictionary
                removeSpendingData(object)
                
            }
            
        }
        
        
    }
    
    func updatespendingdata(_ object:NSDictionary,str:NSString)
    {
        
        let cateId = object.value(forKey: "categoryid") as! String
        let catName = object.value(forKey: "categoryname") as! String
        let parentcatId = object.value(forKey: "parentcategoryid") as! String
        
        let  amount = str.substring(from: 1) as String
        
        let monthyear =  UserDefaults.standard.value(forKey: "SelectedMonthYear") as! NSString
        
        UserDefaults.standard.set(monthyear, forKey: "RepeatSpendingMonthYear")
        UserDefaults.standard.synchronize()
        
        let  query = "insert into RepeatSpending values(\(cateId), '\(catName)', '\(amount)','\(parentcatId)','\(monthyear)')"
        
        
        if(DBHandler.genericQuery(query))
        {
            print("updated successfull")
            
            
        }else
        {
            print("query failed")
        }
        self.dataRefresh()
    }
    
    func removeSpendingData(_ object:NSDictionary)
    {
        let cateId = object.value(forKey: "categoryid") as! String
        
        let monthyear =  UserDefaults.standard.value(forKey: "RepeatSpendingMonthYear") as! NSString
        
        //        print("%@",object)
        //        print(monthyear)
        
        let  query = "delete from RepeatSpending where categoryid=\(cateId) AND incomemonthyear='\(monthyear)'"
        
        
        if(DBHandler.genericQuery(query))
        {
            print("updated successfull")
            
            
        }else
        {
            print("query failed")
        }
        self.dataRefresh()
    }
    
}


