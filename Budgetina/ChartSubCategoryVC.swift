//
//  ChartSubCategoryVC.swift
//  Budgetina
//
//  Created by Ovedgroup LLC on 15/12/15.
//  Copyright © 2015 Ovedgroup LLC. All rights reserved.
//

import UIKit

class ChartSubCategoryVC: UIViewController,UITableViewDelegate,UITableViewDataSource
{

    @IBOutlet var tableSubCategory: UITableView!
    
    var SubCategoryArray:NSMutableArray!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        tableSubCategory.tableFooterView = UIView()
        self.navigationItem.setHidesBackButton(false, animated: false)
        self.navigationController?.navigationBar.isTranslucent = false
        
        let navTitle = UserDefaults.standard.value(forKey: "SelectedMonthYear") as! String
        self.navigationItem.title = navTitle
        
        let BackButton = UIImage(named: "back_icon.PNG") as UIImage?
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: BackButton, style: UIBarButtonItem.Style.plain, target: self, action: #selector(ChartSubCategoryVC.BackButtonAction))
        
        
    }
    @objc func BackButtonAction()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: TableView Delegate Method
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return SubCategoryArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell

    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        let lblTitle = cell.contentView.viewWithTag(100) as! UILabel
        let lblAmount = cell.contentView.viewWithTag(101) as! UILabel

        let dic = self.SubCategoryArray[indexPath.row] as! NSDictionary
        
        lblTitle.text = dic["categoryname"] as? String
        
        let amount = dic["amount"] as! String
        lblAmount.text = NSString(format: "$%.2f", Float(amount)!) as String
        
        return cell
    }
    
    
}
