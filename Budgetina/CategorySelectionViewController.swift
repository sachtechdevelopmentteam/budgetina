//
//  CategorySelectionViewController.swift
//  Budgetina
//
//  Created by HANDA, KUNWAR on 2/23/19.
//  Copyright © 2019 WeEnggs Technology. All rights reserved.
//

import UIKit
import FirebaseDatabase

class CategoryViewCell: UICollectionViewCell {
    
    // MARK: - Constants
    
    static let reuseIdentifier = "CategoryViewCell"
    
    // MARK: - Outlets
    
    @IBOutlet weak var categoryTitle: UILabel!
    @IBOutlet weak var checkMarkView: CheckMarkView!
}



class CategorySelectionViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var doneButton: UIButton!
    let categoryArray = NSArray(array:["ESSENTIALS","LIFESTYLE","FINANCIAL FUTURE"])
    var selectedDataCategories : NSMutableArray = NSMutableArray()
    var dataEssentials : NSMutableArray = NSMutableArray()
    var spendingCategories : NSMutableArray = NSMutableArray()
    var dataLifestyle : NSMutableArray = NSMutableArray()
    var dataFinancial : NSMutableArray = NSMutableArray()
    var ref: DatabaseReference!
    
    private let kCellheaderReuse : String = "CategoryHeaderCollectionReusableView"
    override func viewDidLoad() {
        super.viewDidLoad()
        ref = Database.database().reference()

        doneButton.layer.cornerRadius = doneButton.frame.size.height/2
        self.dataEssentials = NSMutableArray()
        self.dataLifestyle = NSMutableArray()
        self.dataFinancial = NSMutableArray()

        ref.child("spendingCategories").observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user value
            if let value = snapshot.value as? NSMutableArray {
                self.spendingCategories = value
                self.dataEssentials = (self.spendingCategories[0] as! NSDictionary)["subCategories"] as! NSMutableArray
                self.dataLifestyle = (self.spendingCategories[1] as! NSDictionary)["subCategories"] as! NSMutableArray
                self.dataFinancial = (self.spendingCategories[2] as! NSDictionary)["subCategories"] as! NSMutableArray
                self.collectionView .reloadData()
            }
            
            // ...
        }) { (error) in
            print(error.localizedDescription)
        }
       
        self.navigationItem.title = "Spending"
        let rightBarButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.add, target: self, action: #selector(gotoHome))
        self.navigationItem.rightBarButtonItem = rightBarButton

        // Do any additional setup after loading the view.
    }
    
    @objc func gotoHome() -> Void {
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.showMainTabbar()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(section == 0)
        {
            return dataEssentials.count
        }
        else if(section == 1)
        {
            return dataLifestyle.count
        }
        else if(section == 2)
        {
            return dataFinancial.count
        } else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CategoryViewCell.reuseIdentifier, for: indexPath as IndexPath) as! CategoryViewCell
        
        var dic = NSDictionary()

        if(indexPath.section == 0)
        {
            dic = self.dataEssentials[indexPath.row] as! NSDictionary
           
        }
        else if(indexPath.section == 1)
        {
            dic = self.dataLifestyle[indexPath.row] as! NSDictionary
            
        
        }
        else if(indexPath.section == 2)
        {
            
            dic = self.dataFinancial[indexPath.row] as! NSDictionary
            
        }
        
        cell.categoryTitle.text = replaceDummyStrings(string: dic["categoryname"] as? String ?? "")

        cell.checkMarkView.style = .openCircle
        
        if selectedDataCategories.contains(dic) {
            cell.checkMarkView.checked = true
        } else {
            cell.checkMarkView.checked = false
        }
        
        cell.checkMarkView.setNeedsDisplay()

        return cell
    }
    
    // MARK: - UICollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath as IndexPath) as! CategoryViewCell
        cell.checkMarkView.checked = !cell.checkMarkView.checked
        
        var dic = NSDictionary()
        
        if(indexPath.section == 0)
        {
            dic = self.dataEssentials[indexPath.row] as! NSDictionary
            
        }
        else if(indexPath.section == 1)
        {
            dic = self.dataLifestyle[indexPath.row] as! NSDictionary
            
            
        }
        else if(indexPath.section == 2)
        {
            
            dic = self.dataFinancial[indexPath.row] as! NSDictionary
            
        }
        
        selectedDataCategories.add(dic)
        
        let arr = selectedDataCategories.compactMap { $0 as? Dictionary<String, String> }
        UserDefaults.standard.set(arr, forKey: "selectedDataCategories")
        UserDefaults.standard.synchronize()
        
        print( UserDefaults.standard.value(forKey: "selectedDataCategories") ?? "nil")
        collectionView.reloadData()
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return categoryArray.count
    }
    
//    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
//        var reusableView : UICollectionReusableView? = nil
//
//        // Create header
//        if (kind == UICollectionView.elementKindSectionHeader) {
//            // Create Header
//            let headerView : CategoryHeaderCollectionReusableView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: kCellheaderReuse, for: indexPath) as! CategoryHeaderCollectionReusableView
//            let str = categoryArray.object(at: indexPath.section) as? String
//
//            headerView.hearderTitle.text = str
//            reusableView = headerView
//        }
//        return reusableView!
//    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "CategoryHeaderCollectionReusableView", for: indexPath as IndexPath) as! CategoryHeaderCollectionReusableView
        let str = categoryArray.object(at: indexPath.section) as? String
        
        headerView.hearderTitle.text = str?.lowercased().capitalizingFirstLetter()
        
        headerView.layer.cornerRadius = headerView.frame.size.height/2

        return headerView
    }
    
    // MARK: UICollectionViewDelegateFlowLayout

    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 20)  // Header size
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func doneButtonPressed(_ sender: Any) {
        let subCategoriesQuestionsViewController = self.storyboard?.instantiateViewController(withIdentifier: "SubCategoriesQuestionsViewController") as! SubCategoriesQuestionsViewController
//        self.navigationController?.pushViewController(subCategoriesQuestionsViewController, animated: true)
        present(subCategoriesQuestionsViewController, animated: true, completion: nil)

    }
    func replaceDummyStrings(string: String) -> String {
        var stringR = string
        stringR = stringR.replacingOccurrences(of: "HOME - ", with: "")
        stringR = stringR.replacingOccurrences(of: "UTILITIES - ", with: "")
        stringR = stringR.replacingOccurrences(of: "GROCERIES - ", with: "")
        stringR = stringR.replacingOccurrences(of: "TRANSPORTATION - ", with: "")
        stringR = stringR.replacingOccurrences(of: "FAMILY OBLIGATIONS - ", with: "")
        stringR = stringR.replacingOccurrences(of: "DEBT PAYMENT - ", with: "")
        stringR = stringR.replacingOccurrences(of: "ENTERTAINMENT - ", with: "")
        stringR = stringR.replacingOccurrences(of: "GROOMING - ", with: "")
        stringR = stringR.replacingOccurrences(of: "HEALTH & FITNESS - ", with: "")
        stringR = stringR.replacingOccurrences(of: "CLOTHING - ", with: "")
        stringR = stringR.replacingOccurrences(of: "HOBBIES - ", with: "")
        stringR = stringR.replacingOccurrences(of: "HEALTH & MEDICAL - ", with: "")
        stringR = stringR.replacingOccurrences(of: "PROFESSIONAL DUES - ", with: "")
        return stringR.lowercased().capitalizingFirstLetter()
    }
    
   

}
