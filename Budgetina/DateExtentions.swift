//
//  DateExtentions.swift
//  Budgetina
//
//  Created by HANDA, KUNWAR on 2/23/19.
//  Copyright © 2019 WeEnggs Technology. All rights reserved.
//

import Foundation
extension Date {
    func monthAsString() -> String {
        let df = DateFormatter()
        df.setLocalizedDateFormatFromTemplate("MMMM")
        return df.string(from: self)
    }
    
    func yearAsString() -> String {
        let df = DateFormatter()
        df.setLocalizedDateFormatFromTemplate("YYYY")
        return df.string(from: self)
    }
}
