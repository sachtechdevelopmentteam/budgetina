//
//  HomeViewController.swift
//  Budgetina
//
//  Created by HANDA, KUNWAR on 2/17/19.
//  Copyright © 2019 WeEnggs Technology. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth
import Charts
class HomeViewController: UIViewController, ChartViewDelegate {
    
    var dataIncome: NSMutableArray = NSMutableArray()
    var dataEssentials : NSMutableArray = NSMutableArray()
    var dataLifestyle : NSMutableArray = NSMutableArray()
    var dataFinancial : NSMutableArray = NSMutableArray()
    var dataSpending : NSArray = NSArray()
    var dataTotalValues : NSArray = NSArray()
    var ref: DatabaseReference!
    var arrRepeatSpending = NSMutableArray()
    var arrRepeatIncome = NSMutableArray()
    var arrcontentobject = NSMutableArray()
    var legendEnteries = [LegendEntry]()
    @IBOutlet var spendingChartView: PieChartView!
    @IBOutlet var categoryChartView: PieChartView!
    @IBOutlet var yearBarChartView: HorizontalBarChartView!


    var finantialBook = NSMutableDictionary()
    var parentandSubCategories = NSMutableArray()
    var incomeCategories = NSMutableArray()

    let Expandablearray : NSMutableArray = NSMutableArray()
    
    let Categoryarray = NSArray(array:["INCOME","ESSENTIALS","LIFESTYLE","FINANCIAL FUTURE"])
    
    let monthArray = NSArray(array:["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"])
    let monthFirstLetterArray = NSArray(array:["J", "F", "M", "A", "M", "J", "J", "A", "S", "O", "N", "D"])

    var totalIncomeAmount = NSString()
    
    var TotalExpencevalue = 0.0 as CGFloat
    var totalRemainingValue = 0.0 as CGFloat
    
    lazy var customFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.negativePrefix = "$"
        formatter.positivePrefix = "$"
        formatter.negativeSuffix = ""
        formatter.minimumSignificantDigits = 1
        formatter.minimumFractionDigits = 1
        return formatter
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ref = Database.database().reference()

        spendingChartView.isHidden = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap(sender:)))
        categoryChartView.addGestureRecognizer(tap)
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(handleTap(sender:)))
        spendingChartView.addGestureRecognizer(tap1)
        getIncomeCategories()
        getSpendingParentandSubCategories()
       // spendingChartView.delegate = self
      //  categoryChartView.delegate = self
        
        //        chartView.legend = l
        
        // entry label styling
        
        

        finantialBook =  NSMutableDictionary.init()
         let user = Auth.auth().currentUser
        
        ref.child("users").child(user?.uid ?? "nil").child("income").child(Date().yearAsString()).observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user value
            
            if let value = snapshot.value as? NSDictionary {
                    self.finantialBook["income"] = value
             
                self.ref.child("users").child(user?.uid ?? "nil").child("spending").child(Date().yearAsString()).observeSingleEvent(of: .value, with: { (snapshot) in
                    // Get user value
                    
                    if let value = snapshot.value as? NSDictionary {
                        self.finantialBook["spending"] = value
                        
                        print(self.finantialBook)
                        self.setupCat(pieChartView: self.spendingChartView)
                        self.setupCat(pieChartView: self.categoryChartView)
                        self.setupBarChart(chartView: self.yearBarChartView)
                        
                        self.populatePieChartForMonth(finicialDoc: self.finantialBook, month: Date().monthAsString())
                        self.populateBarChartForYear(chartView: self.yearBarChartView)
                    }
                    
                }) { (error) in
                    print(error.localizedDescription)
                }
            }
            
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    
    func setupBarChart(chartView: HorizontalBarChartView) {
        chartView.delegate = self
        
        chartView.chartDescription?.enabled = false
        
        chartView.drawBarShadowEnabled = false
        chartView.drawValueAboveBarEnabled = true
        chartView.doubleTapToZoomEnabled = false
        chartView.pinchZoomEnabled = false
        chartView.leftAxis.enabled = false
        chartView.rightAxis.enabled = true
        let rightAxis = chartView.rightAxis
        
        let limit = getMaximumIncome() > getMaximumSpending() ? getMaximumIncome() : getMaximumSpending()
        
        rightAxis.axisMaximum = limit + (limit * 0.20)
        rightAxis.axisMinimum = -(limit + (limit * 0.20))
        rightAxis.drawZeroLineEnabled = true
        rightAxis.drawAxisLineEnabled = false
        rightAxis.drawGridLinesEnabled = false
        rightAxis.drawLabelsEnabled = true
        rightAxis.labelCount = 7
        rightAxis.valueFormatter = DefaultAxisValueFormatter(formatter: customFormatter)
        rightAxis.labelFont = .systemFont(ofSize: 9)
        
        let xAxis = chartView.xAxis
        xAxis.labelPosition = .bottomInside
        xAxis.drawAxisLineEnabled = false
        xAxis.drawGridLinesEnabled = false
        xAxis.drawLabelsEnabled = true
        xAxis.axisMinimum = 10
        xAxis.axisMaximum = 130
        xAxis.centerAxisLabelsEnabled = true
        xAxis.labelCount = 12
        xAxis.labelPosition = .top
        xAxis.granularity = 10
        xAxis.valueFormatter = self
        xAxis.labelFont = .systemFont(ofSize: 9)
        
        let l = chartView.legend
        l.horizontalAlignment = .center
        l.verticalAlignment = .bottom
        l.orientation = .horizontal
        l.formSize = 8
        l.formToTextSpace = 8
        l.xEntrySpace = 6
    }
    
    func populateBarChartForYear(chartView: HorizontalBarChartView) {
        
        let bookKeeper = populateYearlyIncomeAndSpendingPerMonth()
        var yValue : [BarChartDataEntry] = [BarChartDataEntry]()
        var xValue = 5
        for book in bookKeeper {
            xValue = xValue + 10
            yValue.append(BarChartDataEntry(x:Double(xValue), yValues: book))
        }

        let set = BarChartDataSet(values: yValue, label: "")
        set.drawIconsEnabled = false
        set.valueFormatter = DefaultValueFormatter(formatter: customFormatter)
        set.valueFont = .systemFont(ofSize: 7)
        set.stackLabels = monthArray as! [String]
        set.axisDependency = .right
        set.colors = [#colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1),#colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)]
       // set.color(atIndex: <#T##Int#>)
        set.stackLabels = ["Earnings", "Expenditure"]
        let data = BarChartData(dataSet: set)
        data.barWidth = 8.5
        chartView.highlightFullBarEnabled = true
        chartView.data = data
        chartView.setNeedsDisplay()
    }
    
    
    func populatePieChartForMonth(finicialDoc: NSMutableDictionary, month: String) {

         let allSpendingForMonth = BudgetManager.getAllSpendingValuesForMonth(finicialBookForCategory: finicialDoc, month: month, year: Date().yearAsString())
        
         print(allSpendingForMonth.values)

         let totalSpendingForMonth = BudgetManager.getTotalSpendingValuesForMonth(finicialBookForCategory: finicialDoc, month: month, year: Date().yearAsString())
        
         let totalSpendingForEssentialCategory = BudgetManager.getTotalSpendingValuesForMonthForParentCategory(finicialBookForCategory: finicialDoc, month: month, year: Date().yearAsString(), parentcategoryid: "1")
        
         let totalSpendingForLifeStyleCategory = BudgetManager.getTotalSpendingValuesForMonthForParentCategory(finicialBookForCategory: finicialDoc, month: month, year: Date().yearAsString(), parentcategoryid: "2")
        
         let totalSpendingForFinancialCategory = BudgetManager.getTotalSpendingValuesForMonthForParentCategory(finicialBookForCategory: finicialDoc, month: month, year: Date().yearAsString(), parentcategoryid: "3")
        

        
        let totalSpendingForEssentialCategoryNumber = NSNumber(value: totalSpendingForEssentialCategory)
        let totalSpendingForFinancialCategoryNumber = NSNumber(value: totalSpendingForFinancialCategory)
        let totalSpendingForLifeStyleCategoryNumber = NSNumber(value: totalSpendingForLifeStyleCategory)
        let totalSpendingForMonthNumber = NSNumber(value: totalSpendingForMonth)

        
        let percentForEssential = ((totalSpendingForEssentialCategoryNumber as! Float) / (totalSpendingForMonthNumber as! Float)) * (NSNumber(value: 100) as! Float)
          let percentForFinantial = ((totalSpendingForFinancialCategoryNumber as! Float) / (totalSpendingForMonthNumber as! Float)) * (NSNumber(value: 100) as! Float)
          let percentForlifestyle = ((totalSpendingForLifeStyleCategoryNumber as! Float) / (totalSpendingForMonthNumber as! Float)) * (NSNumber(value: 100) as! Float)


        
        let spendingParentCatSlicesData : NSMutableDictionary = NSMutableDictionary()
        
        if(totalSpendingForEssentialCategoryNumber.intValue > 0){
            spendingParentCatSlicesData["essentialSlice"] = NSDictionary(objects: [percentForEssential,"Essential"], forKeys: ["percent" as NSCopying,"description" as NSCopying])

        }
        if(totalSpendingForFinancialCategoryNumber.intValue > 0){
            spendingParentCatSlicesData["finantialSlice"] = NSDictionary(objects: [percentForFinantial,"Finantial Future"], forKeys: ["percent" as NSCopying,"description" as NSCopying])

        }
        
        if(totalSpendingForLifeStyleCategoryNumber.intValue) > 0{
            spendingParentCatSlicesData["lifestyleSlice"] = NSDictionary(objects: [percentForlifestyle,"Lifestyle"], forKeys: ["percent" as NSCopying,"description" as NSCopying])
        }
        
        
        print(spendingParentCatSlicesData)
        
        
        
//        
//        
//        let spendingParentCatSlicesData : NSMutableDictionary = NSMutableDictionary()
//
//
//        spendingParentCatSlicesData["essentialSlice"] = NSDictionary(objects: [percentForEssential,"Essential"], forKeys: ["percent" as NSCopying,"description" as NSCopying])
//        spendingParentCatSlicesData["finantialSlice"] = NSDictionary(objects: [percentForFinantial,"Finantial Future"], forKeys: ["percent" as NSCopying,"description" as NSCopying])
//        spendingParentCatSlicesData["lifestyleSlice"] = NSDictionary(objects: [percentForlifestyle,"Lifestyle"], forKeys: ["percent" as NSCopying,"description" as NSCopying])
//
//        
//        print(spendingParentCatSlicesData)

        
     //   let slicesArray = NSArray(array: [percentForEssential,percentForlifestyle])
        
        
        self.setSpendingDataForPieChart(spendingParentCatSlicesData.allKeys.count, range: 100, slices: spendingParentCatSlicesData.allValues as NSArray)
        
        let array:NSMutableArray = NSMutableArray()
        
        for dic in allSpendingForMonth.values {
            let sdic = dic as NSDictionary
            let nsdic = sdic.mutableCopy() as! NSMutableDictionary
            let amt = nsdic["amount"] as! String
            let percent = (amt.numberValue!.floatValue / totalSpendingForMonth) * 100.0
            nsdic["percent"] = percent
            array.add(nsdic)
        }
        
        self.setCategoryDataForPieChart(array.count, range: 100, slices: array)
    }
    
    func setCategoryDataForPieChart(_ count: Int, range: UInt32, slices: NSArray) {
        let entries = (0..<count).map { (i) -> PieChartDataEntry in
            // IMPORTANT: In a PieChart, no values (Entry) should have the same xIndex (even if from different DataSets), since no values can be drawn above each other.
            let obj = slices[i] as! NSDictionary
            let percent = (obj["percent"] as? Float)
            
            return PieChartDataEntry(value: Double(percent ?? 0.0),
                                     label: obj["categoryname"] as? String)
        }
        
        let set = PieChartDataSet(values: entries, label: "")
        set.drawIconsEnabled = true
        set.sliceSpace = 2
        
        
        set.colors = ChartColorTemplates.vordiplom()
            + ChartColorTemplates.material()
            + ChartColorTemplates.colorful()
            + [UIColor(red: 51/255, green: 181/255, blue: 229/255, alpha: 1)]
        
        let data = PieChartData(dataSet: set)
        
        let pFormatter = NumberFormatter()
        pFormatter.numberStyle = .percent
        pFormatter.maximumFractionDigits = 1
        pFormatter.multiplier = 1
        pFormatter.percentSymbol = " %"
        data.setValueFormatter(DefaultValueFormatter(formatter: pFormatter))
        
        data.setValueFont(.systemFont(ofSize: 11, weight: .light))
        data.setValueTextColor(.gray)
        
        categoryChartView.data = data
        categoryChartView.highlightValues(nil)
    }
    func setSpendingDataForPieChart(_ count: Int, range: UInt32, slices: NSArray) {
        let entries = (0..<count).map { (i) -> PieChartDataEntry in
            // IMPORTANT: In a PieChart, no values (Entry) should have the same xIndex (even if from different DataSets), since no values can be drawn above each other.
            let obj = slices[i] as! NSDictionary
            let percent = (obj["percent"] as? Float)

            return PieChartDataEntry(value: Double(percent ?? 0.0),
                                     label: obj["description"] as? String)
        }
        
        let set = PieChartDataSet(values: entries, label: "")
        set.drawIconsEnabled = false
        set.sliceSpace = 2
        
        
        set.colors = ChartColorTemplates.joyful()
            + ChartColorTemplates.liberty()
        
        let data = PieChartData(dataSet: set)
        
        let pFormatter = NumberFormatter()
        pFormatter.numberStyle = .percent
        pFormatter.maximumFractionDigits = 1
        pFormatter.multiplier = 1
        pFormatter.percentSymbol = " %"
        data.setValueFormatter(DefaultValueFormatter(formatter: pFormatter))
        
        data.setValueFont(.systemFont(ofSize: 11, weight: .light))
        data.setValueTextColor(.gray)
        
        spendingChartView.data = data
        spendingChartView.highlightValues(nil)
    }
    
    func setup(pieChartView chartView: PieChartView) {
        chartView.usePercentValuesEnabled = false
        chartView.drawSlicesUnderHoleEnabled = true
        chartView.holeRadiusPercent = 0.0
        chartView.transparentCircleRadiusPercent = 1
        chartView.chartDescription?.enabled = false
        chartView.setExtraOffsets(left: 0, top: 0, right: 0, bottom: 0)
        chartView.drawCenterTextEnabled = true
        
        let paragraphStyle = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
        paragraphStyle.lineBreakMode = .byTruncatingTail
        paragraphStyle.alignment = .center
        
        if chartView == spendingChartView {
            chartView.centerText = "Overview";

        } else {
            chartView.centerText = "Monthly breakdown";

        }
        

        chartView.drawHoleEnabled = true
        chartView.rotationAngle = 0
        chartView.rotationEnabled = true
        chartView.highlightPerTapEnabled = false
        chartView.drawEntryLabelsEnabled = true
        let l = chartView.legend
        chartView.legend.enabled = false
        l.form = .circle
        l.horizontalAlignment = .center
        l.verticalAlignment = .bottom
        l.orientation = .horizontal
       // l.yOffset = 400.0
//        l.xOffset = -100.0
        l.drawInside = true

//        l.yOffset = 0
        //        chartView.legend = l
    }
    
    func setupCat(pieChartView chartView: PieChartView) {
        chartView.usePercentValuesEnabled = true
        chartView.drawSlicesUnderHoleEnabled = true
        chartView.holeRadiusPercent = 0.60
        chartView.transparentCircleRadiusPercent = 1
        chartView.chartDescription?.enabled = true
        chartView.setExtraOffsets(left: 0, top: 0, right: 0, bottom: 0)
        chartView.legend.enabled = true
        chartView.drawCenterTextEnabled = true
        chartView.legend.drawInside  = true
        chartView.drawEntryLabelsEnabled = false
        if chartView == spendingChartView {
            chartView.centerText = "Overview";
            
        } else {
            chartView.centerText = "Monthly breakdown";
            
        }
        
        let paragraphStyle = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
        paragraphStyle.lineBreakMode = .byTruncatingTail
        paragraphStyle.alignment = .center
        
        
      //  chartView.centerText = "May";
        
        chartView.drawHoleEnabled = true
        chartView.rotationAngle = 0
        chartView.rotationEnabled = true
        chartView.highlightPerTapEnabled = false
        
        let l = chartView.legend
        l.form = .circle
//
//        l.orientation = .vertical
        l.direction = .leftToRight
        l.font = NSUIFont.systemFont(ofSize: 6.0)
        l.formSize = 4

        // l.formLineDashLengths = [1,2,3]
//        l.xEntrySpace = 7
//        l.yEntrySpace = 0
//        l.yOffset = 0
    }
    
    
    
    func getSpendingParentandSubCategories() {

        self.ref.child("spendingCategories").observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user value
            
            if let value = snapshot.value as? NSMutableArray {
                self.parentandSubCategories = value
                
                print(self.parentandSubCategories)
                
            }
            
        }) { (error) in
            print(error.localizedDescription)
        }
        
    }
    func getIncomeCategories() {
        
        self.ref.child("incomeCategories").observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user value
            
            if let value = snapshot.value as? NSMutableArray {
                self.incomeCategories = value
                
                print(self.incomeCategories)
                
            }
            
        }) { (error) in
            print(error.localizedDescription)
        }
        
    }
    
    @objc func handleTap(sender: UITapGestureRecognizer? = nil) {
        // handling code
        categoryChartView.isHidden = !categoryChartView.isHidden
        spendingChartView.isHidden = !spendingChartView.isHidden
        spendingChartView.animate(xAxisDuration: 1.4, easingOption: .easeOutBack)
        categoryChartView.animate(xAxisDuration: 1.4, easingOption: .easeOutBack)

    }
    
    @objc func handleTap1(sender: UITapGestureRecognizer? = nil) {
        // handling code
        
        spendingChartView.animate(xAxisDuration: 1.4, easingOption: .easeOutBack)
        categoryChartView.animate(xAxisDuration: 1.4, easingOption: .easeOutBack)

        categoryChartView.isHidden = !categoryChartView.isHidden
        spendingChartView.isHidden = !spendingChartView.isHidden
    }

    func populateYearlyIncomeAndSpendingPerMonth() -> [[Double]] {
        
        var bookKeeper:[[Double]] = [[Double]]()
        
        for monthString in monthArray.reversed() {
            var income = Double(BudgetManager.getTotalIncomeValuesForMonth(finicialBookForCategory: self.finantialBook,month: monthString as! String, year: Date().yearAsString()))
            print(monthString)
            print(income)
            var spending = Double(BudgetManager.getTotalSpendingValuesForMonth(finicialBookForCategory: self.finantialBook,month: monthString as! String, year: Date().yearAsString()))
            print(spending)
            
//            if income == 0{
//                income = Double.random(in: 1000 ..< 7000.0)
//            }
//
//            if spending == 0{
//                spending = Double.random(in: 1000 ..< 7000)
//            }
            bookKeeper.append([-Double(income),Double(spending)])
        }
        return bookKeeper
        
    }
    
    func getMaximumIncome() -> Double {
        var incomeMax:[Double] = [Double]()
        
        for monthString in monthArray {
            let income = BudgetManager.getTotalIncomeValuesForMonth(finicialBookForCategory: self.finantialBook,month: monthString as! String, year: Date().yearAsString())
            incomeMax.append(Double(income))
        }
        return incomeMax.max()!
    }
    
    func getMaximumSpending() -> Double {
        var spendingMax:[Double] = [Double]()
        
        for monthString in monthArray {
            let spending = BudgetManager.getTotalSpendingValuesForMonth(finicialBookForCategory: self.finantialBook,month: monthString as! String, year: Date().yearAsString())
            spendingMax.append(Double(spending))
        }
        return spendingMax.max()!
    }
    
    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight) {
        NSLog("chartValueSelected");
        print(entry.y)
        print(entry.x)

     //   entry.in
//        print(monthArray.reversed()[highlight.dataIndex] as! String)
    }



    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */


    @IBAction func btnAddTapped(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ModelScreenVC") as! ModelScreenVC
        self.present(vc, animated: true, completion: nil)
    }
}
extension HomeViewController: IAxisValueFormatter {
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        var index = 0
        if value > 0 {
            index = Int((value/10) - 1)
            if index > 11{
                index = 11
            }
        }
        
        return monthFirstLetterArray.reversed()[index] as! String
    }
    
}
