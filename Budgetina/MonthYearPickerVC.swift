//
//  MonthYearPickerVC.swift
//  Budgetina
//
//  Created by Tushar Navadiya on 01/12/15.
//  Copyright © 2015 Ovedgroup LLC. All rights reserved.
//

import UIKit

protocol MonthYearDelegate
{
    func monthYearSelected(_ month:NSString, year:NSString)

}

class MonthYearPickerVC: UIViewController {
   
    var arrYear = ["2015","2016","2017","2018","2019","2020","2021","2022","2023","2024","2025","2026"]
    var arrMonth = ["January","February","March","April","May","June","July","August","September","October","November","December"]
    
    @IBOutlet var pickerView: UIPickerView!

    var delegate : MonthYearDelegate?

    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.setDate()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.selectDate), name: NSNotification.Name(rawValue: "selectDate"), object: nil)
    }

    func setDate()
    {
        if((UserDefaults.standard.object(forKey: "SelectedMonthYear")) != nil)
        {
            let selectedPicker  =  UserDefaults.standard.value(forKey: "SelectedMonthYear") as! String
            
            let arrselectpicker = selectedPicker.components(separatedBy: "  ") as NSArray
            
            if(arrselectpicker.count  > 0)
            {
                let selectedmonth = arrMonth.index(of: arrselectpicker.object(at: 0) as! String)! as NSInteger
                pickerView.selectRow(selectedmonth, inComponent: 0, animated: true)
                
                let selectedyear = arrYear.index(of: arrselectpicker.object(at: 1) as! String)! as NSInteger
                pickerView.selectRow(selectedyear, inComponent: 1, animated: true)
            }
            
        }
        else{
            
            let df = DateFormatter()
            df.dateFormat = "MMMM yyyy"
            let date = df.string(from: Date())
            let selectedmonth = arrMonth.index(of: date.components(separatedBy: " ")[0])! as NSInteger
            pickerView.selectRow(selectedmonth, inComponent: 0, animated: true)
            
            let selectedyear = arrYear.index(of: date.components(separatedBy: " ")[1])! as NSInteger
            pickerView.selectRow(selectedyear, inComponent: 1, animated: true)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.dismiss(animated: true, completion: nil)
        
    }
    
    func numberOfComponentsInPickerView(_ pickerView: UIPickerView) -> Int
    {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        if(component == 0)
        {
            return arrMonth.count
        }
        else
        {
            return arrYear.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        if(component == 0)
        {
            return arrMonth[row]
        }
        else
        {
            return arrYear[row]
        }
    }
    
    
    @IBAction func doneButtonClicked(_ sender:UIButton)
    {
        selectDate()
    }
    
    @objc func selectDate()
    {
        self.delegate?.monthYearSelected(arrMonth[pickerView.selectedRow(inComponent: 0)] as NSString, year: arrYear[pickerView.selectedRow(inComponent: 1)] as NSString)
        self.dismiss(animated: true, completion: nil)
    }
}
