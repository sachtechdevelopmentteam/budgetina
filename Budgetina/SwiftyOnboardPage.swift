//
//  customPageView.swift
//  SwiftyOnboard
//
//  Created by Jay on 3/25/17.
//  Copyright © 2017 Juan Pablo Fernandez. All rights reserved.
//

import UIKit

open class SwiftyOnboardPage: UIView {
    
    public var questionsTitle: UILabel = {
        let label = UILabel()
        label.text = "Title"
        label.textAlignment = .center
        label.numberOfLines = 4
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.sizeToFit()
        return label
    }()
    
    public var answerField: UITextField = {
        let label = UITextField()
        label.placeholder = "Enter amount in USD"
        label.textAlignment = .center
        label.keyboardType = UIKeyboardType.numbersAndPunctuation
        label.returnKeyType = UIReturnKeyType.next
        label.sizeToFit()
        return label
    }()
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
    }
    
    func set(style: SwiftyOnboardStyle) {
        switch style {
        case .light:
            questionsTitle.textColor = .white
            answerField.textColor = .white
        case .dark:
            questionsTitle.textColor = .black
            answerField.textColor = .black
        }
    }
    
    func setUp() {

        let margin = self.layoutMarginsGuide

        self.addSubview(questionsTitle)
        questionsTitle.translatesAutoresizingMaskIntoConstraints = false
        questionsTitle.leftAnchor.constraint(equalTo: margin.leftAnchor, constant: 30).isActive = true
        questionsTitle.rightAnchor.constraint(equalTo: margin.rightAnchor, constant: -30).isActive = true
        questionsTitle.topAnchor.constraint(equalTo: margin.topAnchor, constant: 100).isActive = true
        questionsTitle.heightAnchor.constraint(equalToConstant: 200).isActive = true
        
        self.addSubview(answerField)
        answerField.translatesAutoresizingMaskIntoConstraints = false
        answerField.leftAnchor.constraint(equalTo: margin.leftAnchor, constant: 30).isActive = true
        answerField.rightAnchor.constraint(equalTo: margin.rightAnchor, constant: -30).isActive = true
        answerField.topAnchor.constraint(equalTo: questionsTitle.bottomAnchor, constant: 10).isActive = true
        answerField.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
}
