//
//  SubCategoriesQuestionsViewController.swift
//  Budgetina
//
//  Created by HANDA, KUNWAR on 2/23/19.
//  Copyright © 2019 WeEnggs Technology. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth

class SubCategoriesQuestionsViewController: UIViewController {
    var swiftyOnboard: SwiftyOnboard!
    var titleArray: NSArray = NSArray()
    var selectedDataCategoriesArray: [Dictionary<String, String>] = [Dictionary<String, String>]()
    var ref: DatabaseReference!

    var _currentTextfieldValue = 0.0 as CGFloat
    var currentIndex : Int = 0
    var dataTotalValues : NSArray = NSArray()

    var gradient: CAGradientLayer = {
        //Gradiant for the background view
        let blue = UIColor(red: 69/255, green: 127/255, blue: 202/255, alpha: 1.0).cgColor
        let purple = UIColor(red: 166/255, green: 172/255, blue: 236/255, alpha: 1.0).cgColor
        let gradiant = CAGradientLayer()
        gradiant.colors = [purple, blue]
        gradiant.startPoint = CGPoint(x: 0.5, y: 0.18)
        return gradiant
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ref = Database.database().reference()
        swiftyOnboard = SwiftyOnboard(frame: view.frame, style: .light)
        swiftyOnboard.shouldSwipe = false
        view.addSubview(swiftyOnboard)
        swiftyOnboard.dataSource = self
        swiftyOnboard.delegate = self
        
        if UserDefaults.standard.value(forKey: "selectedDataCategories") != nil {
            selectedDataCategoriesArray = UserDefaults.standard.value(forKey: "selectedDataCategories") as! Array
            
        }
        print("selectedDataCategoriesArray")

        print(selectedDataCategoriesArray)

        // Do any additional setup after loading the view.
    }

    
    @objc func handleContinue(sender: UIButton) {
        
        print("open home page")
        
        UserDefaults.standard.set(true, forKey: "initialSetupDone")
        UserDefaults.standard.synchronize()
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.showMainTabbar()
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension SubCategoriesQuestionsViewController: SwiftyOnboardDelegate, SwiftyOnboardDataSource, UITextFieldDelegate {
    
    func swiftyOnboardNumberOfPages(_ swiftyOnboard: SwiftyOnboard) -> Int {
        //Number of pages in the onboarding:
        return selectedDataCategoriesArray.count
    }
    
    func swiftyOnboardBackgroundColorFor(_ swiftyOnboard: SwiftyOnboard, atIndex index: Int) -> UIColor? {
        //Return the background color for the page at index:
        return #colorLiteral(red: 0.2666860223, green: 0.5116362572, blue: 1, alpha: 1)
    }
    
    func swiftyOnboardPageForIndex(_ swiftyOnboard: SwiftyOnboard, index: Int) -> SwiftyOnboardPage? {
        let view = SwiftyOnboardPage()
        
        
        //Set the font and color for the labels:
        view.questionsTitle.font = UIFont(name: "Lato-Heavy", size: 22)
        //view.questionsTitle.backgroundColor = .gray
        view.answerField.font = UIFont(name: "Lato-Regular", size: 16)
        //view.answerField.backgroundColor = .gray
        view.answerField.delegate = self
        //Set the text in the page:
        let dic = selectedDataCategoriesArray[index] as Dictionary<String, String>

        view.questionsTitle.text = dic["categoryname"]
        // view.answerField.text = subTitleArray[index]
        
        //Return the page for the given index:
        return view
    }
    
    func swiftyOnboardViewForOverlay(_ swiftyOnboard: SwiftyOnboard) -> SwiftyOnboardOverlay? {
        let overlay = SwiftyOnboardOverlay()
        overlay.skipButton.isHidden = true
        overlay.continueButton.isHidden = true
        //Setup targets for the buttons on the overlay view:
        //        overlay.skipButton.addTarget(self, action: #selector(handleSkip), for: .touchUpInside)
        overlay.continueButton.addTarget(self, action: #selector(handleContinue), for: .touchUpInside)
        //
        //        //Setup for the overlay buttons:
        //        overlay.continueButton.titleLabel?.font = UIFont(name: "Lato-Bold", size: 16)
        //        overlay.continueButton.setTitleColor(UIColor.white, for: .normal)
        //        overlay.skipButton.setTitleColor(UIColor.white, for: .normal)
        //        overlay.skipButton.titleLabel?.font = UIFont(name: "Lato-Heavy", size: 16)
        
        //Return the overlay view:
        return overlay
    }
    
    func swiftyOnboardOverlayForPosition(_ swiftyOnboard: SwiftyOnboard, overlay: SwiftyOnboardOverlay, for position: Double) {
        let currentPage = round(position)
        overlay.pageControl.currentPage = Int(currentPage)
        print(Int(currentPage))
        overlay.continueButton.tag = Int(position)
        
        if Int(currentPage) < selectedDataCategoriesArray.count - 1 {
            overlay.continueButton.setTitle("Continue", for: .normal)
            overlay.skipButton.setTitle("Skip", for: .normal)
            overlay.skipButton.isHidden = true
        } else {
            overlay.continueButton.setTitle("Continue to home!", for: .normal)
            overlay.continueButton.isHidden = false
            overlay.skipButton.isHidden = true
        }
    }
    
    // MARK: TextField Delegate Method
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        let toolBar = UIToolbar(frame: CGRect(x: 0,y: 0,width: self.view.frame.size.width,height: 40))
        toolBar.barStyle = UIBarStyle.default
        
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        textField.inputAccessoryView = toolBar
        
        
        //tableCategory.scrollEnabled = false
        
        let val = (textField.text?.replacingOccurrences(of: "$", with: ""))! as NSString
        _currentTextfieldValue = CGFloat(val.floatValue)
        
        
        //_currentIndexPath = tableCategory.indexPathForCell(cell)
        
        
        if textField.text == "$0.00"
        {
            textField.text = "$"
        }
        textField.becomeFirstResponder()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        
        var object = Dictionary<String, String>()

        object = selectedDataCategoriesArray[currentIndex] as Dictionary<String, String>
        
        let user = Auth.auth().currentUser
        // users/[userid]/spending/year/month/[parent categoryid]/[spending categoryid]/AMOUNT
        let dataRef =  self.ref.child("users").child(user?.uid ?? "nil").child("spending").child(Date().yearAsString()).child(Date().monthAsString()).childByAutoId()
   
        let key = self.ref.child("users").child(user?.uid ?? "nil").child("spending").child(Date().yearAsString()).child(Date().monthAsString()).childByAutoId().key
        
        dataRef.updateChildValues(["parentcategoryid":(object["parentcategoryid"] ?? "nil"),
                                   "categoryid":(object["categoryid"] ?? "nil"),
                                   "categoryname":(object["categoryname"] ?? "nil"),
                                   "timestamp":String(NSDate().timeIntervalSince1970),
                                   "id":key!,
                                   "isRecursive":"1",
                                   "amount":textField.text!])
            
        
        currentIndex = currentIndex + 1
        swiftyOnboard?.goToPage(index: currentIndex, animated: true)
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if let range = string.rangeOfCharacter(from: CharacterSet.decimalDigits) {
            print("start index: \(range.lowerBound), end index: \(range.upperBound)")
            
            return true
        }
        else {
            print("no data")
            
            if ((string == "") && (textField.text?.count != 1))
            {
                return true
            }
            return false
        }
    }
    
}
