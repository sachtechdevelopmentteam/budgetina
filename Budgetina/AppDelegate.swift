//
//  AppDelegate.swift
//  Budgetina
//
//  Created by Ovedgroup LLC on 28/10/15.
//  Copyright © 2015 Ovedgroup LLC. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
  
    var window: UIWindow?
    
    func showMainTabbar() ->Void
    {        
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let homeViewController = mainStoryboard.instantiateViewController(withIdentifier: "HomeViewController")

        appdelegate.window?.rootViewController = homeViewController
     
//        try! Auth.auth().signOut()
//        if (totalEssentials == 32)
//        { //32 sub categories was till 8th Dec. on 9th Dec adding 2 more categories to essential.
//            DBHandler.genericQuery("insert into Categories values(null, 'PET EXPENSE - Food & Veterinary Care', 1)")
//            DBHandler.genericQuery("insert into Categories values(null, 'COMPUTER EXPENSE', 1)")
//            DBHandler.genericQuery("UPDATE Categories SET categoryname = 'ENTERTAINMENT - Dining Out' WHERE  categoryid = 36")
//        }
//
//        if(totalEssentials == 34)
//        {
//            DBHandler.genericQuery("UPDATE Categories SET categoryname = 'ENTERTAINMENT - Dining Out' WHERE  categoryid = 36")
//        }
    }
    
    func showLoginScreen() ->Void
    {
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let ViewController = mainStoryboard.instantiateViewController(withIdentifier: "MainNavController")
        appdelegate.window?.rootViewController = ViewController

    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool
    {
        FirebaseApp.configure()
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}


// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromNSAttributedStringKey(_ input: NSAttributedString.Key) -> String {
	return input.rawValue
}
