//
//  WorkshopVC.swift
//  Budgetina
//
//  Created by Ovedgroup LLC on 08/12/15.
//  Copyright © 2015 Ovedgroup LLC. All rights reserved.
//

import UIKit
import StoreKit

class WorkshopVC: UIViewController,SKProductsRequestDelegate,SKPaymentTransactionObserver, UITextFieldDelegate
{
    let defaults = UserDefaults.standard
    
    @IBOutlet var chartLegendView: UIView!
    @IBOutlet var scrollView: UIScrollView!
    
    @IBOutlet var viewGraph: UIView!
    @IBOutlet var txtLoanAmount: UITextField!
    
    @IBOutlet var btnCalculate: UIButton!
    
    @IBOutlet var txtMonthlyPayment: UITextField!
    
    @IBOutlet var txtInterestRat: UITextField!
    
  
    @IBOutlet var lblMonthsToPayoff: UILabel!
    @IBOutlet var lblTotalInterestPaid: UILabel!
    
    var product_id = NSString()
    
    var _currentTextfield = UITextField()
    var _currentTextfieldValue:String = ""

    var Month:Int = 0
	var LoanBalance:Float = 0.0
	var TotalPaymentsToDate:Float = 0.0
	var PrincipalPaidToDate:Float = 0.0
	var InterestPaidToDate:Float = 0.0
    var currentAmount = "0"
    var currentPayment = "0"
    var currentInterest = "0"
    var loanAmount:Float = 0.00
    var interest:Float = 0.00
    var MonthyPayment:Float = 0.00
    
    @IBAction func btnAppPurchaseAction(_ sender: UIButton)
    {
        if(defaults.bool(forKey: "purchased"))
        {
            
        }
        else
        {
            
        }
        
        if (SKPaymentQueue.canMakePayments())
        {
            let productID:NSSet = NSSet(object: self.product_id)
            let productsRequest:SKProductsRequest = SKProductsRequest(productIdentifiers: productID as! Set<String>)
            productsRequest.delegate = self
            productsRequest.start()
            print("Fething Products")
        }
        else
        {
            print("can't make purchases");
        }
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 200/225, green: 185/225, blue: 207/225, alpha: 1.0)
        self.automaticallyAdjustsScrollViewInsets = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        let label: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width - 20, height: 50))
        label.backgroundColor = UIColor.clear
        label.numberOfLines = 2
        label.font = UIFont.boldSystemFont(ofSize: 16.0)
        label.textAlignment = .center
        label.textColor = UIColor.black
        label.text = "Credit Cards & Loan Debt Payoff Calculator"
        self.navigationItem.titleView = label
        
        //self.navigationItem.title = "Credit Cards & Loan Debt Payoff Calculator"
        
        btnCalculate.layer.borderColor = UIColor.white.cgColor
        btnCalculate.layer.borderWidth = 1
        product_id = ""
        
        SKPaymentQueue.default().add(self)
        
        txtLoanAmount.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControl.Event.editingChanged)

    }
    
    override func viewWillAppear(_ animated: Bool) {
         //loadChart()
    }
    
    func buyProduct(_ product: SKProduct)
    {
        print("Sending the Payment Request to Apple")
        let payment = SKPayment(product: product)
        SKPaymentQueue.default().add(payment)
    }
    
    func productsRequest (_ request: SKProductsRequest, didReceive response: SKProductsResponse)
    {
        
        let count : Int = response.products.count
        if (count>0)
        {
            //var validProducts = response.products
            let validProduct: SKProduct = response.products[0] as SKProduct
            if (validProduct.productIdentifier == self.product_id as String)
            {
                print(validProduct.localizedTitle)
                print(validProduct.localizedDescription)
                print(validProduct.price)
                buyProduct(validProduct);
            } else
            {
                print(validProduct.productIdentifier)
            }
        }

        else    
        {
           // print("nothing")
            let alert = UIAlertController (title:"Alert", message:"Prouct Not Found" as String, preferredStyle: UIAlertController.Style.alert)
            
            alert.addAction(UIAlertAction(title:"OK", style: UIAlertAction.Style.default, handler: nil))
            
            self.present(alert, animated:true, completion: nil)
        }
    }
    
    
    func request(_ request: SKRequest, didFailWithError error: Error)
    {
        print("Error Fetching product information");
    }
    
    func paymentQueue(_ _queue: SKPaymentQueue,
        updatedTransactions transactions: [SKPaymentTransaction])
    {
        print("Received Payment Transaction Response from Apple")
        
        for transaction:AnyObject in transactions {
            if let trans:SKPaymentTransaction = transaction as? SKPaymentTransaction{
                switch trans.transactionState {
                case .purchased:
                    print("Product Purchased");
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    defaults.set(true , forKey: "purchased")
                    //overlayView.hidden = true
                    break;
                    
                case .failed:
                    print("Purchased Failed");
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    break;
                    
                case .restored:
                    print("Already Purchased");
                    SKPaymentQueue.default().restoreCompletedTransactions()
                    
                    
                default:
                    break;
                }
            }
        }

    }
    
    //CC Debt loan payoff calculator
    
    @IBAction func btnCalculateClicked()
    {
        self.view.endEditing(true)
   
        self.loanAmount = Float(currentAmount)!
        self.interest = Float(txtInterestRat.text!.replacingOccurrences(of: "%", with: ""))!
        self.MonthyPayment = Float(currentPayment)!
        
        if(self.loanAmount > 0 && self.interest > 0 && self.MonthyPayment > 0)
        {
            self.Month = 0
            self.LoanBalance = self.loanAmount
            self.PrincipalPaidToDate = 0.00
            self.InterestPaidToDate = 0.00
            self.TotalPaymentsToDate = 0.00
            calculate()
        }
    }
    
    func calculate()
    {
        print("Month| Loan Balance | Total payments | Principal paid | Interest paid")
        while self.PrincipalPaidToDate < self.loanAmount //self.LoanBalance > 0//
        {
            self.LoanBalance = Float(String(format: "%.2f", self.LoanBalance * (1 + (self.interest / 100 / 12)) - self.MonthyPayment))!
            
            if(LoanBalance >= 0){
                if(PrincipalPaidToDate >= 0){
                self.Month += 1
                self.TotalPaymentsToDate = Float(String(format: "%.2f", MonthyPayment * Float(self.Month)))!
                self.PrincipalPaidToDate = Float(String(format: "%.2f", self.loanAmount - self.LoanBalance))!
                self.InterestPaidToDate = Float(String(format: "%.2f", self.TotalPaymentsToDate - self.PrincipalPaidToDate))!
            
                print("\(self.Month)    |   \(LoanBalance)    |     \(TotalPaymentsToDate)      |       \(PrincipalPaidToDate)   |   \(InterestPaidToDate)")
                }else{
                    print("Can not calculate")
                    self.Month = 0
                    self.InterestPaidToDate = 0
                    self.currentPayment = "0"
                    self.txtMonthlyPayment.text = "$0.00"
                    let alert = UIAlertController(title: "Alert", message: "You have entered an incorrect value that cannot be calculated. Please try again.", preferredStyle: UIAlertController.Style.alert)
                    
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    
                    self.present(alert, animated: true, completion: nil)
                    
                    break
                }
            }
            else{
                break
            }
        }
        
       //print(self.Month)
       
        print(PrincipalPaidToDate)
        lblTotalInterestPaid.text = formatAmount(NSNumber(value: self.InterestPaidToDate as Float))
        lblMonthsToPayoff.text = String(format: "%d",self.self.Month)
        
        setChart()
    }

    
    func setChart() {
        
      //  pieChartView.isHidden = false
        chartLegendView.isHidden = false
        
       // scrollView.layer.borderWidth = 2
       // scrollView.layer.borderColor = UIColor.yellowColor().CGColor
        scrollView.contentSize.height =  650//chartLegendView.frame.origin.y + chartLegendView.frame.size.height + 20
        print(scrollView.contentSize)
        
        let totalPayment = self.loanAmount + self.InterestPaidToDate
        let totalInterest = Double((self.InterestPaidToDate/totalPayment)*100)
        let principalAmount = Double((self.loanAmount/totalPayment)*100)
        
        
        var dataPoints =  [String(format: "%.1f%%", totalInterest),String(format: "%.1f%%",principalAmount)]
        var values = [totalInterest, principalAmount]
        
//        var chartDataEntries: [PieChartDataEntry] = []
//
//        for i in 0..<values.count {
//
//            let chartDataEntry = PieChartDataEntry(value: Double(values[i]), label: dataPoints[i], data:  values[i] as AnyObject)
//
//            chartDataEntries.append(chartDataEntry)
//        }
//
//        let pieChartDataSet = PieChartDataSet(values: chartDataEntries, label: "")
//
//        let colors: [UIColor] = [UIColor(red: 255/255, green: 107/255, blue: 65/255, alpha: 1.0), UIColor(red: 96/255, green: 186/255, blue: 113/255, alpha: 1.0)]
//
//
//        pieChartDataSet.colors = colors
//        pieChartDataSet.drawValuesEnabled = false
//        let pieChartData = PieChartData(dataSet: pieChartDataSet)
//        self.pieChartView.data = pieChartData
//        //self.pieChartView.animate(xAxisDuration: NSTimeInterval(5))
//        self.pieChartView.drawHoleEnabled = false
//        self.pieChartView.chartDescription?.text = ""
//        self.pieChartView.rotationEnabled = false
//        self.pieChartView.drawMarkers = false
//        self.pieChartView.isUserInteractionEnabled = false
//
    }
    
    func formatAmount(_ number:NSNumber) -> String{
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.currencySymbol = "$"                  // <--------
        formatter.currencyGroupingSeparator = ","       // <--------
        
       // _currentTextfield.text = formatter.stringFromNumber(number)!
        return formatter.string(from: number)!
    }
    
    func formatInterest(_ val:String) -> String{
        let str = "\(val)%"
        return str
    }
    
    // MARK: TextField Delegate Method
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        let toolBar = UIToolbar(frame: CGRect(x: 0,y: 0,width: self.view.frame.size.width,height: 40))
        toolBar.barStyle = UIBarStyle.default
        
        toolBar.items = [UIBarButtonItem(title: "Cancel", style:UIBarButtonItem.Style.plain, target: self, action: #selector(self.btnKeyboardCancelAction)),
                         UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil),
                         UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.btnKeyboardDoneAction))]
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        textField.inputAccessoryView = toolBar
        
        _currentTextfield = textField
//        textField.text = ""
//        let val = (textField.text?.stringByReplacingOccurrencesOfString("$", withString: ""))! as NSString
//        _currentTextfieldValue = val as String
        
        if(_currentTextfield != txtInterestRat)
        {
            if textField.text == "$0.00"
            {
                textField.text = "$"
            }
            textField.becomeFirstResponder()
            
//            currentString = (_currentTextfield == txtLoanAmount) ? txtLoanAmount.text! : txtMonthlyPayment.text!
//            currentString =  currentString.stringByReplacingOccurrencesOfString(" ", withString: "")
//            currentString =  currentString.stringByReplacingOccurrencesOfString("$", withString: "")
//            currentString =  currentString.stringByReplacingOccurrencesOfString(".00", withString: "")
//            currentString =  currentString.stringByReplacingOccurrencesOfString(",", withString: "")
        }
        else{
            if textField.text == "0.00%"
            {
                textField.text = "%" // "%"
                //pos = 1
            }
            else{
               // pos = 1
            }
            
            let pos: Int = textField.text!.count - 1
      
            if let newPosition = textField.position(from: textField.beginningOfDocument, in: UITextLayoutDirection.right, offset: pos) {
                
                textField.selectedTextRange = textField.textRange(from: newPosition, to: newPosition)
            }
            
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if(_currentTextfield == txtLoanAmount && currentAmount == "0")
        {
            txtLoanAmount.text = String(format: "$%.2f", currentAmount)
        }
        else if(_currentTextfield == txtMonthlyPayment && currentPayment == "0")
        {
            txtMonthlyPayment.text = String(format: "$%.2f", currentPayment)
        }
        else if(_currentTextfield == txtInterestRat && currentInterest == "0"){
            txtInterestRat.text = String(format: "%.2f%@", currentInterest, "%")
        }
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
//        textField.text = formatAmount(Int(textField.text!.stringByReplacingOccurrencesOfString("$", withString: ""))!)
//        print("You typed \(textField.text!)")
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if((textField.text! as NSString).replacingCharacters(in: range, with: string).characters.count > 18)
        {
            return false
        }
        
        if(_currentTextfield != txtInterestRat)
        {
            var currentString = (_currentTextfield == txtLoanAmount) ?  currentAmount : currentPayment
            print(string)
            
        switch string {
        case "0","1","2","3","4","5","6","7","8","9":
            currentString += string
            print(currentString.replacingOccurrences(of: " ", with: ""))
            _currentTextfield.text = formatAmount(NSNumber(value: Int(currentString)!))
        case "": //For backspace
//            if let selectedRange = textField.selectedTextRange {
//                
//                let cursorPosition = textField.offsetFromPosition(textField.beginningOfDocument, toPosition: selectedRange.start)
//                print("\(cursorPosition)")
//                
////                let idx = currentString.startIndex.advancedBy(2)
////                currentString.removeAtIndex(idx)
//
//            }
            if(currentString.count > 1)
            {
                currentString = String(currentString.dropLast())
                print(currentString)
                _currentTextfield.text = formatAmount(NSNumber(value: Int(currentString)!))
            }
            else{
                currentString = ""
                _currentTextfield.text = "$0.00"
            }
            
        default:
            return false
        }
        
            (_currentTextfield == txtLoanAmount) ?  (currentAmount = currentString) : (currentPayment = currentString)
        }
        else if (_currentTextfield == txtInterestRat){
         
           currentInterest = (textField.text! as NSString).replacingCharacters(in: range, with: string)
           print("Current interest : \(currentInterest)")
           currentInterest = currentInterest.replacingOccurrences(of: "%", with: "")
           
            if((currentInterest.contains(".") && currentInterest.count <= 4) || (!currentInterest.contains(".") && currentInterest.characters.count < 3))
            {
            
            
            if(string == "" && txtInterestRat.text == "0.00%")
            {
                return false
            }
            if(string == "")
            {
               currentInterest = String(currentInterest.dropLast())
            }
            
            if(string != "" && currentInterest != "0"){
                //currentInterest += string
                txtInterestRat.text = "%"
            }
           
            txtInterestRat.text = "\(currentInterest)%"
            if(currentInterest == "")
            {
                currentInterest = "0"
                //txtInterestRat.text = "0.00%"
            }
            }else{
                return false
            }
           return false
        }
        return false
    }
    
//    func formatCurrency(string: String) {
//        print("format \(string)")
//        let formatter = NSNumberFormatter()
//        formatter.numberStyle = NSNumberFormatterStyle.CurrencyStyle
//        formatter.locale = NSLocale(localeIdentifier: "en_US")
//        var numberFromField = (NSString(string: (_currentTextfield == txtLoanAmount) ?  currentAmount : currentPayment).doubleValue)/100
//        _currentTextfield.text = formatter.stringFromNumber(numberFromField)
//        print(_currentTextfield.text )
//    }
    
    @objc func btnKeyboardCancelAction()
    {
        view.endEditing(true)
    }
    
    @objc func btnKeyboardDoneAction()
    {
        view.endEditing(true)
    }
   
}
        
