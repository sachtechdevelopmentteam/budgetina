//
//  HelpVC.swift
//  Budgetina
//
//  Created by Ovedgroup LLC on 19/11/15.
//  Copyright © 2015 Ovedgroup LLC. All rights reserved.
//

import UIKit
import MessageUI

class HelpVC: UIViewController,UITextViewDelegate,UITextFieldDelegate,MFMailComposeViewControllerDelegate,SKPSMTPMessageDelegate
{
    
    @IBOutlet var txtName: UITextField!
    @IBOutlet var txtEmail: UITextField!
    @IBOutlet var txtMessage: UITextView!
    
    @IBAction func btnSubmitAction(_ sender: UIButton)
    {
        print("Submit button clicked..")
        if(txtName.text == "")
        {
            ShowMessage("Enter Name")
        }
        else if(txtEmail.text == "")
        {
            ShowMessage("Enter Email")
        }
        else if(!self.isValidEmail(txtEmail.text!))
        {
            ShowMessage("Enter valid Email")
        }
        else if(txtMessage.text == "" || txtMessage.text == "Please let us know how we can help you")
        {
            ShowMessage("Enter Message")
        }
        else
        {
            
            //http://192.169.173.23/web_services/service.php?op=send_email&email=&name=&message=&access_token=
            
          sendEmail(sender)

        }
            
    }

    func sendEmail(_ sender: UIButton){
        
        sender.isEnabled = false
        print("Submit button disabled")
        var url = NSString(format: "%@?op=send_email&email=%@&name=%@&message=%@&access_token=",SERVER_URL,self.txtEmail.text!, self.txtName.text!, self.txtMessage.text!)
        url = url.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
        
        var request: URLRequest = URLRequest(url: URL(string: url as String)!)
        request.httpMethod = "GET"
        request.timeoutInterval = 300
        
        let task = URLSession.shared.dataTask(with: request)
        { (data, response, error) -> Void in
            
                if(error != nil)
                {
                    //self.ActivityIndicator.stopAnimating()
                   // print(error)
                    self.ShowMessage(error.debugDescription as NSString)
                }
                else{
                    do {
                        if  let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                        {
                            print(jsonResult)
                            DispatchQueue.main.sync
                            {
                                sender.isEnabled = true
                                print("Submit button enabled again")
                                if (((jsonResult["success"]! as AnyObject).boolValue) == true)
                                {
                                    //self.ActivityIndicator.stopAnimating()
                                    self.ShowMessage("Your message has been sent successfully")
                                    self.txtName.text = ""
                                    self.txtEmail.text = ""
                                    self.txtMessage.text = ""
                                }
                                else
                                {
                                   // self.ActivityIndicator.stopAnimating()
                                    self.ShowMessage(jsonResult["message"]! as! NSString)
                                }
                            }
                        }
                    } catch
                    {
                        print(error)
                    }
                }
        }
        
        task.resume()
    }

    func sendSMTPEmail()
    {
        let name = txtName.text!
        let Email = txtEmail.text!
        let Message = txtMessage.text!
        
        let sendEmail: SKPSMTPMessage = SKPSMTPMessage()
        sendEmail.fromEmail = Email as String
        sendEmail.toEmail = "vtnavadiya@gmail.com"
        sendEmail.requiresAuth = true
        sendEmail.relayHost = "smtp.gmail.com"
        sendEmail.login = "Contact@budgetina.com"
        sendEmail.pass = "Anat0527!"
        sendEmail.subject = "Budgetina Email Testing"
        sendEmail.wantsSecure = true
        
        sendEmail.delegate = self
        sendEmail.relayPorts = [Int(587)] //8025
        
        let Emaildata = NSString(format: "Name: %@ </br> Email: %@ </br> Message: %@", name,Email,Message)
        
        let dicdata: NSDictionary = [kSKPSMTPPartContentTypeKey:"text/html",kSKPSMTPPartMessageKey:Emaildata,kSKPSMTPPartContentTransferEncodingKey:"8bit"]
        sendEmail.parts = [dicdata]
        
        sendEmail.send()
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 200/225, green: 185/225, blue: 207/225, alpha: 1.0)
        self.automaticallyAdjustsScrollViewInsets = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        self.navigationItem.title = "Contact Us"

        txtMessage.text = "Please let us know how we can help you"
        txtMessage.textAlignment = .center
        txtMessage.textColor = UIColor.lightGray
    }
    
    func ShowMessage(_ message:NSString)
    {
        let alert = UIAlertController (title:"Alert", message:message as String, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title:"OK", style: UIAlertAction.Style.default, handler: nil))
        
        self.present(alert, animated:true, completion: nil)
    }
    
    func isValidEmail(_ testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }

    //MARK: SKPSMTP Message Delegate Method
    
    func messageSent(_ message: SKPSMTPMessage!)
    {
        NSLog("Message sent")
        
        let alert = UIAlertController (title:"Alert", message:"Message sent sucessfully" as String, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title:"OK", style: UIAlertAction.Style.default, handler: nil))
        
        self.present(alert, animated:true, completion: nil)
        txtName.text = ""
        txtEmail.text = ""
        txtMessage.text = ""

    }
    
    func messageFailed(_ message: SKPSMTPMessage!, error: Error!)
    {
        let alert = UIAlertController (title:"Alert", message:"Failed to sent" as String, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title:"OK", style: UIAlertAction.Style.default, handler: nil))
        
        self.present(alert, animated:true, completion: nil)
    }
    
         //MARK: Textfield Delegate Method
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if(textField == txtName)
        {
            txtEmail.becomeFirstResponder()
        }
        else
        {
            txtEmail.resignFirstResponder()
        }
        return true
    }
         //MARK: TextView Delegate Method
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        if(text == "\n")
        {
            txtMessage.resignFirstResponder()
            return false
        }
        return true

    }
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        if textView.textColor == UIColor.lightGray
        {
            textView.text = nil
            textView.textColor = UIColor.black
            textView.textAlignment = .left
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView)
    {
        
        if textView.text.isEmpty
        {
            textView.text = "Please let us know how we can help you"
            txtMessage.textAlignment = .center
            txtMessage.textColor = UIColor.lightGray
        }
    }
    
}
