//
//  YearlyOverviewVC.swift
//  Budgetina
//
//  Created by Ovedgroup LLC on 19/11/15.
//  Copyright © 2015 Ovedgroup LLC. All rights reserved.
//

import UIKit

class YearlyOverviewVC: UIViewController,PiechartDelegate
{
    
    @IBOutlet var viewOverviewDetail: UIView!
    @IBOutlet var viewgraph: UIView!
    @IBOutlet var lblMessage: UILabel!
    @IBOutlet var lblIncome: UILabel!
    @IBOutlet var lblEssential: UILabel!
    @IBOutlet var lblLifestyle: UILabel!
    @IBOutlet var lblFinancial: UILabel!
    
    @IBOutlet var btnMonthlyoverview: UIButton!
    @IBOutlet var btnYearlyoverview: UIButton!

    var dataArray = NSMutableArray()
    var dataIncomeAmount = NSMutableArray()
    var dataEssentialAmount = NSMutableArray()
    var dataLifestyleAmount = NSMutableArray()
    var dataFinancialAmount = NSMutableArray()
    
    @IBAction func btnMonthlyOverviewClick(_ sender: UIButton)
    {
        self.showMonthlyChart()
        
    }
    
    @IBAction func btnYearlyOverviewClick(_ sender: UIButton)
    {
        btnMonthlyoverview.layer.borderColor = UIColor.clear.cgColor
        
        btnYearlyoverview.layer.borderWidth = 1
        btnYearlyoverview.layer.borderColor = UIColor.white.cgColor
        
        let dateformat = DateFormatter()
        dateformat.dateFormat = "yyyy"
        var currentyear = dateformat.string(from: Date())
        
        if let selectedPickerMonth  =  UserDefaults.standard.value(forKey: "SelectedMonthYear") as? String{
        
            currentyear = selectedPickerMonth.components(separatedBy: " ")[2]
        }
        
        var views: [String: UIView] = [:]
        
        dataArray = DBHandler.getDataUsingQuery("select * from Income where incomemonthyear like '%\(currentyear)'")
        
        dataIncomeAmount = DBHandler.getDataUsingQuery(NSString(format:"select sum(amount) total from Income where incomemonthyear like '%%%@'",currentyear) as String)
        NSLog("%@", dataIncomeAmount)
        
        
        dataEssentialAmount = DBHandler.getDataUsingQuery(NSString(format: "select sum(amount) total from Spending where spendingmonthyear like '%%%@' AND parentcategoryid = '1'",currentyear) as String)
        NSLog("%@", dataEssentialAmount)
        
        
        dataLifestyleAmount = DBHandler.getDataUsingQuery(NSString(format: "select sum(amount) total from Spending where spendingmonthyear like '%%%@' AND parentcategoryid = '2'", currentyear) as String)
        NSLog("%@", dataLifestyleAmount)
        
        dataFinancialAmount = DBHandler.getDataUsingQuery(NSString(format: "select sum(amount) total from Spending where spendingmonthyear like '%%%@' AND parentcategoryid = '3'", currentyear) as String)
        NSLog("%@", dataFinancialAmount)
        
        if(((dataIncomeAmount[0] as! NSDictionary)["total"] as! String) == ""  && ((dataEssentialAmount[0] as! NSDictionary)["total"] as! String) == "" && ((dataLifestyleAmount[0] as! NSDictionary)["total"] as! String) == "" && ((dataFinancialAmount[0] as! NSDictionary)["total"] as! String) == "")
        {
            lblMessage.isHidden = false
            viewOverviewDetail.isHidden = true
            viewgraph.isHidden = true
            self.view.viewWithTag(101)?.removeFromSuperview()
        }
        else
        {
            viewOverviewDetail.isHidden = false
            lblMessage.isHidden = true
            viewgraph.isHidden = false
            
            var IncomeAmount = 0.0 as CGFloat
            if dataIncomeAmount.count > 0{
                let st = (dataIncomeAmount[0] as! NSDictionary)["total"] as! String
                if NSString(string: st).length > 0{
                    IncomeAmount = CGFloat(Float(st)!)
                }
            }
            
            var EssentialAmount = 0.0 as CGFloat
            if dataEssentialAmount.count > 0{
                
                let st = (dataEssentialAmount[0] as! NSDictionary)["total"] as! String
                if NSString(string: st).length > 0{
                    EssentialAmount = CGFloat(Float(st)!)
                }
            }
            
            var LifestyleAmount = 0.0 as CGFloat
            if dataLifestyleAmount.count > 0{
                
                let st = (dataLifestyleAmount[0] as! NSDictionary)["total"] as! String
                if NSString(string: st).length > 0{
                    LifestyleAmount = CGFloat(Float(st)!)
                }                
            }
            
            var FinanceAmount = 0.0 as CGFloat
            if dataFinancialAmount.count > 0{
                
                let st = (dataFinancialAmount[0] as! NSDictionary)["total"] as! String
                if NSString(string: st).length > 0{
                    FinanceAmount = CGFloat(Float(st)!)
                }
            }
            
            var Income = Piechart.Slice()
            Income.value = 100 - (FinanceAmount/IncomeAmount + LifestyleAmount/IncomeAmount + EssentialAmount/IncomeAmount)
            Income.color = UIColor(red: 23/255, green: 159/255, blue: 152/255, alpha: 1.0)
            //Income.text = "INCOME"
            
            var Financial = Piechart.Slice()
            Financial.value = FinanceAmount/IncomeAmount
            Financial.color = UIColor(red: 250/255, green: 199/255, blue: 26/255, alpha: 1.0)
            //Financial.text = "FINANCIAL"
            
            var Lifestyle = Piechart.Slice()
            Lifestyle.value = LifestyleAmount/IncomeAmount
            Lifestyle.color = UIColor(red: 54/255, green: 130/255, blue: 148/255, alpha: 1.0)
            // Lifestyle.text = "LIFESTYLE"
            
            var Essentials = Piechart.Slice()
            Essentials.value = EssentialAmount/IncomeAmount
            Essentials.color = UIColor(red: 255/255, green: 107/255, blue: 65/255, alpha: 1.0)
            //Essentials.text = "ESSENTIALS"
            
            let IncomePercentage = 100 - (FinanceAmount/IncomeAmount + LifestyleAmount/IncomeAmount + EssentialAmount/IncomeAmount) * 100
            lblIncome.text = NSString(format: "INCOME %.2f%%", IncomePercentage) as String
            
            let FinancialPercentage = FinanceAmount/IncomeAmount * 100.00
            lblFinancial.text = NSString(format: "FINANCIAL %.2f%%", FinancialPercentage) as String
            
            let LifestylePercentage = LifestyleAmount/IncomeAmount * 100.00
            lblLifestyle.text = NSString(format: "LIFESTYLE %.2f%%", LifestylePercentage) as String
            
            let EssentialPercentage = EssentialAmount/IncomeAmount * 100.00
            lblEssential.text = NSString(format: "ESSENTIALS %.2f%%", EssentialPercentage) as String
            
            
            let piechart = Piechart()
            piechart.delegate = self
            //piechart.title = "Service"
            piechart.activeSlice = 0
            //piechart.layer.borderWidth = 1
            piechart.slices = [Income, Financial, Lifestyle , Essentials]
            piechart.tag = 101
            piechart.translatesAutoresizingMaskIntoConstraints = false
            viewgraph.addSubview(piechart)
            views["piechart"] = piechart
            viewgraph.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-[piechart]-|", options: [], metrics: nil, views: views))
            viewgraph.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[piechart(==202)]", options: [], metrics: nil, views: views))
                       
        }
        

    }
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 200/225, green: 185/225, blue: 207/225, alpha: 1.0)
        self.automaticallyAdjustsScrollViewInsets = false
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationItem.title = "Overview"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.post(name: Notification.Name(rawValue: "selectDate"), object: nil)
        self.showMonthlyChart()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
   
    
    func setSubtitle(_ slice: Piechart.Slice) -> String
    {
        return "\(Int(slice.value * 100))% \(String(describing: slice.text))"
    }
    
    func showMonthlyChart()
    {
        btnYearlyoverview.layer.borderColor = UIColor.clear.cgColor
        btnMonthlyoverview.layer.borderWidth = 1
        btnMonthlyoverview.layer.borderColor = UIColor.white.cgColor
        
        let selectedPickerMonth  =  UserDefaults.standard.value(forKey: "SelectedMonthYear") as! String
        
        var views: [String: UIView] = [:]
        
        dataArray = DBHandler.getDataUsingQuery("select * from Income where incomemonthyear like '%\(selectedPickerMonth)'")
        
        dataIncomeAmount = DBHandler.getDataUsingQuery(NSString(format:"select sum(amount) total from Income where incomemonthyear like '%%%@'",selectedPickerMonth) as String)
        NSLog("%@", dataIncomeAmount)
        
        dataEssentialAmount = DBHandler.getDataUsingQuery(NSString(format: "select sum(amount) total from Spending where spendingmonthyear like '%%%@' AND parentcategoryid = '1'",selectedPickerMonth) as String)
        NSLog("%@", dataEssentialAmount)
        
        
        dataLifestyleAmount = DBHandler.getDataUsingQuery(NSString(format: "select sum(amount) total from Spending where spendingmonthyear like '%%%@' AND parentcategoryid = '2'", selectedPickerMonth) as String)
        NSLog("%@", dataLifestyleAmount)
        
        
        dataFinancialAmount = DBHandler.getDataUsingQuery(NSString(format: "select sum(amount) total from Spending where spendingmonthyear like '%%%@' AND parentcategoryid = '3'", selectedPickerMonth) as String)
        NSLog("%@", dataFinancialAmount)
        
        if(((dataIncomeAmount[0] as! NSDictionary)["total"] as! String) == ""  && ((dataEssentialAmount[0] as! NSDictionary)["total"] as! String) == "" && ((dataLifestyleAmount[0] as! NSDictionary)["total"] as! String) == "" && ((dataFinancialAmount[0] as! NSDictionary)["total"] as! String) == "")
        {
            self.lblMessage.isHidden = false
            self.viewOverviewDetail.isHidden = true
            self.viewgraph.isHidden = true
            self.view.viewWithTag(101)?.removeFromSuperview()
            
        }
        else
        {
            viewOverviewDetail.isHidden = false
            lblMessage.isHidden = true
            viewgraph.isHidden = false
            
            
            var IncomeAmount = 0.0 as CGFloat
            if dataIncomeAmount.count > 0{
                let st = (dataIncomeAmount[0] as! NSDictionary)["total"] as! String
                if NSString(string: st).length > 0{
                    IncomeAmount = CGFloat(Float(st)!)
                }
            }
            
            var EssentialAmount = 0.0 as CGFloat
            if dataEssentialAmount.count > 0{
                
                let st = (dataEssentialAmount[0] as! NSDictionary)["total"] as! String
                if NSString(string: st).length > 0{
                    EssentialAmount = CGFloat(Float(st)!)
                }             
                
            }
            
            var LifestyleAmount = 0.0 as CGFloat
            if dataLifestyleAmount.count > 0{
                
                let st = (dataLifestyleAmount[0] as! NSDictionary)["total"] as! String
                if NSString(string: st).length > 0{
                    LifestyleAmount = CGFloat(Float(st)!)
                }
            }
            
            var FinanceAmount = 0.0 as CGFloat
            if dataFinancialAmount.count > 0{
                
                let st = (dataFinancialAmount[0] as! NSDictionary)["total"] as! String
                if NSString(string: st).length > 0{
                    FinanceAmount = CGFloat(Float(st)!)
                }
            }
            
            var MonthIncome = Piechart.Slice()
            MonthIncome.value = 100 - (FinanceAmount/IncomeAmount + LifestyleAmount/IncomeAmount + EssentialAmount/IncomeAmount)
            print(MonthIncome.value)
            MonthIncome.color = UIColor(red: 23/255, green: 159/255, blue: 152/255, alpha: 1.0)
            //Income.text = "INCOME"
            
            var MonthFinancial = Piechart.Slice()
            MonthFinancial.value = FinanceAmount/IncomeAmount
            print(MonthIncome.value)

            MonthFinancial.color = UIColor(red: 250/255, green: 199/255, blue: 26/255, alpha: 1.0)
            //Financial.text = "FINANCIAL"
            
            var MonthLifestyle = Piechart.Slice()
            MonthLifestyle.value = LifestyleAmount/IncomeAmount
            MonthLifestyle.color = UIColor(red: 54/255, green: 130/255, blue: 148/255, alpha: 1.0)
            // Lifestyle.text = "LIFESTYLE"
            
            var MonthEssentials = Piechart.Slice()
            MonthEssentials.value = EssentialAmount/IncomeAmount
            print(MonthEssentials.value)
            MonthEssentials.color = UIColor(red: 255/255, green: 107/255, blue: 65/255, alpha: 1.0)
            //Essentials.text = "ESSENTIALS"
            
            let IncomePercentage = 100 - (FinanceAmount/IncomeAmount + LifestyleAmount/IncomeAmount + EssentialAmount/IncomeAmount) * 100
            lblIncome.text = NSString(format: "INCOME %.2f%%", IncomePercentage) as String
            
            let FinancialPercentage = FinanceAmount/IncomeAmount * 100.00
            lblFinancial.text = NSString(format: "FINANCIAL %.2f%%", FinancialPercentage) as String
            
            let LifestylePercentage = LifestyleAmount/IncomeAmount * 100.00
            lblLifestyle.text = NSString(format: "LIFESTYLE %.2f%%", LifestylePercentage) as String
            
            let EssentialPercentage = EssentialAmount/IncomeAmount * 100.00
            lblEssential.text = NSString(format: "ESSENTIALS %.2f%%", EssentialPercentage) as String
            
            let piechart = Piechart()
            piechart.delegate = self
            //piechart.title = "Service"
            piechart.activeSlice = 0
            //piechart.layer.borderWidth = 1
            piechart.slices = [MonthIncome, MonthFinancial, MonthLifestyle , MonthEssentials]
            
            piechart.translatesAutoresizingMaskIntoConstraints = false
            viewgraph.addSubview(piechart)
            views["piechart"] = piechart
            viewgraph.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-[piechart]-|", options: [], metrics: nil, views: views))
            viewgraph.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[piechart(==202)]", options: [], metrics: nil, views: views))
            piechart.center = self.view.center
        }

    }
    
    /*func setInfo(slice: Piechart.Slice) -> String {
    return "\(Int(slice.value * total))/\(Int(total))"
    }*/
    
    
}



