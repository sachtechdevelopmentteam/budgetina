//
//  Piechart.swift
//  Budgetina
//
//  Created by Ovedgroup LLC on 16/12/15.
//  Copyright © 2015 Ovedgroup LLC. All rights reserved.
//

import UIKit

public protocol PiechartDelegate {
    func setSubtitle(_ slice: Piechart.Slice) -> String
    //func setInfo(slice: Piechart.Slice) -> String
}


/**
 * Piechart
 */
open class Piechart: UIControl {
    
    /**
     * Slice
     */
    
    
    public struct Slice {
        public var color: UIColor!
        public var value: CGFloat!
        public var text: String!
    
    }
    
    /**
     * Radius
     */
    public struct Radius {
        public var inner: CGFloat = 40
        public var outer: CGFloat = 100
    }
    
    /**
     * private
     */
    fileprivate var titleLabel: UILabel!
    fileprivate var subtitleLabel: UILabel!
    fileprivate var infoLabel: UILabel!
    
    
    /**
     * public
     */
    open var radius: Radius = Radius()
    open var activeSlice: Int = 0
    open var delegate: PiechartDelegate?
    
     open var title: String = "title" {
    didSet {
    titleLabel.text = title
    }
    }
    
    open var subtitle: String = "subtitle" {
    didSet {
    subtitleLabel.text = subtitle
    }
    }
    
  /*  public var info: String = "info" {
    didSet {
    infoLabel.text = info
    }
    }*/
    
    open var slices: [Slice] = []    
    
    /**
     * methods
     */
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.clear
        /*
        self.addTarget(self, action: "click", forControlEvents: .TouchUpInside)
        titleLabel = UILabel()
        titleLabel.text = "10%"
        titleLabel.font = UIFont.preferredFontForTextStyle(UIFontTextStyleHeadline)
        titleLabel.textAlignment = .Center
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(titleLabel)
       
        subtitleLabel = UILabel()
        subtitleLabel.text = subtitle
        subtitleLabel.font = UIFont.preferredFontForTextStyle(UIFontTextStyleCaption1)
        subtitleLabel.textColor = UIColor.grayColor()
        subtitleLabel.textAlignment = .Center
        subtitleLabel.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(subtitleLabel)
        
        infoLabel = UILabel()
        infoLabel.text = subtitle
        infoLabel.font = UIFont.preferredFontForTextStyle(UIFontTextStyleCaption1)
        infoLabel.textColor = UIColor.grayColor()
        infoLabel.textAlignment = .Center
        infoLabel.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(infoLabel)
        
        self.addConstraint(NSLayoutConstraint(item: titleLabel, attribute: .CenterX, relatedBy: .Equal, toItem: self, attribute: .CenterX, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: titleLabel, attribute: .Bottom, relatedBy: .Equal, toItem: self, attribute: .CenterY, multiplier: 1, constant: 0))
        
        //self.addConstraint(NSLayoutConstraint(item: subtitleLabel, attribute: .CenterX, relatedBy: .Equal, toItem: self, attribute: .CenterX, multiplier: 1, constant: 0))
       // self.addConstraint(NSLayoutConstraint(item: subtitleLabel, attribute: .Top, relatedBy: .Equal, toItem: self, attribute: .CenterY, multiplier: 1, constant: 0))
        
        self.addConstraint(NSLayoutConstraint(item: infoLabel, attribute: .CenterX, relatedBy: .Equal, toItem: self, attribute: .CenterX, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: infoLabel, attribute: .Top, relatedBy: .Equal, toItem: subtitleLabel, attribute: .Bottom, multiplier: 1, constant: 0))*/
    }
    
    convenience init() {
        self.init(frame: CGRect.zero)
    }
    
    open override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        
        let center = CGPoint(x: bounds.width / 2, y: bounds.height / 2)
        var startValue: CGFloat = 0
        var startAngle: CGFloat = 0
        var endValue: CGFloat = 0
        var endAngle: CGFloat = 0
        
        for (_, slice) in slices.enumerated() {
            
            startAngle = (startValue * 2 * CGFloat(Double.pi/2)) - CGFloat(Double.pi/2_2)
            endValue = startValue + slice.value
            endAngle = (endValue * 2 * CGFloat(Double.pi/2)) - CGFloat(Double.pi/2_2)
            
            let path = UIBezierPath()
            path.move(to: center)
            path.addArc(withCenter: center, radius: radius.outer, startAngle: startAngle, endAngle: endAngle, clockwise: true)
            
            var color = UIColor(red: 230/255.0, green: 230/255.0, blue: 230/255.0, alpha: 1)
          
            //if (index == activeSlice) {
            if true {
                color = slice.color
                // subtitle = delegate?.setSubtitle(slice) ?? "subtitle"
                //info = delegate?.setInfo(slice) ?? "info"
            }
            color.setFill()
            path.fill()
            
            // add white border to slice
            UIColor.clear.setStroke()
            path.stroke()
                        
            
            // increase start value for next slice
            startValue += slice.value
           // val = 16
            
        }
                // create center donut hole
       /* let innerPath = UIBezierPath()
        innerPath.moveToPoint(center)
        innerPath.addArcWithCenter(center, radius: radius.inner, startAngle: 0, endAngle: CGFloat(Double.pi/2) * 2, clockwise: true)
        UIColor.whiteColor().setFill()
        innerPath.fill()*/
    }
    
    func click() {
        activeSlice += 1
        if activeSlice >= slices.count {
            activeSlice = 0
        }
        setNeedsDisplay()
    }
    
}
