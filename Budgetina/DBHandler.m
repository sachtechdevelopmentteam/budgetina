//
//  DBHandler.m
//  ImgScore
//
//  Created by Vipul Patel on 13/10/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "DBHandler.h"
#import <sqlite3.h>

@implementation DBHandler

+(NSString *) getDatabasePath:(NSString *)dbName
{
    // Get the path to the documents directory and append the databaseName

	NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDir = [documentPaths objectAtIndex:0];
    return [documentsDir stringByAppendingPathComponent:dbName];
}

+(void)checkAndCreateDB:(NSString *)dbName dbPath:(NSString *)dbPath
{
    BOOL success;
	
	NSFileManager *fileManager = [NSFileManager defaultManager];
	
	success = [fileManager fileExistsAtPath:dbPath];
	
	if(success) return;
	
	NSString *databasePathFromApp = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:dbName];
	
	[fileManager copyItemAtPath:databasePathFromApp toPath:dbPath error:nil];
}

+(NSMutableArray *) getDataFromTable:(NSString *)tableNameWithAnyCondition{
    NSMutableArray *mainArray = [[NSMutableArray alloc] init];
    sqlite3 *database;
    
    NSString *path = [self getDatabasePath:@"Budget.sqlite"];
    [self checkAndCreateDB:@"Budget.sqlite" dbPath:path];
    
    if (sqlite3_open([path UTF8String], &database) == SQLITE_OK) {
        // delete all records
        sqlite3_stmt *statement;
        NSString *temp_sql = [NSString stringWithFormat:@"Select * from %@", tableNameWithAnyCondition];
        NSLog(@"%@", temp_sql);
        const char *sql = [temp_sql UTF8String] ;
        if (sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
            
            int columnCounter=sqlite3_column_count(statement);
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                NSMutableDictionary *subDict = [[NSMutableDictionary alloc] init];
                for(int i=0;i<columnCounter;i++)
                {
                    NSString * test =[self NullsafeValue:(char *) sqlite3_column_text(statement,i)];
                    NSString * colName = [NSString stringWithUTF8String:(char *)sqlite3_column_name(statement, i)];
                    if(i != columnCounter )
                    {
                        [subDict setObject:test forKey:colName];
                    }
                }
                [mainArray addObject:subDict];
            }
            
            sqlite3_finalize(statement);
        }
        sqlite3_close(database);
    }
    return mainArray;
}

+(NSMutableArray *) getDataUsingQuery:(NSString *)query{
    NSMutableArray *mainArray = [[NSMutableArray alloc] init];
    sqlite3 *database;
    
    NSString *path = [self getDatabasePath:@"Budget.sqlite"];
    [self checkAndCreateDB:@"Budget.sqlite" dbPath:path];
    
    if (sqlite3_open([path UTF8String], &database) == SQLITE_OK) {
        // delete all records
        sqlite3_stmt *statement;
        NSString *temp_sql = [NSString stringWithFormat:@"%@", query];
        NSLog(@"%@", temp_sql);
        const char *sql = [temp_sql UTF8String] ;
        if (sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
            
            int columnCounter=sqlite3_column_count(statement);
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                NSMutableDictionary *subDict = [[NSMutableDictionary alloc] init];
                for(int i=0;i<columnCounter;i++)
                {
                    NSString * test =[self NullsafeValue:(char *) sqlite3_column_text(statement,i)];
                    NSString * colName = [NSString stringWithUTF8String:(char *)sqlite3_column_name(statement, i)];
                    if(i != columnCounter )
                    {
                        [subDict setObject:test forKey:colName];
                    }
                }
                [mainArray addObject:subDict];
            }
            
            sqlite3_finalize(statement);
        }
        sqlite3_close(database);
    }
    return mainArray;
}

+(BOOL) checkMonthAlreadyAddedForCategory:(NSString *)query{
    sqlite3 *database;
    
    NSString *path = [self getDatabasePath:@"Budget.sqlite"];
    [self checkAndCreateDB:@"Budget.sqlite" dbPath:path];
    
    if (sqlite3_open([path UTF8String], &database) == SQLITE_OK) {
        // delete all records
        sqlite3_stmt *statement;
        NSString *temp_sql = [NSString stringWithFormat:@"%@", query];
        NSLog(@"%@", temp_sql);
        const char *sql = [temp_sql UTF8String] ;
        if (sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
            
           // int columnCounter=sqlite3_column_count(statement);
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                sqlite3_finalize(statement);
                sqlite3_close(database);
                return true;
            }
            sqlite3_finalize(statement);
            sqlite3_close(database);
            return false;
        }
    }
    return false;
}

+(NSInteger) getTotalSubCategoriesForCategory:(NSString *)query{
    sqlite3 *database;
    
    NSString *path = [self getDatabasePath:@"Budget.sqlite"];
    [self checkAndCreateDB:@"Budget.sqlite" dbPath:path];
    
    if (sqlite3_open([path UTF8String], &database) == SQLITE_OK) {
        // delete all records
        sqlite3_stmt *statement;
        NSString *temp_sql = [NSString stringWithFormat:@"%@", query];
        NSLog(@"%@", temp_sql);
        const char *sql = [temp_sql UTF8String] ;
        if (sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
            
            int columnCounter=sqlite3_column_count(statement);
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                int total = -1;
                for(int i=0;i<columnCounter;i++)
                {
                    total = sqlite3_column_int(statement,i);
                    
                }
                
                sqlite3_finalize(statement);
                sqlite3_close(database);
                return total;
            }
            sqlite3_finalize(statement);
            sqlite3_close(database);
            return -1;
        }
    }
    return -1;
}


+(BOOL) genericQuery:(NSString *)temp_sql{
	NSString *path = [self getDatabasePath:@"Budget.sqlite"];
    [self checkAndCreateDB:@"Budget.sqlite" dbPath:path];
	sqlite3 *database;
	
	if (sqlite3_open([path UTF8String], &database) == SQLITE_OK) {
		
		//NSLog(@"%@",temp_sql);
		const char *sql = [temp_sql UTF8String];
		char *err;
		if ( sqlite3_exec(database,sql,NULL,NULL,&err)!=SQLITE_OK) {
			NSLog(@"ERROR: %@ - %s", temp_sql, err);
			sqlite3_close(database);
			return FALSE;
		}else {
			NSLog(@"%@ - performed OK.", temp_sql);
			return TRUE;
		}
	}
	return FALSE;
}

+(NSString *)NullsafeValue:(char *)value{
    if (value == nil) {
        return @"";
    }
    return [NSString stringWithUTF8String:value];
}

-(id)init {
    
    //alert view
    self = [super init];

    
    return self;
}

+(void) addSignature:(UIImage *) imgSignature onPDFData:(NSData *)pdfData atPosition:(CGRect )rect {
    
    NSMutableData* outputPDFData = [[NSMutableData alloc] init];
    CGDataConsumerRef dataConsumer = CGDataConsumerCreateWithCFData((CFMutableDataRef)outputPDFData);
    
    CFMutableDictionaryRef attrDictionary = NULL;
    attrDictionary = CFDictionaryCreateMutable(NULL, 0, &kCFTypeDictionaryKeyCallBacks, &kCFTypeDictionaryValueCallBacks);
    CFDictionarySetValue(attrDictionary, kCGPDFContextTitle, CFSTR("My Doc"));
    CGContextRef pdfContext = CGPDFContextCreate(dataConsumer, NULL, attrDictionary);
    CFRelease(dataConsumer);
    CFRelease(attrDictionary);
    CGRect pageRect;
    
    // Draw the old "pdfData" on pdfContext
    CFDataRef myPDFData = (__bridge CFDataRef) pdfData;
    CGDataProviderRef provider = CGDataProviderCreateWithCFData(myPDFData);
    CGPDFDocumentRef pdf = CGPDFDocumentCreateWithProvider(provider);
    CGDataProviderRelease(provider);
    CGPDFPageRef page = CGPDFDocumentGetPage(pdf, 1);
    pageRect = CGPDFPageGetBoxRect(page, kCGPDFMediaBox);
    CGContextBeginPage(pdfContext, &pageRect);
    CGContextDrawPDFPage(pdfContext, page);
    
    // Draw the signature on pdfContext
    pageRect = rect;//CGRectMake(230, 630,imgSignature.size.width , imgSignature.size.height);
    CGImageRef pageImage = [imgSignature CGImage];
    CGContextDrawImage(pdfContext, pageRect, pageImage);
    
    // release the allocated memory
    CGPDFContextEndPage(pdfContext);
    CGPDFContextClose(pdfContext);
    CGContextRelease(pdfContext);
    
    // write new PDFData in "outPutPDF.pdf" file in document directory
    NSString *docsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *pdfFilePath =[NSString stringWithFormat:@"%@/SpendingReport.pdf",docsDirectory];
    NSLog(@"%@",pdfFilePath);
    [outputPDFData writeToFile:pdfFilePath atomically:YES];
    
}

@end
