//
//  ModelScreenVC.swift
//  Budgetina
//
//  Created by Kapil Dhawan on 04/04/19.
//  Copyright © 2019 WeEnggs Technology. All rights reserved.
//

import UIKit
import DropDown
import FirebaseDatabase
import FirebaseAuth


class ModelScreenVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
   
    
   
    @IBOutlet weak var segmentController: UISegmentedControl!
    
   
    @IBOutlet weak var toggleRecursive: UISwitch!
    @IBOutlet weak var btnDrop: UIButton!
    @IBOutlet weak var tblForDrop: UITableView!
    @IBOutlet weak var tfAmount: UITextField!
    @IBOutlet weak var vwExpense: UIView!
    @IBOutlet weak var vwIncome: UIView!
    
    let categoryArray = NSArray(array:["ESSENTIALS","LIFESTYLE","FINANCIAL FUTURE"])
    var selectedDataCategories : NSMutableArray = NSMutableArray()
    var dataEssentials : NSMutableArray = NSMutableArray()
    var spendingCategories : NSMutableArray = NSMutableArray()
    var dataLifestyle : NSMutableArray = NSMutableArray()
    var dataFinancial : NSMutableArray = NSMutableArray()
    var selectedDataCategoriesArray = [Dictionary<String, String>]()
    var ref: DatabaseReference!
    var segmentTitle = String()
    
    var dataIncome: NSMutableArray = NSMutableArray()
    var currentIndex: Int = 0
    var index: Int = 0
    var userkey = String()
    var arrIncomeCategory = ["What is your net paycheck every month?", "Do you earn any income from interest?", "Do you get any alimony or child support?","Any other misc. income you want to enter?"]
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        tblForDrop.delegate = self
        tblForDrop.dataSource = self
        self.segmentTitle = segmentController.titleForSegment(at: segmentController.selectedSegmentIndex)!
        toggleRecursive.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
        ref = Database.database().reference()
        // get income
        ref.child("incomeCategories").observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user value
            if let value = snapshot.value as? NSMutableArray {
                self.dataIncome = value
                print(self.dataIncome)
            }
            
            // ...
        }) { (error) in
            print(error.localizedDescription)
        }
        
        // get spending categories
        ref.child("spendingCategories").observeSingleEvent(of: .value, with: { (snapshot) in
            
            if let value = snapshot.value as? NSMutableArray {
                self.spendingCategories = value
                self.dataEssentials = (self.spendingCategories[0] as! NSDictionary)["subCategories"] as! NSMutableArray
                self.dataLifestyle = (self.spendingCategories[1] as! NSDictionary)["subCategories"] as! NSMutableArray
                self.dataFinancial = (self.spendingCategories[2] as! NSDictionary)["subCategories"] as! NSMutableArray
                DispatchQueue.main.async {
                    self.tblForDrop .reloadData()
                }
                
            }
            
            // ...
        }) { (error) in
            print(error.localizedDescription)
        }
        //selected category array
        if UserDefaults.standard.value(forKey: "selectedDataCategories") != nil {
            selectedDataCategoriesArray = UserDefaults.standard.value(forKey: "selectedDataCategories") as! Array
        }
        print(selectedDataCategoriesArray)
        
    }
    
    
    
    @IBAction func btnCategoryTapped(_ sender: Any) {
        if tblForDrop.isHidden{
            animateState(toogle: false)
        }else{
            animateState(toogle: true)
        }
        
    }
    
    func animateState(toogle: Bool)
    {
        if toogle{
            UIView.animate(withDuration: 0.0) {
                self.tblForDrop.isHidden = true
            }}
        else{
            UIView.animate(withDuration: 0.0) {
                self.tblForDrop.isHidden = false
            }
        }
    }

    
    //Action for Segmented Controller
    @IBAction func segmentControlTapped(_ sender: Any) {
        self.segmentTitle = segmentController.titleForSegment(at: segmentController.selectedSegmentIndex)!
       self.btnDrop.setTitle("Select...", for: .normal)
        self.tblForDrop.isHidden = true
        if segmentTitle == "Income"{
            vwExpense.isHidden = true
            vwIncome.isHidden = false
            DispatchQueue.main.async {
                self.tblForDrop.reloadData()
            }
        }else{
            vwIncome.isHidden = true
            vwExpense.isHidden = false
            DispatchQueue.main.async {
                self.tblForDrop.reloadData()
            }
        }
        
    }
    


    
    @IBAction func btnBackPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    

func numberOfSections(in tableView: UITableView) -> Int {
    if segmentTitle == "Expenses" {
    return categoryArray.count
    }
    else {
        return 1
    }
}
func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
    if segmentTitle == "Expenses" {
    if section == 0 {
        return (categoryArray[0] as! String)
    }
    else if section == 1{
        return (categoryArray[1] as! String)
    }else {
        return (categoryArray[2] as! String)
    }
    }else{
        return nil
    }
}

func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if segmentTitle == "Expenses" {
    if(section == 0)
    {
        return dataEssentials.count
    }
    else if(section == 1)
    {
        return dataLifestyle.count
    }
    else if(section == 2)
    {
        return dataFinancial.count
    } else {
        return 0
    }
    }
    else {
        return arrIncomeCategory.count
    }
    
}

func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "DropdownCell", for: indexPath) as! DropdownCell
    if segmentTitle == "Expenses" {
    var dic = NSDictionary()
    
    if(indexPath.section == 0)
    {
        dic = self.dataEssentials[indexPath.row] as! NSDictionary
        
    }
    else if(indexPath.section == 1)
    {
        dic = self.dataLifestyle[indexPath.row] as! NSDictionary
        
        
    }
    else if(indexPath.section == 2)
    {
        
        dic = self.dataFinancial[indexPath.row] as! NSDictionary
        
    }
    
    cell.lblCategories.text = replaceDummyStrings(string: dic["categoryname"] as? String ?? "")
    } else{
        cell.lblCategories.text = arrIncomeCategory[indexPath.row]
    }
    return cell
}
func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    if segmentTitle == "Expenses" {
    var dic = NSDictionary()
    
    if(indexPath.section == 0)
    {
        dic = self.dataEssentials[indexPath.row] as! NSDictionary
     }
    else if(indexPath.section == 1)
    {
        dic = self.dataLifestyle[indexPath.row] as! NSDictionary
     }
    else if(indexPath.section == 2)
    {
        dic = self.dataFinancial[indexPath.row] as! NSDictionary
    }
    let title = replaceDummyStrings(string: dic["categoryname"] as? String ?? "")
    self.btnDrop.setTitle(title, for: .normal)
    self.selectedDataCategoriesArray.append(dic as! Dictionary<String, String>)
    let arr = selectedDataCategoriesArray.compactMap { $0 as? Dictionary<String, String> }
    UserDefaults.standard.set(arr, forKey: "selectedDataCategories")
    UserDefaults.standard.synchronize()
    
    print( UserDefaults.standard.value(forKey: "selectedDataCategories") ?? "nil")
    DispatchQueue.main.async {
        self.tblForDrop.reloadData()
    }
    }else{
        let title = arrIncomeCategory[indexPath.row]
        self.btnDrop.setTitle(title, for: .normal)
        self.currentIndex = indexPath.row
        let user = Auth.auth().currentUser
        self.ref.child("users").child(user?.uid ?? "nil").child("income").child(Date().yearAsString()).child(Date().monthAsString()).queryOrdered(byChild: "incomecategoryid").queryEqual(toValue: self.currentIndex).observeSingleEvent(of: .value, with: { (snapshot) in
            
            if snapshot.exists(){
                
                for child in snapshot.children {
                    self.userkey = (child as AnyObject).key as String
                }
                print(self.userkey)
         }
        })
    
    }
    }

func replaceDummyStrings(string: String) -> String {
    var stringR = string
    stringR = stringR.replacingOccurrences(of: "HOME - ", with: "")
    stringR = stringR.replacingOccurrences(of: "UTILITIES - ", with: "")
    stringR = stringR.replacingOccurrences(of: "GROCERIES - ", with: "")
    stringR = stringR.replacingOccurrences(of: "TRANSPORTATION - ", with: "")
    stringR = stringR.replacingOccurrences(of: "FAMILY OBLIGATIONS - ", with: "")
    stringR = stringR.replacingOccurrences(of: "DEBT PAYMENT - ", with: "")
    stringR = stringR.replacingOccurrences(of: "ENTERTAINMENT - ", with: "")
    stringR = stringR.replacingOccurrences(of: "GROOMING - ", with: "")
    stringR = stringR.replacingOccurrences(of: "HEALTH & FITNESS - ", with: "")
    stringR = stringR.replacingOccurrences(of: "CLOTHING - ", with: "")
    stringR = stringR.replacingOccurrences(of: "HOBBIES - ", with: "")
    stringR = stringR.replacingOccurrences(of: "HEALTH & MEDICAL - ", with: "")
    stringR = stringR.replacingOccurrences(of: "PROFESSIONAL DUES - ", with: "")
    return stringR.lowercased().capitalizingFirstLetter()
}
    
    @IBAction func btnAdd(_ sender: Any) {
        var recursive: Int = 0
        if toggleRecursive.isOn {
            recursive = 1
        }else {
            recursive = 0
        }
        
        
        if (btnDrop.titleLabel!.text == "Select..." || tfAmount.text?.count == 0){
            let alertController = UIAlertController(title: "Empty Field", message: "Please provide the required information for adding income. ", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            
            alertController.addAction(defaultAction)
            self.present(alertController, animated: true, completion: nil)
            
        } else{
            if segmentTitle == "Income" {
            print("working")
            var object = NSDictionary()
            object = self.dataIncome[currentIndex] as! NSDictionary
            
            let user = Auth.auth().currentUser
              /*  self.ref.child("users").child(user?.uid ?? "nil").child("income").child(Date().yearAsString()).child(Date().monthAsString()).queryOrdered(byChild: "incomecategoryid").queryEqual(toValue: self.currentIndex).observeSingleEvent(of: .value, with: { (snapshot) in
                    
                    if snapshot.exists(){
                        
                        for child in snapshot.children {
                            self.userkey = (child as AnyObject).key as String
                        }*/
            // users/[userid]/income/year/month/[income categoryid]/AMOUNT
            let dataRef =  self.ref.child("users").child(user?.uid ?? "nil").child("income").child(Date().yearAsString()).child(Date().monthAsString()).childByAutoId()
            
            let key = self.ref.child("users").child(user?.uid ?? "nil").child("income").child(Date().yearAsString()).child(Date().monthAsString()).childByAutoId().key
            
        
            dataRef.updateChildValues(["incomecategoryid":(object["incomecategoryid"] ?? "nil"),
                                       "timestamp":String(NSDate().timeIntervalSince1970),
                                       "id":key!,
                                       "isRecursive":"\(recursive)",
                                       "amount":tfAmount.text!])

            }else {
                var object = Dictionary<String, String>()
                
               // object = selectedDataCategoriesArray[] as Dictionary<String, String>
                
                let user = Auth.auth().currentUser
                // users/[userid]/spending/year/month/[parent categoryid]/[spending categoryid]/AMOUNT
                let dataRef =  self.ref.child("users").child(user?.uid ?? "nil").child("spending").child(Date().yearAsString()).child(Date().monthAsString()).childByAutoId()
                
                let key = self.ref.child("users").child(user?.uid ?? "nil").child("spending").child(Date().yearAsString()).child(Date().monthAsString()).childByAutoId().key
                
                dataRef.updateChildValues(["parentcategoryid":(object["parentcategoryid"] ?? "nil"),
                                           "categoryid":(object["categoryid"] ?? "nil"),
                                           "categoryname":(object["categoryname"] ?? "nil"),
                                           "timestamp":String(NSDate().timeIntervalSince1970),
                                           "id":key!,
                                           "isRecursive":"\(recursive)",
                                           "amount":tfAmount.text!])
            }
    }
}
}
