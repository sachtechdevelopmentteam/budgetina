//
//  AccountVC.swift
//  Budgetina
//
//  Created by Ovedgroup LLC on 19/11/15.
//  Copyright © 2015 Ovedgroup LLC. All rights reserved.
//

import UIKit

class AccountVC: UIViewController,UITextFieldDelegate
{
    
    @IBOutlet var txtEmail: UITextField!
    @IBOutlet var txtOldPassword: UITextField!
    @IBOutlet var txtNewPassword: UITextField!
    @IBOutlet var ActivityIndicator: UIActivityIndicatorView!

    
    @IBAction func btnLogoutAction(_ sender: AnyObject)
    {

        let alert = UIAlertController(title: "Alert", message: "Are you sure you want to logout?", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "No", style: UIAlertAction.Style.default, handler: nil))
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: { (UIAlertAction) -> Void in
            
            
            UserDefaults.standard.removeObject(forKey: "password")
            UserDefaults.standard.removeObject(forKey: "user_id")
            UserDefaults.standard.removeObject(forKey: "email")
            UserDefaults.standard.removeObject(forKey: "username")
            UserDefaults.standard.removeObject(forKey: "initialSetupDone")
            let domain = Bundle.main.bundleIdentifier!
            UserDefaults.standard.removePersistentDomain(forName: domain)
            UserDefaults.standard.synchronize()
            
            
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            appdelegate.showLoginScreen()
            
        }))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    @IBAction func btnClickArrowAction(_ sender: AnyObject)
    {
        self.ParsingURL()
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 200/225, green: 185/225, blue: 207/225, alpha: 1.0)
        self.automaticallyAdjustsScrollViewInsets = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        self.navigationItem.title = "Change Password"
        self.ActivityIndicator.hidesWhenStopped = true
        
        let Email = UserDefaults.standard.value(forKey: "email") as! String
        txtEmail.text = Email
        
    }
   
    func ShowMessage(_ message:NSString)
    {
        let alert = UIAlertController (title:"Alert", message:message as String, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title:"Ok", style: UIAlertAction.Style.default, handler: nil))
        
        self.present(alert, animated:true, completion: nil)
    }

    //MARK: Textfield Delegate Method
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if(textField == txtEmail)
        {
            txtOldPassword.becomeFirstResponder()
        }
        else if (textField == txtOldPassword)
        {
            txtNewPassword.becomeFirstResponder()
        }
        else if(textField == txtNewPassword)
        {
            txtNewPassword.resignFirstResponder()
            self.ParsingURL()
           
        }
                
        return true
    }
    
    func ParsingURL()
    {
        
        if(txtEmail.text == "")
        {
            self.ShowMessage("Enter Email")
        }
        else if(txtOldPassword.text == "")
        {
            self.ShowMessage("Enter Old Password")
        }
        else if(txtNewPassword.text == "")
        {
            self.ShowMessage("Enter New Password")
        }
        else
        {
            self.ActivityIndicator.isHidden = false
            self.ActivityIndicator.startAnimating()
            
            let userid = UserDefaults.standard.value(forKey: "user_id") as! String
            NSLog("%@", userid)
            
            let url = NSString(format: "%@?op=change_password&user_id=%@&old_password=%@&new_password=%@&access_token=%@",SERVER_URL,userid,txtOldPassword.text!,txtNewPassword.text!,"")
            
            
            var request: URLRequest = URLRequest(url: URL(string: url as String)!)
            request.httpMethod = "GET"

            let task = URLSession.shared.dataTask(with: request)
            { (data, response, error) -> Void in
                
                do {
                    //let str = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                   // print(str)
                    if  let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                        
                    {
                        print(jsonResult)
                        
                        DispatchQueue.main.sync
                            {
                                self.performActionOnmainThread(jsonResult)
                        }
                    }
                } catch
                {
                    print(error)
                }
                
            }
            task.resume()

        
        }

    }
    
    func performActionOnmainThread(_ data:NSDictionary)
    {
        if (((data["success"]! as AnyObject).boolValue) == true)
        {
            let newpassword = txtNewPassword.text
            
            UserDefaults.standard.set(newpassword, forKey: "password")
            UserDefaults.standard.synchronize()
            
            self.ActivityIndicator.stopAnimating()
            self.ShowMessage(data["message"]! as! NSString)
            txtEmail.text = ""
            txtOldPassword.text = ""
            txtNewPassword.text = ""
           
           
        }
        else
        {
            self.ActivityIndicator.stopAnimating()
            self.ShowMessage(data["message"]! as! NSString)
            //txtEmail.text = ""
            txtOldPassword.text = ""
            txtNewPassword.text = ""
        }
        
    }


    
}
