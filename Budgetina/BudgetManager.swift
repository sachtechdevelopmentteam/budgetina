//
//  BudgetManager.swift
//  Budgetina
//
//  Created by HANDA, KUNWAR on 3/18/19.
//  Copyright © 2019 WeEnggs Technology. All rights reserved.
//

import Foundation

class BudgetManager {
    
    // Gets income for specific subcategory
    class func getIncomeValuesForMonthForCategory(finicialBookForCategory: NSMutableDictionary, month: String, year: String, incomecategoryid: String) -> NSNumber {
        
        if let _ = finicialBookForCategory["income"] {
            if let typeOfTransaction = finicialBookForCategory["income"] as? NSDictionary {
                if let yearOfTransaction = typeOfTransaction[year] as? NSDictionary {
                    if let monthOfTransaction = yearOfTransaction[month] as? NSArray {
                        if let amountString = monthOfTransaction[incomecategoryid.numberValue?.intValue ?? 1 - 1] as? String {
                            if let i = amountString.numberValue{
                                return i
                            } else {
                                return 0
                            }
                        }
                    }
                }
            }
            return 0
            
        } else {
            print("No income or spending found")
            return 0
        }
    }
    
    // Gets total income for entire month

    class func getTotalIncomeValuesForMonth(finicialBookForCategory: NSMutableDictionary, month: String, year: String) -> Float {
        var totalValue:Float = 0.0

        if let _ = finicialBookForCategory["income"] {
            if let typeOfTransaction = finicialBookForCategory["income"] as? NSDictionary {
                if let monthOfTransaction = typeOfTransaction[month] as? NSDictionary {
                        for dicKeyValue in monthOfTransaction {
                            let dic = dicKeyValue.value as! NSDictionary
                            let amount = dic["amount"] as! String
                            if !amount.isEmpty{
                                totalValue += (amount.numberValue?.floatValue)!
                            }
                        }
                    return totalValue
                    }
            }
            return 0
            
        } else {
            print("No income or spending found")
            return 0
        }
    }
    
    // Gets total income for entire month
    
    class func getAllIncomeValuesForMonth(finicialBookForCategory: NSMutableDictionary, month: String, year: String) -> [String: String] {
        
        if let _ = finicialBookForCategory["income"] {
            if let typeOfTransaction = finicialBookForCategory["income"] as? NSDictionary {
                if let yearOfTransaction = typeOfTransaction[year] as? NSDictionary {
                    if let monthOfTransaction = yearOfTransaction[month] as? [String: String] {
                        return monthOfTransaction
                    }
                }
            }
            return ["error":"no data"]
            
        } else {
            print("No income or spending found")
            return ["error":"no data"]
        }
    }
    
    
    // Gets spending for specific category

    class func getSpendingValuesForMonthForParentsSubCategory(finicialBookForCategory: NSMutableDictionary, month: String, year: String, parentcategoryid: String, subcategoryid: String) -> NSNumber {
        
          if let _ = finicialBookForCategory["spending"] {
             if let typeOfTransaction = finicialBookForCategory["spending"] as? NSDictionary {
                if let yearOfTransaction = typeOfTransaction[year] as? NSDictionary {
                   if let monthOfTransaction = yearOfTransaction[month] as? NSDictionary {
                    if let parentCat = monthOfTransaction[parentcategoryid] as? NSDictionary {
                      if let amountString = parentCat[subcategoryid] as? String {
                        if let i = amountString.numberValue{
                             return i
                        } else {
                           return 0
                        }
                      }
                    }
                  }
                }
              }
            return 0
            
        } else {
            print("No income or spending found")
            return 0
        }
    }
    // Gets total spending for specific parent catrgory [essentials OR lifestyle OR finicial future]

    class func getTotalSpendingValuesForMonthForParentCategory(finicialBookForCategory: NSMutableDictionary, month: String, year: String, parentcategoryid: String) -> Float {
        var totalValue: Float  = 0.00
        if let _ = finicialBookForCategory["spending"] {
            if let typeOfTransaction = finicialBookForCategory["spending"] as? NSDictionary {
                if let monthOfTransaction = typeOfTransaction[month] as? [String:Any] {
                    for val in monthOfTransaction.values {
                        let parentCat = val as! NSDictionary
                        if let id = parentCat["parentcategoryid"] as? String {
                            if id == parentcategoryid{
                                if let amt = parentCat["amount"] as? String{
                                    totalValue += (amt.numberValue?.floatValue)!
                                }
                            }
                            
                        }
                    }
                    return totalValue
                }
            }
            return 0
            
        } else {
            print("No income or spending found")
            return 0
        }
    }
    
    // Gets total spending for specific parent catrgory [essentials OR lifestyle OR finicial future]
   
    class func getAllSpendingValuesForMonth(finicialBookForCategory: NSMutableDictionary, month: String, year: String) -> [String:[String:String]] {
        
        if let _ = finicialBookForCategory["spending"] {
            if let typeOfTransaction = finicialBookForCategory["spending"] as? NSDictionary {
                if let monthOfTransaction = typeOfTransaction[month] as? [String:[String:String]] {
                    return monthOfTransaction

                }
            }
            return ["error":["value":"no data"]]
            
        } else {
            print("No income or spending found")
            return ["error":["value":"no data"]]
        }
    }
    
    
    
    class func getAllSpendingValuesForMonthForParentCategory(finicialBookForCategory: NSMutableDictionary, month: String, year: String, parentcategoryid: String) -> NSDictionary {
        
        if let _ = finicialBookForCategory["spending"] {
            if let typeOfTransaction = finicialBookForCategory["spending"] as? NSDictionary {
                if let yearOfTransaction = typeOfTransaction[year] as? NSDictionary {
                    if let monthOfTransaction = yearOfTransaction[month] as? NSDictionary {
                        if let parentCat = monthOfTransaction[parentcategoryid] as? NSDictionary {
                            return parentCat
                        }
                    }
                }
            }
            return NSDictionary(object: "no data", forKey: "error" as NSCopying)
            
        } else {
            print("No income or spending found")
            return NSDictionary(object: "no data", forKey: "error" as NSCopying)
        }
    }
    
    // Gets total spending for entire month [essentials + lifestyle + finicial future]
    class func getTotalSpendingValuesForMonth(finicialBookForCategory: NSMutableDictionary, month: String, year: String) -> Float {
        
        var totalValue:Float = 0.0
        if let _ = finicialBookForCategory["spending"] {
            if let typeOfTransaction = finicialBookForCategory["spending"] as? NSDictionary {
                if let monthOfTransaction = typeOfTransaction[month] as? [String:[String:String]] {
                    for val in monthOfTransaction {
                        let parentCat = val.value
                        for (key, value) in parentCat {
                            if key == "amount"{
                                totalValue += ((value).numberValue?.floatValue)!
                            }
                        }
                    }
                    return totalValue
                }
            }
            return 0
            
        } else {
            print("No income or spending found")
            return 0
        }
    }
    
    func getNameOfpendingSubCategoryFromParentId(parentId: String, subCatId: String, spendingCategories: NSMutableArray) -> String {
        
        if  spendingCategories.count > 0 {
            for dic in spendingCategories {
                if let sendDic = dic as? NSDictionary{
                    if let catid = sendDic["categoryid"] as? String{
                        if catid == parentId {
                            if let subCatArray = sendDic["subCategories"] as? NSArray {
                                for subCatDic in subCatArray{
                                    if let subDic = subCatDic as? NSDictionary{
                                        if let string = subDic["categoryid"] as? String{
                                            if string == subCatId{
                                                return subDic["categoryname"] as? String ?? "nil"
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return "nil"
        } else {
            return "nil"
        }
    }
    
    
    
    func getParentIdFromSubCategoryId(subCatId: String, spendingCategories: NSMutableArray) -> String {
        if  spendingCategories.count > 0 {
            for dic in spendingCategories {
                if let sendDic = dic as? NSDictionary{
                    if let subCatArray = sendDic["subCategories"] as? NSArray{
                        for catDic in subCatArray {
                            if let dic = catDic as? NSDictionary{
                                if let id = dic["categoryid"] as? String{
                                    if id == subCatId {
                                        return dic["parentcategoryid"] as? String ?? "nil"
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return "nil"
        } else {
            return "nil"
        }
    }
    
    
    func getSubCategoryNameFromId(subCatId: String, spendingCategories: NSMutableArray) -> String {
        if  spendingCategories.count > 0 {
            for dic in spendingCategories {
                if let sendDic = dic as? NSDictionary{
                    if let subCatArray = sendDic["subCategories"] as? NSArray{
                        for catDic in subCatArray {
                            if let dic = catDic as? NSDictionary{
                                if let id = dic["categoryid"] as? String{
                                    if id == subCatId {
                                        return dic["categoryname"] as? String ?? "nil"
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return "nil"
        } else {
            return "nil"
        }
    }
    
}
