//
//  CategoryHeaderCollectionReusableView.swift
//  Budgetina
//
//  Created by HANDA, KUNWAR on 2/23/19.
//  Copyright © 2019 WeEnggs Technology. All rights reserved.
//

import UIKit

class CategoryHeaderCollectionReusableView: UICollectionReusableView {

    @IBOutlet weak var hearderTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
