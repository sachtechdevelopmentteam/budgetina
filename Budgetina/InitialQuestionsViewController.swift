//
//  InitialQuestionsViewController.swift
//  Budgetina
//
//  Created by HANDA, KUNWAR on 2/17/19.
//  Copyright © 2019 WeEnggs Technology. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth
class InitialQuestionsViewController: UIViewController {
    
    var _currentTextfieldValue = 0.0 as CGFloat
    var ref: DatabaseReference!
    var dataIncome: NSMutableArray = NSMutableArray()
    var currentIndex : Int = 0
    var swiftyOnboard: SwiftyOnboard!
    var titleArray: [String] = ["What is your net paycheck every month?", "Do you earn any income from interest?", "Do you get any alimony or child support?","Any other misc. income you want to enter?"]
    
    var gradiant: CAGradientLayer = {
        //Gradiant for the background view
        let blue = UIColor(red: 69/255, green: 127/255, blue: 202/255, alpha: 1.0).cgColor
        let purple = UIColor(red: 166/255, green: 172/255, blue: 236/255, alpha: 1.0).cgColor
        let gradiant = CAGradientLayer()
        gradiant.colors = [purple, blue]
        gradiant.startPoint = CGPoint(x: 0.5, y: 0.18)
        return gradiant
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        gradient()
        ref = Database.database().reference()

        swiftyOnboard = SwiftyOnboard(frame: view.frame, style: .light)
        swiftyOnboard.shouldSwipe = false

        ref.child("incomeCategories").observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user value
            if let value = snapshot.value as? NSMutableArray {
                self.dataIncome = value
                print(self.dataIncome)
            }
            
            // ...
        }) { (error) in
            print(error.localizedDescription)
        }

        view.addSubview(swiftyOnboard)
        swiftyOnboard.dataSource = self
        swiftyOnboard.delegate = self
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func gradient() {
        //Add the gradiant to the view:
        self.gradiant.frame = view.bounds
        view.layer.addSublayer(gradiant)
    }
    
    @objc func handleSkip() {
        currentIndex = currentIndex + 1
        swiftyOnboard?.goToPage(index: currentIndex, animated: true)
    }
    
    @objc func handleContinue(sender: UIButton) {

        print("open category page")
        
        let categorySelectionViewController = self.storyboard?.instantiateViewController(withIdentifier: "CategorySelectionViewController") as! CategorySelectionViewController
        present(categorySelectionViewController, animated: true, completion: nil)

        //self.navigationController?.pushViewController(categorySelectionViewController, animated: true)
    }
}

extension InitialQuestionsViewController: SwiftyOnboardDelegate, SwiftyOnboardDataSource, UITextFieldDelegate {
    
    func swiftyOnboardNumberOfPages(_ swiftyOnboard: SwiftyOnboard) -> Int {
        //Number of pages in the onboarding:
        return 4
    }
    
    func swiftyOnboardBackgroundColorFor(_ swiftyOnboard: SwiftyOnboard, atIndex index: Int) -> UIColor? {
        //Return the background color for the page at index:
        return #colorLiteral(red: 0.2666860223, green: 0.5116362572, blue: 1, alpha: 1)
    }
    
    func swiftyOnboardPageForIndex(_ swiftyOnboard: SwiftyOnboard, index: Int) -> SwiftyOnboardPage? {
        let view = SwiftyOnboardPage()
        

        //Set the font and color for the labels:
        view.questionsTitle.font = UIFont(name: "Lato-Heavy", size: 22)
        //view.questionsTitle.backgroundColor = .gray
        view.answerField.font = UIFont(name: "Lato-Regular", size: 16)
        //view.answerField.backgroundColor = .gray
        view.answerField.delegate = self
        //Set the text in the page:
        view.questionsTitle.text = titleArray[index]
       // view.answerField.text = subTitleArray[index]
        
        //Return the page for the given index:
        return view
    }
    
    func swiftyOnboardViewForOverlay(_ swiftyOnboard: SwiftyOnboard) -> SwiftyOnboardOverlay? {
        let overlay = SwiftyOnboardOverlay()
        overlay.skipButton.isHidden = true
        overlay.continueButton.isHidden = true
        //Setup targets for the buttons on the overlay view:
        overlay.skipButton.addTarget(self, action: #selector(handleSkip), for: .touchUpInside)
        overlay.continueButton.addTarget(self, action: #selector(handleContinue), for: .touchUpInside)
//
//        //Setup for the overlay buttons:
//        overlay.continueButton.titleLabel?.font = UIFont(name: "Lato-Bold", size: 16)
//        overlay.continueButton.setTitleColor(UIColor.white, for: .normal)
//        overlay.skipButton.setTitleColor(UIColor.white, for: .normal)
//        overlay.skipButton.titleLabel?.font = UIFont(name: "Lato-Heavy", size: 16)
        
        //Return the overlay view:
        return overlay
    }
    
    func swiftyOnboardOverlayForPosition(_ swiftyOnboard: SwiftyOnboard, overlay: SwiftyOnboardOverlay, for position: Double) {
        let currentPage = round(position)
        overlay.pageControl.currentPage = Int(currentPage)
        print(Int(currentPage))
        overlay.continueButton.tag = Int(position)
        
        if currentPage == 0.0 {
            overlay.continueButton.setTitle("Continue", for: .normal)
            overlay.skipButton.setTitle("Skip", for: .normal)
            overlay.skipButton.isHidden = true
        } else {
            overlay.continueButton.setTitle("Continue to spending!", for: .normal)
            overlay.continueButton.isHidden = false
            overlay.skipButton.isHidden = false
        }
    }
    
    // MARK: TextField Delegate Method
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        let toolBar = UIToolbar(frame: CGRect(x: 0,y: 0,width: self.view.frame.size.width,height: 40))
        toolBar.barStyle = UIBarStyle.default
        
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        textField.inputAccessoryView = toolBar
        
        
        //tableCategory.scrollEnabled = false
        
        let val = (textField.text?.replacingOccurrences(of: "$", with: ""))! as NSString
        _currentTextfieldValue = CGFloat(val.floatValue)

        
        //_currentIndexPath = tableCategory.indexPathForCell(cell)
        
        
        if textField.text == "$0.00"
        {
            textField.text = "$"
        }
        textField.becomeFirstResponder()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        
        var object = NSDictionary()
        object = self.dataIncome[currentIndex] as! NSDictionary

         let user = Auth.auth().currentUser
        // users/[userid]/income/year/month/[income categoryid]/AMOUNT
        
        
        
        
        let dataRef =  self.ref.child("users").child(user?.uid ?? "nil").child("income").child(Date().yearAsString()).child(Date().monthAsString()).childByAutoId()
        
        let key = self.ref.child("users").child(user?.uid ?? "nil").child("income").child(Date().yearAsString()).child(Date().monthAsString()).childByAutoId().key
        
        
        dataRef.updateChildValues(["incomecategoryid":(object["incomecategoryid"] ?? "nil"),
                                   "timestamp":String(NSDate().timeIntervalSince1970),
                                   "id":key!,
                                   "isRecursive":"1",
                                   "amount":textField.text!])
        
        self.ref.child("users").child(user?.uid ?? "nil").child("income").child(Date().yearAsString()).child(Date().monthAsString()).observeSingleEvent(of: .value, with: {snapshot in
            let value = snapshot.value
            print(value)
        })
   
        currentIndex = currentIndex + 1
        swiftyOnboard?.goToPage(index: currentIndex, animated: true)
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if let range = string.rangeOfCharacter(from: CharacterSet.decimalDigits) {
            print("start index: \(range.lowerBound), end index: \(range.upperBound)")
            
            return true
        }
        else {
            print("no data")
            
            if ((string == "") && (textField.text?.count != 1))
            {
                return true
            }
            return false
        }
    }
    
}
