//
//  SignUpVC.swift
//  Budgetina
//
//  Created by Ovedgroup LLC on 04/11/15.
//  Copyright © 2015 Ovedgroup LLC. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

class SignUpVC: UIViewController,UITextFieldDelegate
{
    @IBOutlet var txtUserName: UITextField!
    @IBOutlet var txtPassword: UITextField!
    @IBOutlet var txtReEnterPassword: UITextField!
    @IBOutlet var txtEmail:UITextField!
    var ref: DatabaseReference!

    @IBOutlet weak var signUpBtn: TKTransitionSubmitButton!
    
    
    @IBOutlet var ActivityIndicator: UIActivityIndicatorView!
    
       
    @IBAction func btnClickArrowAction()
    {
        self.parsingURL()
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        ref = Database.database().reference()

        self.navigationController?.navigationBar.barTintColor = UIColor(red: 200/225, green: 185/225, blue: 207/225, alpha: 1.0)
        self.automaticallyAdjustsScrollViewInsets = false
        
        
        txtPassword.layer.cornerRadius = txtPassword.frame.size.height/2
        txtReEnterPassword.layer.cornerRadius = txtReEnterPassword.frame.size.height/2
        txtEmail.layer.cornerRadius = txtEmail.frame.size.height/2
        signUpBtn.layer.cornerRadius = signUpBtn.frame.size.height/2


        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationItem.title = "Welcome To Budgetina"
        self.ActivityIndicator.hidesWhenStopped = true

    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.tintColor = UIColor.black

    }
    
    override var prefersStatusBarHidden : Bool
    {
        return false
    }
    
     func ShowMessage(_ message:NSString)
    {
        let alert = UIAlertController (title:"Alert", message:message as String, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title:"OK", style: UIAlertAction.Style.default, handler: nil))
        
        self.present(alert, animated:true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
        //MARK: Textfield Delegate Method
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if(textField == txtEmail)
        {
            txtPassword.becomeFirstResponder()
        }
        else if(textField == txtPassword)
        {
            txtReEnterPassword.becomeFirstResponder()
        }
        else if(textField == txtReEnterPassword)
        {
            txtReEnterPassword.resignFirstResponder()
            
            self .parsingURL()

        }
        
        return true
    }
    
    func parsingURL()
    {
        if(txtEmail.text == "")
        {
            txtEmail.shake(10, withDelta: 10.0)
        }
        else if(!self.isValidEmail(txtEmail.text!))
        {
            txtEmail.shake(10, withDelta: 10.0)
        }
        else if(txtPassword.text == "")
        {
            txtPassword.shake(10, withDelta: 10.0)
        }
        else if(txtReEnterPassword.text == "")
        {
            txtReEnterPassword.shake(10, withDelta: 10.0)
        }
        else if(txtPassword.text != txtReEnterPassword.text)
        {
            txtPassword.shake(10, withDelta: 10.0)
            txtReEnterPassword.shake(10, withDelta: 10.0)

        }
        else
        {
            signUpBtn.startLoadingAnimation()
            Auth.auth().createUser(withEmail: txtEmail.text ?? "nil", password: txtPassword.text ?? "nil") { authResult, error in
                // ...
                
                if let errorL = error {
                    self.ShowMessage(errorL.localizedDescription as NSString)
                    self.signUpBtn.returnToOriginalState()

                } else {
                    self.ActivityIndicator.stopAnimating()
                    
                    let currentUser = Auth.auth().currentUser
                    
                    let userDic = NSDictionary(objects: [currentUser?.email ?? "nil", currentUser?.uid ?? "nil"], forKeys: ["email" as NSCopying, "userid" as NSCopying])
                    
                    //self.ref.child("users").child(currentUser?.uid ?? "nil").setValue(["email": currentUser?.email ?? "nil"])

                    self.ref.child("users").child(currentUser?.uid ?? "nil").setValue(userDic)
                    self.emailVerification()
                    
                    UserDefaults.standard.set(currentUser?.email, forKey: "email")
                    
                    
                    UserDefaults.standard.set(authResult?.user.email, forKey:"email")
                    UserDefaults.standard.synchronize()
                    
                    
                    let initialSetupDone =  UserDefaults.standard.value(forKey: "initialSetupDone") as? Bool
                    
                    if initialSetupDone ?? true {
                        let initialQuestionsViewController = self.storyboard?.instantiateViewController(withIdentifier: "InitialQuestionsViewController") as! InitialQuestionsViewController
                        
                        self.present(initialQuestionsViewController, animated: true, completion: nil)
                        self.navigationController?.pushViewController(initialQuestionsViewController, animated: true)
                    } else {
                        let appdelegate = UIApplication.shared.delegate as! AppDelegate
                        appdelegate.showMainTabbar()
                    }
                }
            }
        }
    }
    
    func emailVerification() -> Void {
        Auth.auth().currentUser?.sendEmailVerification { (error) in
            if let errorL = error {
                self.ShowMessage(errorL.localizedDescription as NSString)
            } else {
                self.ShowMessage("We have also sent you an email for verification. Please take a moment to verify your email.")
            }
        }
    }


    func performActionOnmainThread(_ dict: NSDictionary){
        
        if (((dict["success"]! as AnyObject).boolValue) == true)
        {
           // self.ActivityIndicator.stopAnimating()

           self.parsigLoginurl()
        }
        else
        {
            self.signUpBtn.returnToOriginalState()
            self.ShowMessage(dict["message"]! as! NSString)
        }
            
    }
    
    func isValidEmail(_ testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }

    func textfieldReset()
    {
        txtEmail.text = ""
        txtUserName.text = ""
        txtPassword.text = ""
        txtReEnterPassword.text = ""
    }
    
    func parsigLoginurl()
    {
            
            let url = NSString(format:"%@?op=login_user&username=%@&password=%@&access_token=%@",SERVER_URL,txtUserName.text!,txtPassword.text!,"")
        
            
            var request: URLRequest = URLRequest(url: URL(string: url as String)!)
            request.httpMethod = "GET"
            
        let task = URLSession.shared.dataTask(with: request)
        { (data, response, error) -> Void in
                do {
                    //let str = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)

                    if  let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                        
                    {
                        print(jsonResult)
                        

                       
                    }
                } catch
                {
                    print(error)
                }
                
            }
            task.resume()
        
    }
    
    @IBAction func closeSignUp(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
    }
    func performActionOnmainThreadLogin(_ data:NSDictionary)
    {
        if (((data["success"]! as AnyObject).boolValue) == true)
        {
           
            
        }
        else
        {
            self.ActivityIndicator.stopAnimating()
            self.ShowMessage(data["message"]! as! NSString)
            txtUserName.text = ""
            txtPassword.text = ""
        }
        
    }

    
}
